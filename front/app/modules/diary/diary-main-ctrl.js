app.controller('DiaryMainCtrl',
    ['$scope','diary','$rootScope','$q','auth',
        function($scope,diary,$rootScope,$q,auth) {

            $scope.prepared = {'trainings': []};

            $scope.data = {};
            $scope.options = {
                'perPage'       : 5,
                'limit'         : 5,
                'selectedScheme': null
            };


            $scope.init = function() {
                diary.data().then(function(response) {
                    $scope.data = response.plain();
                });
                $scope.options.selectedScheme = auth.getUser().scheme;
            };


            $rootScope.title = 'Дневник';
            $scope.showMore = function() {
                $scope.options.limit += $scope.options.perPage;
            };
            $scope.resetLimit = function() {
                $scope.options.limit = $scope.options.perPage;
            };

        }]);


