app.controller('CommentCtrl',['$scope','diary','commentModal',
    function($scope,diary,commentModal) {

        $scope.test = commentModal.variable;
        $scope.default = {
            'comment': ''
        };

        $scope.$watch('commentData.exercise',function() {
            $scope.clearForm();
            //if($scope.commentData.exercise) {
            //    $scope.form.comment = angular.copy($scope.commentData.exercise.comment);
            //}
        });


        $scope.deleteComment = function() {
            diary
                .setComment({
                    'comment': '',
                    'id'     : $scope.commentData.exercise.id
                })
                .success(function() {
                    $scope.commentData.exercise.comment = '';
                });
        };
        $scope.submit = function() {

            if(!$scope.CommentForm.$valid)
                return false;

            diary
                .setComment({
                    'comment': $scope.form.comment,
                    'id'     : $scope.commentData.exercise.id
                })
                .success(function() {
                    $scope.commentData.exercise.comment = angular.copy($scope.form.comment);
                });
        };

        $scope.clearForm = function() {

            $scope.CommentForm.$setPristine();
            $scope.form = angular.copy($scope.default);
        };

    }]);

