app.controller('DiaryDetailsCtrl',['$scope','diary','$q','$stateParams',
    function($scope,diary,$q,$stateParams) {

        $scope.prepared = {};

        $scope.init = function() {
            $scope.updateData();
        };

        $scope.updateData = function() {
            var promises = [
                diary.exercises($scope.options.selectedScheme)
            ];

            $q.all(promises)
                .then(function(response) {
                    $scope.prepared = response[0].plain();
                });
            /*                .then(function() {
             $scope.prepareTrainings();
             $scope.prepareScheme()
             })*/
        };


    }]);
