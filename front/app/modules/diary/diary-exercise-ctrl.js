app.controller('ExerciseCtrl',['$scope','diary',
    function($scope,diary) {

        $scope.$watch('exercise.exerciseId',function() {
            $scope.params = $scope.data.params[$scope.exercise.exerciseId];
        });

        $scope.$watch('exercise.reps + exercise.count + exercise.weight',function() {
            $scope.calculateWeight();
        });

        $scope.calculateWeight = function() {
            var params = {
                'weight'      : $scope.exercise.weight,
                'count'       : $scope.exercise.count,
                'reps'        : $scope.exercise.reps,
                'weightStep'  : $scope.params.weightStep,
                'increaseType': $scope.exercise.increaseType
            };
            $scope.exercise.weights = diary.calculateWeight(params);
        };


        $scope.removeExercise = function() {
            $scope.training.exercises.splice($scope.$index,1);
        };

        /*        $scope.editComment = function() {
         commentModal.setCurrent($scope.exercise);
         };*/

    }]);
