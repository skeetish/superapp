app.service('diary',['Restangular',function(Restangular) {

    this.generate = function(number) {
        return Restangular.one('schemes',number).customGET('generate')
    };

    this.data = function() {
        return Restangular.all('schemes').get('initials');
    };

    this.list = function(number) {
        return Restangular.one('schemes',number).customGET('trainings');
    };

    this.exercises = function(number) {
        return Restangular.one('schemes',number).one('exercises').customGET('deadlift');
    };

    this.create = function(attributes,number) {
        return Restangular.one('schemes',number).all('trainings').post(attributes);
    };

    this.remove = function(id) {
        return Restangular.all('trainings').customDELETE(id);
    };

    this.update = function(attributes) {
        return Restangular.one('trainings',attributes.id).customPUT(attributes);
    };


    this.setComment = function(comment,id) {
        return Restangular.customPUT({comment: comment},'update-training',{'id': id});
    };

    this.calculateWeight = function(params) {

        var weights = [];
        switch (parseInt(params.increaseType)) {
            case 0:
                weights = linearWeights(params);
                break;
            case 1:
                weights = pyramidWeights(params);
                break;
        }
        return weights;
    };

    var linearWeights = function(params) {
        var plan = [];
        var curWeight = Math.round(params.weight / params.weightStep) * params.weightStep;

        for(var i = 1; i <= params.count; i++) {
            plan.push(createWeightText(curWeight,params.reps));
        }
        return plan;
    };

    var pyramidWeights = function(params) {
        var plan = [];

        for(var i = 1; i <= params.count; i++) {
            var curWeight = Math.round(( ( params.weight * 0.5 ) + ( params.weight * 0.5 / ( params.count - 1 ) * ( i - 1 ) ) ) / params.weightStep) * params.weightStep;
            plan.push(createWeightText(curWeight,params.reps));
        }
        return plan;
    };

    var createWeightText = function(weight,reps) {
        return weight + 'x' + reps;
    };

    return this;
}]);
