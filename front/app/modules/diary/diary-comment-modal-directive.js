/**
 * Created by Skeet on 14.01.2015.
 */
app.directive('commentModal',['appSettings','commentModal',
    function(appSettings,commentModal) {
        return {
            templateUrl: appSettings.templatePath + 'diary-comment-modal.html',
            restrict   : 'E',
            replace    : true,
            scope      : {},
            link       : function(scope,element,attrs) {
                scope.variable = commentModal.variable;
            }
        }
    }]);
