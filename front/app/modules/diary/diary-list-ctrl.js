app.controller('DiaryListCtrl',
    ['$scope','diary','$q',
        function($scope,diary,$q) {


            $scope.commentData = {};
            $scope.backup = [];


            $scope.init = function() {
                $scope.$watch('options.selectedScheme',function() {
                    $scope.updateData();
                    $scope.resetLimit();
                });

            };
            $scope.prepareScheme = function() {
                $scope.data.schemes.forEach(function(value,k) {
                    if(value.id == $scope.options.selectedScheme)
                        $scope.prepared.currentScheme = value;
                });
            };

            $scope.updateData = function() {
                var promises = [
                    diary.list($scope.options.selectedScheme),
                    diary.generate($scope.options.selectedScheme)
                ];

                $q.all(promises)
                    .then(function(response) {
                        $scope.data.trainings = response[0].plain();
                        $scope.data.generated = response[1].plain();
                    })
                    .then(function() {
                        $scope.prepareTrainings();
                        $scope.prepareScheme()
                    })
            };


            $scope.prepareTrainings = function() {
                $scope.prepared.trainings = angular.copy($scope.data.trainings);
                $scope.prepared.trainings.unshift($scope.data.generated);
            };
        }]);

