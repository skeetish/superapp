app.controller('TrainingCtrl',['$scope','diary',
    function($scope,diary) {

        $scope.local = {
            'edit': false
        };

        $scope.$watch('training.id',function(newValue) {
            $scope.isNew = newValue ? false : true;
        });

        $scope.editTraining = function() {
            $scope.local.edit = true;
            $scope.backup = angular.copy($scope.training);
        };

        $scope.saveTraining = function() {
            $scope.local.edit = false;
            $scope.backup = {};
            if(!$scope.isNew)
                diary.update($scope.training);
        };

        $scope.revertTraining = function() {
            $scope.local.edit = false;
            $scope.training = angular.copy($scope.backup);
            $scope.backup = {};
        };

        $scope.approveTraining = function() {
            diary
                .create($scope.training,$scope.options.selectedScheme)
                .then(function() {
                    $scope.updateData();
                });
        };

        $scope.removeTraining = function() {
            diary
                .remove($scope.training.id)
                .then(function() {
                    $scope.updateData();
                });
        };

        $scope.addExercise = function() {
            if($scope.NewExerciseForm.$valid) {
                var exercise = angular.copy($scope.form);
                exercise.completed = 1;
                $scope.training.exercises.push(exercise);
            }
        };

    }]);

