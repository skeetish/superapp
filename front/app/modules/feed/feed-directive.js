app.directive('torrentUrl',function() {
    var EMAIL_REGEXP = /^http:\/\/dl\.rutracker\.org\/forum\/dl\.php\?t=(\d+)$/i;

    return {
        require : 'ngModel',
        restrict: '',
        link    : function(scope,elm,attrs,ngModel) {
            if(ngModel && ngModel.$validators.url) {
                ngModel.$validators.url = function(modelValue) {
                    return ngModel.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
                };
            }
        }
    };
});
