app.service('feed',
    ['Restangular',function(Restangular) {


        this.list = function() {
            return Restangular.all('torrents').getList()
        };
        this.parse = function() {
            return Restangular.all('torrents').get('parse')
        };
        this.create = function(data) {
            return Restangular.all('torrents').post(data);
        };


    }]);

