app.controller('FeedMainCtrl',
    ['$scope','feed','$rootScope',function($scope,feed,$rootScope) {
        
        $scope.torrents = [];
        $scope.history = {};
        $rootScope.title = 'Торренты';

        $scope.init = function() {
            feed.list().then(function(response) {
                $scope.torrents = response;
            });
        };

        $scope.update = function() {
            feed.parse().success(function(response) {
                $scope.torrents.forEach(function(value) {
                    value.changes = response.changes[value.torrentId];
                });
            })
        };

    }]);
