app.controller('TorrentCtrl',['$scope',
    function($scope) {

        $scope.options = {
            showFiles: 5,
            isOpen   : false
        };
        $scope.showHistory = function() {
            $scope.history.data = $scope.torrent.torrentLogs;
        };

        $scope.$watch('torrent.changes',function(value) {
            if(value)
                $scope.options.isOpen = true;
        })
    }]);
