app.controller('NewTorrentCtrl',
    ['$scope','feed',function($scope,feed) {

        $scope.addTorrent = function() {


            if(!$scope.NewTorrentForm.$valid) {
                $scope.NewTorrentForm.$setSubmitted();
                return false;
            }

            var torrentData = angular.copy($scope.form);
            feed.create(torrentData)
                .then(function(response) {
                    if(response.result) {
                        var torrent = response.attributes;
                        torrent.torrentFiles = response.torrentFiles;
                        torrent.torrentLogs = response.torrentLogs;
                        $scope.torrents.unshift(torrent);
                    }
                    else if(response.errors.torrentUrl) {
                        $scope.NewTorrentForm.torrentUrl.$error.alreadyExists = true;
                    }
                });
        };
    }]);

