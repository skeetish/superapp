var app = angular.module('app',[
    'ui.router',
    'ngStorage',
    'chieffancypants.loadingBar',
    'ngAnimate',
    'restangular',
    'QuickList'
])

    .constant('appSettings',{
        isMobile    : navigator.userAgent.match('iPod|iPhone|iPad') ? true : false,
        templatePath: 'templates/'
    })

    .config(['cfpLoadingBarProvider',function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.parentSelector = 'content';
    }])

    .config(['RestangularProvider',function(RestangularProvider) {
        RestangularProvider.setBaseUrl('/back/rest');
    }])


    .config(['$urlMatcherFactoryProvider',function($urlMatcherFactoryProvider) {
        $urlMatcherFactoryProvider.strictMode(false);
    }])

    .run(['$rootScope','auth','appSettings',function($rootScope,auth,appSettings) {
        $rootScope.isMobile = appSettings.isMobile;
        $rootScope.redirectTo = 'diary.main';
        $rootScope.isLogged = auth.isLogged;
    }])

    .run(['$rootScope','$state','auth',function($rootScope,$state,auth) {
        $rootScope.$on('$stateChangeStart',function(event,next) {
                if(next.name != 'login' && !auth.isLogged()) {
                    event.preventDefault();
                    $state.go('login');
                }
            }
        )
    }]);