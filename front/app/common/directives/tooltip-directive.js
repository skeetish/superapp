app.directive('tooltip',function() {
    return function(scope,element,attrs) {
        $(element).on('mouseenter',function() {
            $(this)
                .tooltip({
                    'placement': attrs.tooltipPosition,
                    'title'    : attrs.tooltip,
                    'trigger'  : 'manual'
                })
                .tooltip('show');
        });

        $(element).on('mouseleave',function() {
            $(this).tooltip('destroy');
        });
    }
});
