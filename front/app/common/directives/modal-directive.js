app.directive('modal',['appSettings',function(appSettings) {
    return {
        templateUrl: appSettings.templatePath + 'modal-directive.html',
        restrict   : 'E',
        transclude : true,
        replace    : true,
        scope      : {
            'modalTitle'      : '@',
            'modalId'         : '@',
            'modalSize'       : '@',
            'modalCloseButton': '@',
            'modalOpen'       : '='
        },

        link: function(scope,element,attrs) {

            var that = $(element);

            console.log(element);
            var options = {
                'show'    : false,
                'backdrop': attrs['modalBackdrop']
            };
            that.modal(options);

            scope.$watch('modalOpen',function(newv) {

                if(newv === true)
                    that.modal('show');
                else
                    that.modal('hide');
            });


        }
    }
}])

    .directive('modalBtn',function() {
        return function(scope,element,attrs) {
            $(element).on('click',function() {
                var modalFrame = $('#' + attrs.modalBtn);
                if(attrs['modalBtnAction'] == 'close')
                    modalFrame.modal('hide');
                else
                    modalFrame.modal('show');
            });
        }
    });

