app.directive('datepicker',[function() {
    return {
        'require': 'ngModel',
        'link'   : function(scope,elem,attrs,ngModel) {

            var that = $(elem);
            var datepickerFormat = attrs['datepickerFormat']
                ? attrs['datepickerFormat']
                : 'dd/mm/yyyy';

            ngModel.$formatters.push(function(value) {
                return $.fn.datepicker.DPGlobal.formatDate(new Date(value * 1000),datepickerFormat,'en');
            });

            that.datepicker({'format': datepickerFormat});
            that.on('changeDate',function(val) {
                ngModel.$setViewValue(Date.parse(val.date) / 1000);
            });
        }
    };
}]);
