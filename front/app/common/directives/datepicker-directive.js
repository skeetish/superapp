app.directive('collapse',function() {
    return {
        'scope': {
            'collapseOpen': '='
        },
        'link' : function(scope,element) {
            element.addClass('panel-collapse collapse');
            scope.$watch('collapseOpen',function() {
                if(scope.collapseOpen)
                    element.addClass('in');
                else
                    element.removeClass('in');

            });
        }
    }
});
