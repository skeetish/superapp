app.controller('LeftMenuCtrl',['$scope',
    function($scope) {

        $scope.menu = [
            {"icon": "fa fa-tasks","label": "Дневник","url": "diary/index"},
            {"icon": "fa fa-download","label": "Торренты","url": "feed"}
        ];
        $scope.show = !$scope.isMobile;
        $scope.toggleMenu = function() {
            $scope.show = !$scope.show;
        }

    }]);
