app.directive('activeMenu',['$location',function($location) {
    return function(scope,elem) {
        scope.$on('$locationChangeSuccess',function() {

            var pathParts = $location.path().split('/');
            var href = elem.find('a').attr('href');
            var urlParts = href.split('/');

            if(urlParts[2] == pathParts[1])
                elem.addClass('active');
            else
                elem.removeClass('active')
        });
    };
}]);
