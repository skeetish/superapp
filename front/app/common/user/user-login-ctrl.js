app.controller('LoginCtrl',
    ['$scope', 'auth', '$rootScope', '$state',
        function ($scope, auth, $rootScope, $state) {

            $scope.errors = {};
            $rootScope.title = '';
            $scope.$watch('form.username + form.password', function () {
                delete $scope.LoginForm.$error.wrongCredentials;
            });

            $scope.submit = function () {

                if(!$scope.LoginForm.$valid) {
                    $scope.LoginForm.$setSubmitted();
                    return false;
                }

                auth.login($scope.form.username, $scope.form.password).then(
                    function () {
                        $state.go($rootScope.redirectTo);
                    },
                    function (response) {
                        $scope.errors = response;
                        $scope.LoginForm.$error.wrongCredentials = true;
                    });

            }
        }]);
