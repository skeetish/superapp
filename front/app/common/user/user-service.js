app.service('auth', ['$http', '$window', '$q', '$sessionStorage',
    function ($http, $window, $q, $sessionStorage) {


        var changeUser = function(value) {
            $sessionStorage.userData = value;
            setToken(value.hash)
        };

        var setToken = function(token) {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        };

        var login = function (userData) {
            return $http.post("/back/user/login", userData);
        };

        this.getUser = function () {
            return $sessionStorage.userData;
        };

        this.login = function(username,password) {
            var userData = {username: username,password: password};
            return login(userData).then(
                function(response) {
                    if(response.data.errors == null)
                        changeUser(response.data);
                    else
                        return $q.reject(response.data.errors);
                });
        };

        this.logout = function() {
            changeUser({'hash' : ''});
        };

        this.isLogged = function() {
            return ($sessionStorage.userData && $sessionStorage.userData.hash)
        };

        if(this.isLogged())
            setToken($sessionStorage.userData.hash);

        return this;

    }]);


