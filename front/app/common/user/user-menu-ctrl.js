/**
 * Created by Skeet on 11.01.2015.
 */
app.controller('UserMenuCtrl',
    ['$scope', 'auth', '$state', function ($scope, auth, $state) {

        $scope.vars = {
            userName : auth.getUser().name
        };
        $scope.logout = function () {
            auth.logout();
            $state.go('login');
        }
    }]);