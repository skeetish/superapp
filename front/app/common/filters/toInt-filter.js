app.filter('toInt',function() {
    return function(input,fieldname) {
        var array = input;
        array.forEach(function(val) {
            val[fieldname] = Number(val[fieldname]);
        });
        return array;
    }
});
