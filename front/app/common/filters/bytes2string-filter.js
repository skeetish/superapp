app.filter('bytes2string',function() {
    return function(size) {
        var values = ['Кб','Мб','Гб'];
        var text = '';

        values.forEach(function(value) {
            if(!text && ( size /= 1024 ) < 1024)
                text = parseFloat(size.toFixed(2)) + ' ' + value;
        });
        return text;
    };
});
