app.config(['$stateProvider','$urlRouterProvider','appSettings',
    function($stateProvider,$urlRouterProvider,appSettings) {

        var getVersionPostfix = function() {
            return (!appSettings.isMobile ? 'full' : 'mobile')
        };

        $urlRouterProvider.when('','/gallery');
        $urlRouterProvider.otherwise("/404");

        $stateProvider
            .state('404',{
                url        : '/404',
                templateUrl: appSettings.templatePath + 'home-404.html'
            })

            .state('diary',{
                url        : '/diary',
                templateUrl: appSettings.templatePath + 'diary-main.html',
                controller : 'DiaryMainCtrl',
                reload     : true
            })
            .state('diary.main',{
                url        : '/index',
                templateUrl: appSettings.templatePath + 'diary-list-' + getVersionPostfix() + '.html',
                controller : 'DiaryListCtrl',
                reload     : true
            })
            .state('diary.details',{
                url        : '/exercise/:alias',
                templateUrl: appSettings.templatePath + 'diary-details.html',
                controller : 'DiaryDetailsCtrl',
                reload     : true

            })

            .state('feed',{
                url        : '/feed',
                templateUrl: appSettings.templatePath + 'feed-full.html',
                controller : 'FeedMainCtrl'
            })
            .state('login',{
                url        : '/login',
                templateUrl: appSettings.templatePath + 'user-login-' + getVersionPostfix() + '.html'
            });
    }]);

