/**
 * Created by Skeet on 10.01.2015.
 */
var gulp = require('gulp'),
uglify   = require('gulp-uglify'),
csso     = require('gulp-csso'),
concat   = require('gulp-concat'),
imagemin = require('gulp-imagemin'),
flatten  = require('gulp-flatten'),
ftp      = require('gulp-ftp');

var assetsPath = './../web/assets/',
    webPath    = './../web/',
    srcPath    = './app/';

var ftpCred = {
    host : 'ftp.sktish.zz.mu',
    user : 'u139323023.sktish',
    pass : 'pylon321'
};

gulp.task('build', ['js', 'css', 'html', 'copyfonts'])

    /*.task('uploadAll', ['uploadApp', 'uploadVendor', 'uploadHtml', 'uploadCSS'])*/

    /*.task('uploadApp', function () {

        var connection = ftpCred;
        connection.remotePath = '/web/assets';

        return gulp.src([
            './assets/css*//**//*styles.css',
        ])
            .pipe(ftp(connection))
    })
*/
    .task('css', function () {
        gulp.src([
            './assets/css/**/*',
            './bower_components/font-awesome/css/font-awesome.css'
        ])
            .pipe(csso())
            .pipe(concat('styles.css'))
            .pipe(gulp.dest(assetsPath + 'css'))
    })

    .task('copyfonts', function () {
        gulp.src([
            './bower_components/font-awesome/fonts/*.{ttf,woff,eot,svg}',
            './assets/fonts/*.{ttf,woff,eot,svg}'
        ])
            .pipe(gulp.dest(assetsPath + 'fonts'));
    })

    .task('images', function () {
        gulp.src('./assets/images/**/*')
            .pipe(imagemin())
            .pipe(gulp.dest(assetsPath + 'images'))

    })

    .task('html', function () {

        gulp.src('./index.html')
            .pipe(gulp.dest(webPath));

        gulp.src(srcPath + '**/**/*.html')
            .pipe(flatten())
            .pipe(gulp.dest(webPath + 'templates'));
    })

    .task('js', function () {

        var vendorSrc = [
            'bower_components/angular/angular.js',
            "bower_components/angular-ui-router/release/angular-ui-router.js",
            "bower_components/angular-loading-bar/build/loading-bar.min.js",
            "bower_components/angular-animate/angular-animate.min.js",
            "bower_components/restangular/dist/restangular.js",
            "bower_components/lodash/dist/lodash.underscore.min.js",
            "bower_components/jquery/dist/jquery.min.js",
            "bower_components/bootstrap/dist/js/bootstrap.min.js",
            "bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js",
            "bower_components/ngstorage/ngStorage.min.js",
            "bower_components/quick-ng-repeat/quick-ng-repeat.js",
            "bower_components/highcharts/highcharts.js"
        ];

        gulp.src(vendorSrc)
            .pipe(concat('vendor.js'))
            .pipe(uglify())
            .pipe(gulp.dest(assetsPath + 'js'));

        gulp.src([srcPath + "**/**/*.js"])
            .pipe(concat('app.js'))
            //.pipe(uglify())
            .pipe(gulp.dest(assetsPath + 'js'));
    })


    .task('watch', ['build'], function () {
        gulp.watch(srcPath + '**/*.js', ['js']);
        gulp.watch([srcPath + '**/*.html', './index.html'], ['html']);
    })

    .task('default', ['build']);