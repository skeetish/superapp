<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "torrent_log".
 * @property integer $logId
 * @property string  $logDate
 * @property integer $logType
 * @property string  $logAttribute
 * @property string  $logValue
 * @property string  $logOldValue
 * @property integer $logLink
 * @property Torrent $torrent
 */
class TorrentLog extends \yii\db\ActiveRecord
{
    const TYPE_FILE_DELETE = 0;
    const TYPE_FILE_ADD = 1;

    const TYPE_TORRENT_INSERT = 2;
    const TYPE_TORRENT_CHANGE = 3;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'torrent_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ [ 'logType' , 'logValue' , 'logLink' ] , 'required' ] ,
            [ [ 'logType' ] , 'integer' ] ,
            [ [ 'logOldValue' , 'logValue' , 'logAttribute' ] , 'string' , 'max' => 255 ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'logId'     => Yii::t( 'yii' , 'Log ID' ) ,
            'logDate'   => Yii::t( 'yii' , 'Log Date' ) ,
            'logType'   => Yii::t( 'yii' , 'Log Type' ) ,
            'torrentId' => Yii::t( 'yii' , 'Torrent ID' ) ,
        ];
    }

    public function beforeSave( $insert )
    {
        $this->logDate = time();
        return parent::beforeSave( $insert );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTorrent()
    {
        return $this->hasOne( Torrent::className() , [ 'torrentId' => 'torrentId' ] )->inverseOf( 'torrentLogs' );
    }

    public static function find()
    {
        return new TorrentLogQuery( get_called_class() );
    }

}

class TorrentLogQuery extends ActiveQuery
{

    public function compact()
    {
        $this->select( [ 'logId' , 'logDate' , 'logType' , 'logLink' , 'logValue' ] )->inverseOf( NULL );
        return $this;
    }
}