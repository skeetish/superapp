<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property Exercise[] affectedExercises
 * @property Exercise[] sourceExercises
 * @property mixed      sourceDayId
 */
class SchemeDayLink extends ActiveRecord
{

    public static function tableName()
    {
        return 'scheme_day_link';
    }

    public function getDay()
    {
        return $this->hasOne(SchemeDay::className(), ['id' => 'dayId']);
    }

    public function getSourceDay()
    {
        return $this->hasOne(SchemeDay::className(), ['id' => 'sourceDayId']);
    }

    public function getSourceExercises()
    {
        return $this->hasMany(Exercise::className(), ['exerciseId' => 'exerciseId'])
            ->joinWith('training')
            ->where('training.dayId = :day', [':day' => $this->sourceDayId]);
    }

    public function getLastSourceExercise()
    {
        return $this->hasOne(Exercise::className(), ['exerciseId' => 'exerciseId'])
            ->joinWith('training')
            ->orderBy(['training.date' => SORT_DESC])
            ->where('training.dayId = :day', [':day' => $this->sourceDayId]);
    }

}

