<?php
namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property bool                 $customInterval
 * @property integer              $weightIncrease
 * @property SchemeDay[]          $days
 * @property integer              $intervalTime
 * @property SchemeModifier[]     modifiers
 * @property string               $label
 * @property string               $schemeId
 * @property bool                 $separateModifiers
 * @property Training[]           trainings
 * @property mixed                id
 * @property SchemeDayLink[]      links
 */
class Scheme extends ActiveRecord
{
    public static function tableName()
    {
        return 'scheme';
    }

    public static function find()
    {
        return new SchemeQuery(get_called_class());
    }

    public function fixedIntervalDate()
    {
        //todo сделать возможность даты без привязки к дням недели.
        return 123;
    }

    /**
     * @param $dates
     *
     * @return array
     */
    public function findClosestDate($dates)
    {
        $minFromNow = min($dates['nextFromNow']);
        $minFromDate = min($dates['nextFromDate']);

        if ($minFromNow > $minFromDate) {
            $nextDate = $minFromNow;
        } else {
            $nextDate = $minFromDate;
        }

        return $nextDate;
    }

    public function getTrainings()
    {
        return $this->hasMany(Training::className(), ['schemeId' => 'id'])->inverseOf('scheme');
    }

    public function getDays()
    {
        return $this->hasMany(SchemeDay::className(), ['schemeId' => 'id'])->inverseOf('scheme');
    }

    public function getModifiers()
    {
        return $this->hasMany(SchemeModifier::className(), ['schemeId' => 'id'])->inverseOf('scheme');
    }

    public function generateNewTrain()
    {
        $training = new Training();
        if ($params = $this->calcNextTrainParams()) {

            $training->populateRelation('modifier', $params['modifier']);
            $training->populateRelation('day', $params['day']);
            $training->populateRelation('exercises', $params['exercises']);
            $training->populateRelation('scheme', $this);

            $training->modifierId = $training->modifier->id;
            $training->schemeId = $training->scheme->id;
            $training->dayId = $training->day->id;
            $training->date = $params['date'];
        }
        return $training;
    }

    public function calcNextDay(Training $training)
    {
        $params = [];

        $currDate = $training
            ? $training->date
            : time();

        if ($this->customInterval && $this->days)
            $params = $this->customIntervalDate($currDate, $this->days);
        else if ($this->intervalTime)
            $params = $this->fixedIntervalDate($training->date);

        return $params;

    }

    /**
     * @param Training         $training
     * @param SchemeModifier[] $modifiers
     *
     * @return SchemeModifier
     */
    public function calcNextModifier(Training $training, $modifiers)
    {
        $modifier = false;

        foreach ($modifiers as $k => $modifier) {
            if ($modifier->id == $training->modifierId) {
                $modifier = isset($modifiers[$k+1])
                    ? $modifiers[$k+1]
                    : $modifiers[0];
                break;
            }
        }

        return $modifier;
    }

    public function getLinks()
    {
        return $this->hasMany(SchemeDayLink::className(), ['dayId' => 'id'])
            ->via('days')
            ->indexBy('exerciseId');
    }

    /**
     * @param Exercise[]     $exercises
     * @param SchemeModifier $modifier
     *
     * @return Exercise[]
     */
    public function calcNextTrainExercises($exercises, SchemeModifier $modifier)
    {
        $newExercises = [];
        foreach ($exercises as $exercise) {

            $newExercise = $this->calcNewExercise($modifier, $exercise);
            unset($newExercise->id);
            unset($newExercise->comment);
            $newExercise->completed = 1;
            $newExercises[] = $newExercise;
        }
        return $newExercises;
    }

    /**
     * @param             $date
     * @param SchemeDay[] $days
     *
     * @return array
     */
    public function customIntervalDate($date, array $days)
    {
        $dates = [];
        $nextDay = false;
        foreach ($days as $k => $day) {

            if (!$day->expression)
                continue;

            $closestDay = strtotime('this '.$day->expression, time());
            $nextDay = strtotime('next '.$day->expression, time());

            $dates['nextFromNow'][] = $closestDay != $date
                ? $closestDay
                : $nextDay;

            $dates['nextFromDate'][] = strtotime('next '.$day->expression, $date);
        }

        $newDate = $this->findClosestDate($dates);

        foreach ($dates as $category) {
            $nextDay = array_search($newDate, $category);
            if ($nextDay !== false)
                break;
        }
        return [
            'day'  => ArrayHelper::getValue($days, $nextDay),
            'date' => $newDate,
        ];

    }

    /**
     * @param $params
     *
     * @return int
     */
    public function calcSeparate($params)
    {
        $newModifier = null;
        $newExercises = [];

        if ($prevSameDayTrain = $this->getLastTraining($params['day']->id)) {

            $newModifier = $this->calcNextModifier($prevSameDayTrain, $this->modifiers);
            if ($this->separateModifiers || $newModifier->weightIncrease) {
                $newExercises = $this->calcNextTrainExercises($prevSameDayTrain->exercises, $newModifier);
            }
        }
        return [
            'modifier'  => $newModifier,
            'exercises' => $newExercises,
        ];
    }

    public function calcNonSeparate(Training $prevTrain)
    {
        $newModifier = $this->calcNextModifier($prevTrain, $this->modifiers);
        $newExercises = $this->calcNextTrainExercises($prevTrain->exercises, $newModifier);
        return [
            'modifier'  => $newModifier,
            'exercises' => $newExercises,
        ];
    }

    /**
     * @return array
     */
    public function calcNextTrainParams()
    {
        if (!$prevTrain = $this->getLastTraining())
            return [];

        $trainParams = $this->calcNextDay($prevTrain);

        if ($this->separateModifiers) {
            $params = $this->calcSeparate($trainParams);
        } else {
            $params = $this->calcNonSeparate($prevTrain);
        }

        return ArrayHelper::merge($trainParams, $params);
    }

    /**
     * @param null $day
     * @param null $userId
     *
     * @return Training
     */
    public function getLastTraining($day = null, $userId = null)
    {
        return Training::find()->last($this->id, $day)->one();
    }

    /**
     * @param SchemeModifier $modifier
     * @param                $exercise
     *
     * @return Exercise
     */
    public function calcNewExercise(SchemeModifier $modifier, $exercise)
    {
        If (($link = ArrayHelper::getValue($this->links, $exercise->exerciseId)) && $link->dayId == $exercise->training->dayId) {
            $newExercise = $link->lastSourceExercise;
            $newExercise->weight *= $link->modifier;
        } else {
            $newExercise = clone $exercise;
            if ($modifier->weightIncrease && $exercise->completed) {
                $newExercise->weight += $newExercise->params->weightStep;
            }

        }
        return $newExercise;
    }

}

class SchemeQuery extends ActiveQuery
{
    public function byUser($userId)
    {
        $this->andWhere(['userId' => $userId]);
        return $this;
    }

}