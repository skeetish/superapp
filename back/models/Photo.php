<?php

namespace app\models;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property integer $date
 * @property string  $path
 * @property string  $original
 */
class Photo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;

    public static function tableName()
    {
        return 'gallery';
    }

    public static function getImageLink($path, $isThumb = false)
    {
        $imageLink = self::getImageDir($isThumb).$path;
        return $imageLink;
    }

    public static function getImageDir($isThumb = false)
    {
        $dirName = ($isThumb)
            ? 'thumbs'
            : 'pics';

        $imageDir = '/images/'.$dirName.'/';
        return $imageDir;
    }

    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    Photo::EVENT_BEFORE_INSERT => 'date',
                    Photo::EVENT_BEFORE_UPDATE => 'date',
                ],
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $imagine = new Imagine();
        $imagine->open(yii::getAlias('@webroot').self::getImageDir().$this->path)
            ->thumbnail(new Box(200, 200), ImageInterface::THUMBNAIL_OUTBOUND)
            ->save(yii::getAlias('@webroot').self::getImageDir(true).$this->path);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'original'], 'required'],
            [['date'], 'integer'],
            [['path', 'original'], 'string', 'max' => 255]
        ];
    }

}
