<?php

namespace app\models;

use app\common\components\CurlRequest;
use app\common\libs\Torrent as TorrentParser;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "torrents".
 *
 * @property integer         $torrentId
 * @property string          $torrentUrl
 * @property string          $torrentParseDate
 * @property string          $torrentCreateDate
 * @property string          $torrentChangeDate
 * @property string          $torrentFilePath
 * @property string          $torrentHash
 * @property string          $torrentName
 * @property int             torrentUserId
 * @property TorrentFile[]   torrentFiles
 */
class Torrent extends ActiveRecord
{
    CONST STATUS_ACTIVE = 0;
    CONST STATUS_HIDDEN = 1;

    CONST EVENT_NOT_CHANGED        = 0;
    CONST EVENT_CHANGED_ATTRIBUTES = 1;
    public static $logging = array ();

    public $logIgnore = ['torrentParseDate', 'torrentCreateDate', 'torrentChangeDate', 'torrentHash'];
    public $initialParse;

    private $tmpFile;
    private $cookiePath;

    public static function refreshTorrents($torrentModels)
    {
        $changes = [];
        if (!is_array($torrentModels))
            $torrentModels = [$torrentModels];

        /** @var $torrentModel Torrent */
        foreach ($torrentModels as $torrentModel)
            if ($change = $torrentModel->refreshTorrent())
                $changes[$torrentModel->torrentId] = $change;

        return $changes;
    }

    public static function tmpFilepath()
    {
        return self::buildPath('temp');

    }

    public static function buildPath($fileName)
    {
        return Yii::getAlias('@uploads').'/'.$fileName.'.torrent';
    }

    /**
     * @inheritdoc
     * @return TorrentQuery
     */
    public static function find()
    {
        return new TorrentQuery(get_called_class());
    }

    public static function tableName()
    {
        return 'torrent';
    }

    public function refreshTorrent()
    {
        $torrentUrl = $this->torrentUrl;
        if (!$this->fetchTorrentFile($torrentUrl, self::tmpFilepath()))
            return false;

        $parsedData = $this->parseTorrentFile($torrentUrl, self::tmpFilepath());
        if (!$parsedData)
            return false;

        ArrayHelper::multisort($parsedData['torrentFiles'], 'fileName');
        $this->scenario = 'parse';
        $this->attributes = $parsedData;

        if (!$this->dirtyAttributes) {
            $this->save(false);
            return false;
        }
        if (!$this->torrentFiles)
            $this->initialParse = true;

        $this->saveTempFile($parsedData['torrentFilePath']);
        return $this->buildChangeList($parsedData, $this->torrentFiles);

    }

    public function fetchTorrentFile($torrentUrl, $destination)
    {
        $i = 1;
        do {
            $result = $this->performFetch($torrentUrl, $destination);
            if ($result)
                return true;

            $this->performTrackerLogin();
            $i++;

        } while ($i >= 3);
        return false;
    }

    public function parseTorrentFile($source)
    {
        $torrent = new TorrentParser($source);

        $data = [
            'torrentName'     => $torrent->name(),
            'torrentSize'     => $torrent->size(),
            'torrentFilePath' => md5($torrent->name()),
            'torrentHash'     => md5(file_get_contents($source))
        ];

        if ($files = $torrent->content()) {
            foreach ($files as $name => $size) {

                $data['torrentFiles'][] = [
                    'fileName' => basename($name),
                    'filePath' => $name,
                    'fileSize' => $size,
                ];
            }
        }
        return $data;
    }

    public function saveTempFile($to)
    {
        $destTorrentPath = self::buildPath($to);
        $srcFilepath = self::tmpFilepath();
        file_put_contents($destTorrentPath, file_get_contents($srcFilepath));
    }

    public function buildChangeList($incomingData, $currentData)
    {
        $torrentFiles = ArrayHelper::toArray($currentData, [
            'app\models\TorrentFile' => ['fileName', 'filePath', 'fileSize']
        ]);

        $income = ArrayHelper::index($incomingData['torrentFiles'], function ($value) {
            return $value['fileName'].':'.$value['fileSize'];
        });

        $current = ArrayHelper::index($torrentFiles, function ($value) {
            return $value['fileName'].':'.$value['fileSize'];
        });

        // Work in Progress
        $changeData = [
            'deleted' => array_diff_assoc($current, $income),
            'added'   => array_diff_assoc($income, $current)
            //            'deleted' => [],
            //            'added'   => []
        ];

        $changesList = [];

        /** @var TorrentFile[] $currentData */
        foreach ($currentData as $file) {
            $index = $file->fileName.':'.$file->fileSize;
            if (isset($changeData['deleted'][$index])) {
                $file->delete();
                $fileData = $changeData['deleted'][$index];
                $fileData['type'] = 'deleted';
                $changesList[] = $fileData;
            }
        }

        foreach ($changeData['added'] as $fileData) {
            $fileModel = new TorrentFile();
            $fileModel->attributes = $fileData;
            $fileModel->initialParse = $this->initialParse;
            $this->link('torrentFiles', $fileModel);
            $fileData['type'] = 'added';
            $changesList[] = $fileData;
        }
        return $changesList;
    }

    public function performFetch($source, $destination)
    {
        $curl = Yii::$app->factory->getCurlInstance();
        $curl->setOption(CURLOPT_COOKIESESSION, true);

        $cookieHeader = $this->extractCookie(CurlRequest::getCookiePath(), $this->torrentUrl);
        $curl->setOption(CURLOPT_COOKIE, $cookieHeader);
        $content = $curl->makeRequest($source);
        file_put_contents($destination, $content);

        return TorrentParser::is_torrent($destination);
    }

    public function performTrackerLogin()
    {
        $postData = [
            'login_username' => 'Stickah',
            'login_password' => 'EswLi',
            'login'          => 'Вход',
        ];

        $curl = Yii::$app->factory->getCurlInstance();
        $curl->allowCookieWrite();
        $curl->makeRequest('http://login.rutracker.org/forum/login.php', $postData, true);

    }

    public function extractCookie($cookiePath, $torrentUrl)
    {
        $file = file_get_contents($cookiePath);
        preg_match('/bb_data\s{1}(.*)\r/', $file, $matches);
        if (!$matches)
            return false;
        $cookies[] = 'bb_data='.$matches[1];
        preg_match('/\?t=(\d+)/', $torrentUrl, $matches);
        $cookies[] = 'bb_dl='.$matches[1];
        return implode(';', $cookies);
    }

    public function getTorrentPath()
    {
        return self::buildPath($this->torrentFilePath);
    }

    public function init()
    {
        $this->tmpFile = Yii::getAlias('@uploads').'/temp.torrent';
        $this->cookiePath = __DIR__.'/../runtime/cookies.txt';
        parent::init();
    }

    public function scenarios()
    {
        return [
            'default' => ['torrentUrl'],
            'parse'   => ['torrentName', 'torrentHash', 'torrentFilePath'],
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert)
            $this->torrentCreateDate = Time();
        if ($this->dirtyAttributes)
            $this->torrentChangeDate = Time();

        $this->torrentParseDate = Time();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {

        if ($insert) {
            $this->refreshTorrent();
            $this->save();
        }

        if (!$this->initialParse) {
            foreach ($changedAttributes as $key => $value) {
                if (!in_array($key, $this->logIgnore)) {
                    $logEntry = new TorrentLog();
                    $logEntry->logOldValue = $value;
                    $logEntry->logValue = $this->$key;
                    $logEntry->logType = TorrentLog::TYPE_TORRENT_CHANGE;
                    $logEntry->logAttribute = $key;
                    $logEntry->logLink = $this->torrentUrl;
                    $logEntry->save();
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function getTorrentFiles()
    {
        return $this->hasMany(TorrentFile::className(), ['torrentId' => 'torrentId'])->inverseOf('torrent');
    }

    public function getTorrentLogs()
    {
        return $this->hasMany(TorrentLog::className(), ['logLink' => 'torrentUrl'])->inverseOf('torrent');
    }

    public function getTorrentLink()
    {
        return '/uploads/'.$this->torrentFilePath.'.torrent';
    }
}

class TorrentQuery extends ActiveQuery
{
    public static $compactAttributes = [
        'torrentId',
        'torrentName',
        'torrentFilePath',
        'torrentUrl',
        'torrentChangeDate',
        'torrentCreateDate',
        'torrentHash'
    ];

    public function byUser($user = null)
    {
        if (!$user)
            $user = Yii::$app->user->id;

        $this->andWhere(['torrentUserId' => $user]);
        return $this;
    }

    public function active()
    {
        $this->andWhere(['torrentStatus' => Torrent::STATUS_ACTIVE]);
        return $this;
    }

    public function compact()
    {
        $this->select(self::$compactAttributes)->inverseOf(null);
        return $this;
    }
}