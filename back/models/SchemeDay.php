<?php

namespace app\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property mixed   expression
 */
class SchemeDay extends ActiveRecord
{

    public static function tableName()
    {
        return 'scheme_day';
    }

    public static function find()
    {
        return new SchemeDayQuery(get_called_class());
    }

    public function getScheme()
    {
        return $this->hasOne(Scheme::className(), ['schemeId' => 'id'])->inverseOf('days');
    }

    public function getLinks()
    {
        return $this->hasMany(SchemeDayLink::className(), ['id' => 'dayId']);
    }

}

class SchemeDayQuery extends ActiveQuery
{
    public function compact()
    {
        $this->select([
            'id',
            'label',
            'expression',
        ])
            ->inverseOf(null);
        return $this;
    }

}