<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trn_exercise_names".
 *
 * @property integer $exerciseId
 * @property string  $label
 * @property double  $weightStep
 */
class ExerciseName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise_name';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercises()
    {
        return $this->hasOne(Exercise::className(), ['exerciseId' => 'id'])->inverseOf('params');
    }
}
