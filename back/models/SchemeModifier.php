<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property mixed   weightIncrease
 * @property mixed   value
 */
class SchemeModifier extends ActiveRecord
{

    public static function tableName()
    {
        return 'scheme_modifier';
    }

    public function getScheme()
    {
        return $this->hasOne(Scheme::className(), ['schemeId' => 'id'])->inverseOf('modifiers');
    }

}
