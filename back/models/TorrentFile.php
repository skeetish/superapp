<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "torrent_files".
 * @property integer   $fileId
 * @property string    $fileName
 * @property string    $filePath
 * @property string    $fileSize
 * @property Torrent   torrent
 */
class TorrentFile extends \yii\db\ActiveRecord
{
    public $logIgnore = [ ];
    public $initialParse;

    public function scenarios()
    {
        return [
            'default' => [ 'fileName' , 'filePath' , 'fileSize' ]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className() ,
                'createdAtAttribute' => 'fileCreated' ,
                'updatedAtAttribute' => 'fileUpdated' ,
            ] ,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'torrent_file';
    }

    public function getTorrent()
    {
        return $this->hasOne( Torrent::className() , [ 'torrentId' => 'torrentId' ] )->inverseOf( 'torrentFiles' );
    }

    public function afterSave( $insert , $changedAttributes )
    {
        if ( $insert && !$this->initialParse ) {
            $logEntry = new TorrentLog();
            $logEntry->logValue = $this->fileName;
            $logEntry->logType = TorrentLog::TYPE_FILE_ADD;
            $logEntry->logLink = $this->torrent->torrentUrl;
            $logEntry->save();
        }
        parent::afterSave( $insert , $changedAttributes );
    }

    public function afterDelete()
    {
        $logEntry = new TorrentLog();
        $logEntry->logValue = $this->fileName;
        $logEntry->logType = TorrentLog::TYPE_FILE_DELETE;
        $logEntry->logLink = $this->torrent->torrentUrl;
        $logEntry->save();
        parent::afterDelete();
    }

    public static function find()
    {
        return new TorrentFileQuery( get_called_class() );
    }

}

class TorrentFileQuery extends ActiveQuery
{

    public function compact()
    {
        $this->select( [ 'fileId' , 'fileName' , 'fileSize' , 'torrentId' , 'fileCreated' ] )->inverseOf( NULL );
        return $this;
    }
}