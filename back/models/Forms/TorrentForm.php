<?php

namespace app\models\forms;

use app\models\Torrent;
use yii\base\Model;

/**
 * @property Torrent model
 * @package app\models\forms
 */
class TorrentForm extends Model
{
    public $torrentUrl;
    public $torrentId;
    public $model;

    public function rules()
    {
        return [
            [ [ 'torrentUrl' ] , 'required' ] ,
            [ [ 'torrentUrl' ] , 'url' ] ,
            [ [ 'torrentUrl' ] , 'unique' , 'targetClass' => 'app\models\Torrent' , 'targetAttribute' => 'torrentUrl' ] ,
        ];
    }

    public function saveTorrent()
    {
        $this->model = ( $this->torrentId === NULL )
            ? new Torrent()
            : Torrent::findOne(['id' => $this->torrentId]);

        $this->model->attributes = $this->attributes;
        $this->model->torrentUserId = 2;
        $this->model->save();

        return ( $this->model->errors )
            ? $this->model->errors
            : TRUE;
    }

}
