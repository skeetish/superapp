<?php
namespace app\models\forms;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\web\Cookie;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Неправильный логин или пароль.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            yii::$app->user->setIdentity($this->getUser());
            yii::$app->response->cookies->add(new Cookie([
                'name'   => 'login',
                'value'  => $this->username,
                'expire' => strtotime('next Sunday'),

            ]));
            yii::$app->response->cookies->add(new Cookie([
                'name'   => 'password', 'value' => $this->password,
                'expire' => strtotime('next Sunday'),

            ]));
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
