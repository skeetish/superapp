<?php

namespace app\models\forms;

use app\models\Exercise;
use app\models\ExerciseName;
use app\models\Scheme;
use app\models\Training;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Class NewExerciseForm
 * @package app\models\forms
 */
class NewCommentForm extends Model
{

    public $id, $comment;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['comment'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'comment' => 'Комментарий',
        ];
    }

    public function CreateExercise()
    {
        $exercise = [ ];
        if ($params = ExerciseName::FindOne(['exerciseId' => $this->exerciseId])) {

            $exerciseModel = new Exercise;
            $trainingModel = new Training;
            $schemeModel = Scheme::Find()->where(['alias' => $this->alias])->one();

            $trainingModel->populateRelation( 'scheme' , $schemeModel );
            $exerciseModel->populateRelation('params', $params);
            $exerciseModel->populateRelation( 'training' , $trainingModel );

            $exerciseModel->attributes = $this->attributes;
            $url = Url::toRoute( [
                '/backend/trainings/exercise' ,
                'alias' => $params->alias,
                'alias' => $schemeModel->alias,
            ] );

            $exercise[ 'weights' ] = $exerciseModel->calculateWeights();
            $exercise['params'] = $params->toArray();
            $exercise['params']['completed'] = 1;
            $exercise[ 'site' ] = [ 'url' => $url ];

        }
        return $exercise;

    }

}
