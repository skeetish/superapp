<?php

namespace app\models\forms;

use app\common\components\helpers\Text;
use app\models\Exercise;
use app\models\ExerciseName;
use app\models\Scheme;
use app\models\Training;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Class NewExerciseForm
 * @package app\models\forms
 */
class NewExerciseForm extends Model
{

    public $modifier, $alias;
    public $exerciseId, $count, $weight, $reps, $increaseType;
    public $weightStep;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [ [ 'exerciseId' ] , 'required' , 'message' => Text::Select( 'упражнение' ) ] ,
            [['count'], 'required', 'message' => Text::Enter('подходы')],
            [['weight'], 'required', 'message' => Text::Enter('вес')],
            [['reps'], 'required', 'message' => Text::Enter('повторы')],
            [['increaseType'], 'required', 'message' => Text::Select('тип увеличения')],
            [['modifier', 'alias'], 'required'],
            [['modifier'], 'integer'],
            [
                [
                    'exerciseId', 'count', 'reps',
                    'weight', 'modifier', 'increaseType'
                ] , 'number'
            ] ,
        ];
    }

    public function attributeLabels()
    {
        return [
            //            'count' => Yii::t( 'app' , 'Exercise Count' ) ,
            'count'  => 'Подходы',
            //            'weight' => Yii::t( 'app' , 'Exercise Weight' ) ,
            'weight' => 'Вес',
            //            'reps' => Yii::t( 'app' , 'Exercise Reps' ) ,
            'reps'   => 'Повторы',
        ];
    }

    public function CreateExercise()
    {
        $exercise = [ ];
        if ($params = ExerciseName::FindOne(['exerciseId' => $this->exerciseId])) {

            $exerciseModel = new Exercise;
            $trainingModel = new Training;
            $schemeModel = Scheme::Find()->where(['alias' => $this->alias])->one();

            $trainingModel->populateRelation( 'scheme' , $schemeModel );
            $exerciseModel->populateRelation('params', $params);
            $exerciseModel->populateRelation( 'training' , $trainingModel );

            $exerciseModel->attributes = $this->attributes;
            $url = Url::toRoute( [
                '/backend/trainings/exercise' ,
                'alias' => $params->alias,
                'alias' => $schemeModel->alias,
            ] );

            $exercise[ 'weights' ] = $exerciseModel->calculateWeights();
            $exercise['params'] = $params->toArray();
            $exercise['params']['completed'] = 1;
            $exercise[ 'site' ] = [ 'url' => $url ];

        }
        return $exercise;

    }

}
