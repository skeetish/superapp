<?php

namespace app\models\forms;

use app\models\Photo;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class FileUploadForm extends Model
{
    public $_uploadArray;
    public $_uploadFile;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [ '_uploadFile' , 'file' , 'extensions' => 'jpg,png' ] ,
        ];
    }

    public function loadFiles( $files )
    {
        if ( is_array( $files ) )
            $this->_uploadArray = $files;
        else
            $this->loadFile( $files );

    }

    public function loadFile( UploadedFile $file )
    {
        $this->_uploadArray = [ $file ];
    }

    public function saveFiles()
    {
        $data = [ ];
        if ( $this->_uploadArray ) {
            foreach ( $this->_uploadArray as $uploadFile ) {

                $this->_uploadFile = $uploadFile;
                if ( $this->validate( '_uploadFile' ) ) {

                    $fileName = substr( md5( $this->_uploadFile->baseName . time() ) , 0 , 13 ) . '.' . $this->_uploadFile->extension;
                    $filePath = yii::getAlias( '@webroot' ) . Photo::getImageDir() . $fileName;

                    if ( $this->_uploadFile->saveAs( $filePath ) ) {
                        $data[ 'files' ] = $filePath;
                    }
                } else
                    $data[ 'errors' ] = $this->errors;

            }
        }
        return $data;
    }

}
