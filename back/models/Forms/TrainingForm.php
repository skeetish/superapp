<?php

namespace app\models\forms;

use app\models\Exercise;
use app\models\Training;
use yii\base\Model;

/**
 * @property Training model
 * @package app\models\forms
 */
class TrainingForm extends Model
{
    public $modifier, $day, $date;
    public $exercises;
    public $id = null;
    public $model;

    public function rules()
    {
        return [
            [['modifier', 'day', 'date'], 'integer'],
            [['modifier', 'day', 'date'], 'required'],
            [['exercises', 'id'], 'safe'],
        ];
    }

    public function saveTraining()
    {
        $this->model = ($this->id === null)
            ? new Training()
            : Training::findOne(['id' => $this->id]);

        $this->model->attributes = $this->attributes;
        $this->model->schemeNumber = 1;

        if ($this->model->save() && $this->exercises) {

            if ($this->model->exercises)
                $this->model->unlinkAll('exercises', true);

            foreach ($this->exercises as $exerciseData) {
                $exerciseModel = new Exercise();
                $exerciseModel->attributes = $exerciseData;
                $this->model->link('exercises', $exerciseModel);
            }
        }
        return ($this->model->errors)
            ? $this->model->errors
            : true;
    }

}
