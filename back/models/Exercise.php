<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * @property integer         $id
 * @property integer         $trainId
 * @property integer         $exerciseId
 * @property integer         $count
 * @property double          $weight
 * @property integer         $exerciseOrder
 * @property integer         $increaseType
 * @property integer         $reps
 * @property bool            $completed
 * @property mixed           comment
 * @property mixed           params
 * @property Training        training
 * @property Exercise        parentExercise
 * @static Exercise findOne()
 * *
 */
class Exercise extends \yii\db\ActiveRecord
{

    const INCREASE_LINEAR  = 0;
    const INCREASE_PYRAMID = 1;
    const INCREASE_REV_PYRAMID = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise';
    }

    public static function find()
    {
        return new ExerciseQuery(get_called_class());
    }

    public static function increaseTypes()
    {
        return [
            self::INCREASE_LINEAR      => 'Линейное',
            self::INCREASE_PYRAMID     => 'Пирамида',
            self::INCREASE_REV_PYRAMID => 'Обратная пирамида',
        ];
    }

    public function scenarios()
    {
        return [
            'default' => [
                'exerciseId',
                'count',
                'weight',
                'increaseType',
                'reps',
                'completed',
                'comment'
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (!$this->exerciseOrder)
            $this->exerciseOrder = 0;
        if ($this->completed === null)
            $this->completed = 1;

        return parent::beforeSave($insert);
    }

    public function getIncreaseTypeText()
    {
        return self::increaseTypes()[$this->increaseType];
    }

    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['id' => 'trainId'])->inverseOf('exercises');
    }

    public function getParams()
    {
        return $this->hasOne(ExerciseName::className(), ['id' => 'exerciseId'])->inverseOf('exercises');
    }

}

class ExerciseQuery extends ActiveQuery
{

    public function compact()
    {
        $this->select([
            'id',
            'trainId',
            'exerciseId',
            'count',
            'weight',
            'increaseType',
            'reps',
            'completed',
            'comment',
        ])
            ->inverseOf(null);
        return $this;
    }
}