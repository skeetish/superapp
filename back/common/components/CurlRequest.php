<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 09.12.2014
 * Time: 20:49
 */

namespace app\common\components;

use Yii;

class CurlRequest
{
    private static $cookiePath;
    private $instance;

    public function __construct()
    {
        $this->instance = curl_init();

        $this->setOption(CURLOPT_USERAGENT, 'User-Agent: Mozilla/4.0 (compatible; MSIE 5.01; Widows NT)');
        $this->setOption(CURLOPT_RETURNTRANSFER, true);

    }

    public function setOption($option, $value)
    {
        curl_setopt($this->instance, $option, $value);
    }

    public function allowCookieWrite()
    {
        $this->setOption(CURLOPT_COOKIESESSION, true);
        $this->setOption(CURLOPT_COOKIEJAR, self::getCookiePath());

    }

    public static function getCookiePath()
    {
        if (!self::$cookiePath)
            self::$cookiePath = Yii::$app->runtimePath.'/cookies.txt';

        return self::$cookiePath;
    }

    public function allowCookieRead()
    {
        $this->setOption(CURLOPT_COOKIESESSION, true);
        $this->setOption(CURLOPT_COOKIEFILE, self::getCookiePath());
    }

    public function makeRequest($url, $params = array (), $isPost = false)
    {
        if ($params) {
            if ($isPost) {
                $this->setOption(CURLOPT_POST, 1);
                $this->setOption(CURLOPT_POSTFIELDS, $params);
            }else
                $url .= '?'.http_build_query($params);
        }

        $this->setOption(CURLOPT_URL, $url);

        $result = curl_exec($this->instance);
        curl_close($this->instance);
        return $result;
    }

} 