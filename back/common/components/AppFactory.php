<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 09.12.2014
 * Time: 23:13
 */

namespace app\common\components;

use yii\base\Component;

class AppFactory extends Component
{

    public function getCurlInstance()
    {
        return new CurlRequest();
    }

}