<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 15.12.2014
 * Time: 23:17
 */

namespace app\common\components;

use app\common\base\AppServiceLayer;
use yii\base\Component;

class LayerFactory extends Component
{
    private $classMap;

    public function getInstance($class)
    {
        $class = $this->normalizeClassname($class);

        if (isset($this->classMap[$class]))
            return $this->classMap[$class];

        if (class_exists($class)) {
            $object = new $class();
            if ($object instanceof AppServiceLayer) {
                $this->classMap[$class] = $object;
                return $this->classMap[$class];
            }
        }

    }

    private function normalizeClassname($class)
    {
        $shortname = basename($class);
        return $shortname == $class
            ? "\\app\\rest\\services\\".$shortname
            : $class;
    }

} 