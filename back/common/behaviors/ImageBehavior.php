<?php

class ImageBehavior extends CActiveRecordBehavior
{
    public $file;
    public $sourceDir = 'pics';
    public $thumbDir = 'thumbs';

    public $_sourceLink , $_thumbLink , $fileName , $pathAttribute;


    //Yii::app()->baseUrl
    public function getSourcePath()
    {
        return Yii::getPathOfAlias( 'webroot.images.' . $this->sourceDir . '/' );
    }

    public function getThumbPath()
    {
        return Yii::getPathOfAlias( 'webroot.images.' . $this->thumbDir . '/' );
    }

    public function getThumbLink()
    {
        return Yii::app()->baseUrl . '/images/' . $this->thumbDir . '/' . $this->fileName;
    }

    public function getSourceLink()
    {
        return Yii::app()->baseUrl . '/images/' . $this->sourceDir . '/' . $this->fileName;
    }

    public function beforeSave($event)
    {
        if ( !file_exists( $this->sourcePath ) )
            mkdir( $this->sourcePath , 0777 , TRUE );

        $this->fileName = substr( md5( microtime() ) , 0 , 13 ) . '.' . $this->file->extensionName;
        if ( $this->file->saveAs( $this->sourcePath . $this->fileName ) )
            $this->owner->{$this->pathAttribute} = $this->fileName;
        else
            $event->isValid = FALSE;

    }

    public function afterSave($event)
    {
        if ( !file_exists( $this->thumbPath ) )
            mkdir( $this->thumbPath , 0777 , TRUE );

        $thumb = Yii::app()->phpThumb->create( $this->sourcePath . $this->fileName );
        $thumb->adaptiveResize( 170 , 170 );
        $thumb->save( $this->thumbPath . $this->fileName );

    }

    public function afterDelete($event)
    {
        if ( file_exists( $this->thumbPath . $this->fileName ) )
            unlink( $this->thumbPath . $this->fileName );
        if ( file_exists( $this->sourcePath . $this->fileName ) )
            unlink( $this->sourcePath . $this->fileName );
    }
}