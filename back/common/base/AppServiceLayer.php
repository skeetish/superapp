<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 12.12.2014
 * Time: 2:49
 */

namespace app\common\base;

use Yii;

abstract class AppServiceLayer
{
    protected static $_class = __CLASS__;

    public static function className()
    {
        return static::$_class;
    }

}