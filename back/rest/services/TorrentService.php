<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 15.08.2015
 * Time: 14:14
 */

namespace app\rest\services;

use app\common\base\AppServiceLayer;
use app\models\Torrent;
use app\models\TorrentQuery;

class TorrentService extends AppServiceLayer
{

    public function create($data, $userId)
    {
        $model = new Torrent();

        $model->attributes = $data;
        $model->torrentUserId = $userId;

        $model->save();
        $model->refresh();

        return [$model, ['torrentFiles', 'torrentLogs']];
    }

    public function parseTorrents()
    {

        /** @var $torrentModels Torrent[] */
        $torrentModels = Torrent::Find()
            ->active()
            ->compact()
            ->indexBy('torrentId')
            ->all();

        $fileChanges = Torrent::refreshTorrents($torrentModels);

        $attributes = [];
        foreach ($fileChanges as $id => $change) {
            $model = $torrentModels[$id];
            $attributes[$id] = $model->getAttributes(TorrentQuery::$compactAttributes);
        }

        return [
            'changes'    => $fileChanges,
            'attributes' => $attributes,
        ];
    }
}
