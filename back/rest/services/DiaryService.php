<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 13.12.2014
 * Time: 3:01
 */

namespace app\rest\services;

use app\common\base\AppServiceLayer;
use app\models\Exercise;
use app\models\ExerciseName;
use app\models\Scheme;
use app\models\Training;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class DiaryService extends AppServiceLayer
{

    public function findTraining($id)
    {
        return Training::find()
            ->where(['id' => $id])
            ->one();
    }

    /**
     * @param $schemeId
     * @param $user
     *
     * @return Scheme
     */
    public function findScheme($schemeId, $user)
    {
        return Scheme::find()->where(['id' => intval($schemeId)])->byUser($user)->one();
    }

    public function getList($schemeNumber)
    {
        $data = Training::find()
            ->compact()
            ->where(['schemeId' => $schemeNumber])
            ->orderBy(['date' => SORT_DESC])
            ->with(['exercises' => function ($query) { return $query->compact(); }])
            ->all();

        foreach ($data as &$object) {
            if ($object instanceof ActiveRecord)
                $object = $object->toArray([], array_keys($object->relatedRecords));
        }

        return $data;

    }

    public function getData()
    {
        $data = [
            'params'        => ArrayHelper::index(ExerciseName::find()->all(), 'id'),
            'increaseTypes' => Exercise::increaseTypes(),
            'schemes'       => Scheme::find()->with(['days', 'modifiers'])->byUser(Yii::$app->user->id)->all()
        ];

        foreach ($data as &$element) {
            foreach ($element as &$object) {
                if ($object instanceof ActiveRecord)
                    $object = $object->toArray([], array_keys($object->relatedRecords));
            }
        }

        return $data;
    }

    public function updateTraining($id, $attributes)
    {
        if (!$training = $this->findTraining($id))
            return false;

        $training->attributes = $attributes;
        if ($training->save()) {
            if ($exercises = ArrayHelper::getValue($attributes, 'exercises')) {
                $training->unlinkAll('exercises', true);
                foreach ($exercises as $exerciseAttrs) {
                    $exercise = new Exercise();
                    $exercise->attributes = $exerciseAttrs;
                    $exercise->link('training', $training);
                }
            }
            $result = ['errors' => null];
        } else {
            $result = ['errors' => $training->errors];
        }
        return $result;
    }

    public function removeTraining($id)
    {
        if (!$training = $this->findTraining($id))
            return false;

        $training->delete();
        return ['errors' => null];

    }

    public function createTraining($scheme, $attributes)
    {
        $training = new Training();
        $training->attributes = $attributes;
        $training->schemeId = $scheme;

        if ($training->save()) {
            if ($exercises = ArrayHelper::getValue($attributes, 'exercises'))
                foreach ($exercises as $exerciseAttrs) {
                    $exercise = new Exercise();
                    $exercise->attributes = $exerciseAttrs;
                    $exercise->link('training', $training);
                }
            $result = [
                'id'     => $training->id,
                'errors' => null
            ];

        } else {
            $result = ['errors' => $training->errors];
        }
        return $result;
    }

    public function getExercises($schemeNumber, $alias)
    {
        $data = Exercise::find()
            ->compact()
            ->where(['exerciseId' => 1])
            /*->orderBy(['date' => SORT_DESC])
            ->where(['alias' => ':alias'], [':alias' => 'deadlift'])*/
            ->all();

        foreach ($data as &$object) {
            if ($object instanceof ActiveRecord)
                $object = $object->toArray([], array_keys($object->relatedRecords));
        }

        return $data;
    }

}