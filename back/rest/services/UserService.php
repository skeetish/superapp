<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 13.12.2014
 * Time: 3:01
 */

namespace app\rest\services;

use app\common\base\AppServiceLayer;
use app\models\User;
use Yii;

class UserService extends AppServiceLayer
{

    public function login($username, $password)
    {
        $user = User::findByUsername($username);
        if ($user && $user->validatePassword($password)) {

            $user->generateAuthKey();
            $user->save(false, ['userAuthKey']);
            $result = [
                'name'   => $user->userLogin,
                'hash'   => $user->userAuthKey,
                'role'   => $user->userRole,
                'scheme' => $user->userScheme,
                'errors' => null
            ];
        } else {
            $result = [
                'errors' => ['password' => 'Неправильный логин или пароль.']
            ];
        }
        return $result;

    }

}