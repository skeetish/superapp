<?php

namespace app\rest;

use Yii;
use yii\base\Module;

class Rest extends Module
{
    public $controllerNamespace = 'app\rest\controllers';
    public $layout = 'main';

    public function init()
    {
        Yii::$app->setComponents([
            'user' => [
                'identityClass' => 'app\models\User',
                'class' => 'app\rest\components\RestUser',
                'enableSession' => false
            ]
        ]);
        parent::init();
        // custom initialization code goes here
    }
}
