<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 18.03.2015
 * Time: 3:00
 */

namespace app\rest\controllers;

use app\models\Training;
use app\rest\base\AppActiveController;
use app\rest\services\DiaryService;
use Yii;

class TrainingController extends AppActiveController
{
    /**
     * @var DiaryService
     */
    protected $diaryService;

    public function actions()
    {
        return [
            'delete' => [
                'class'       => 'yii\rest\DeleteAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

    public function init()
    {
        $this->modelClass = Training::className();
        $this->diaryService = Yii::$app->services->getInstance('DiaryService');
    }

    public function actionUpdate($id)
    {
        if (($result = $this->diaryService->updateTraining($id, $_REQUEST)) === false)
            $this->notFound();

        return $result;
    }

    /*    public function actionRemove($id)
        {
            if (($result = $this->diaryService->removeTraining($id)) === false)
                $this->notFound();

            return $result;
        }*/



}