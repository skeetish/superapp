<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 18.10.14
 * Time: 21:49
 */

namespace app\rest\controllers;

use app\models\Scheme;
use app\rest\base\AppActiveController;
use app\rest\services\DiaryService;
use Yii;
use yii\filters\auth\HttpBearerAuth;

class SchemeController extends AppActiveController
{
    /**
     * @var DiaryService
     */
    protected $diaryService;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($this->action->id != 'login') {
            $behaviors['authenticator'] = [
                'class' => HttpBearerAuth::className(),
            ];
        }
        return $behaviors;
    }

    public function init()
    {
        $this->modelClass = Scheme::className();
        $this->diaryService = Yii::$app->services->getInstance('DiaryService');
    }

    public function actionInitials()
    {
        return $this->diaryService->getData();
    }

    public function actionGenerate($id)
    {
        if (!$scheme = $this->diaryService->findScheme($id, Yii::$app->user->id))
            $this->notFound();

        return $scheme
            ->generateNewTrain()
            ->toArray([], ['exercises']);
    }

    public function actionExercises($id, $alias)
    {
        if (!$this->diaryService->findScheme($id, Yii::$app->user->id))
            $this->notFound();

        return $this->diaryService->getExercises($id, $alias);
    }

    public function actionTrainings($id)
    {
        if (!$this->diaryService->findScheme($id, Yii::$app->user->id))
            $this->notFound();

        return $this->diaryService->getList($id);
    }

    public function actionCreateTraining($id)
    {
        return $this->diaryService->createTraining($id, $_REQUEST);

    }

}