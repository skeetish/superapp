<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 28.12.2014
 * Time: 1:19
 */

namespace app\rest\controllers;

use app\models\Torrent;
use app\rest\base\AppActiveController;
use app\rest\services\TorrentService;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class TorrentController extends AppActiveController
{
    /**
     * @var TorrentService
     */
    protected $torrentService;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($this->action->id != 'login') {
            $behaviors['authenticator'] = [
                'class' => HttpBearerAuth::className(),
            ];
        }
        return $behaviors;
    }

    public function init()
    {
        $this->modelClass = Torrent::className();
        $this->torrentService = Yii::$app->services->getInstance('TorrentService');
    }

    public function actions()
    {
        return [
            'delete' => [
                'class'       => 'yii\rest\DeleteAction',
                'modelClass'  => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
            'index'  => [
                'class'               => 'yii\rest\indexAction',
                'modelClass'          => $this->modelClass,
                'checkAccess'         => [$this, 'checkAccess'],
                'prepareDataProvider' => function () {
                    return new ActiveDataProvider([
                        'query' => Torrent::find()
                            ->active()
                            ->compact()
                            ->orderBy(['torrentCreateDate' => SORT_DESC])
                            ->with([
                                'torrentFiles' => function ($query) { return $query->compact()->orderBy(['fileName' => SORT_ASC]); },
                                'torrentLogs'  => function ($query) { return $query->compact(); }
                            ])
                            ->asArray()
                    ]);
                }
            ]
        ];
    }

    public function actionParse()
    {
        return $this->torrentService->parseTorrents();
    }

    public function actionCreate()
    {
        if (($result = $this->torrentService->create($_REQUEST, Yii::$app->user->id)) === false)
            $this->notFound();

        return $result;
    }
}