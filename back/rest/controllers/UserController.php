<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 18.10.14
 * Time: 21:49
 */

namespace app\rest\controllers;

use app\rest\base\RestController;
use app\rest\services\UserService;
use Yii;
use yii\helpers\ArrayHelper;

class UserController extends RestController
{

    /**
     * @var UserService
     */
    protected $userService;

    public function init()
    {
        $this->userService = Yii::$app->services->getInstance('UserService');
    }

    public function actionLogin()
    {
        $username = ArrayHelper::getValue($_REQUEST, 'username');
        $password = ArrayHelper::getValue($_REQUEST, 'password');

        if (!$username || !$password)
            $this->badRequest();

        if (($result = $this->userService->login($username, $password)) && $error = $result['errors'])
            $this->notAuthorized($error);

        return $result;
    }

}