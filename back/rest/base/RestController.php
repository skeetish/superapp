<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 28.12.2014
 * Time: 22:58
 */

namespace app\rest\base;

use Yii;
use yii\helpers\Json;
use yii\web\HttpException;

class RestController extends AppActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        //        if ($this->action->id != 'login') {
        //            $behaviors['authenticator'] = [
        //                'class' => HttpBearerAuth::className(),
        //            ];
        //        }
        return $behaviors;
    }

    public function beforeAction($action)
    {
        $_REQUEST = Json::decode(file_get_contents('php://input'), true);
        return parent::beforeAction($action);
    }

    public function notFound()
    {
        throw new HttpException(404, 'Not Found');
    }

    public function badRequest()
    {
        throw new HttpException(400, 'Bad Request');
    }

    public function notAuthorized($message)
    {
        $this->response(401, $message);
    }

    private function response($code, $message)
    {
        Yii::$app->response->setStatusCode($code);
        Yii::$app->response->content = Json::encode(['errors' => $message]);
        Yii::$app->end();
    }

}