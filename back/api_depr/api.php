<?php

namespace app\api;

use yii\base\Module;

class api extends Module
{
    public $controllerNamespace = 'app\api\controllers';
    public $layout = 'main';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
