<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 25.10.14
 * Time: 18:47
 */

namespace app\api\components;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\rest\Controller;

class ApiController extends Controller
{
    public $onlyLogged = true;
    public $serviceLayer;

    public function init()
    {
        //        $this->serviceLayer = ServiceLayerFactory::getLayer( $this->id );
        parent::init();
    }


    public function beforeAction($action)
    {
        if ( $this->onlyLogged && Yii::$app->user->isGuest )
            return [ 'result' => false ];

        $_POST = json_decode( file_get_contents( 'php://input' ), true );
        return Controller::beforeAction( $action );
    }

    protected function activeRecord2Array($data, $relations = [ ], $attributes = [ ])
    {
        $result = ( $data instanceof ActiveRecord || $data instanceof Model )
            ? $this->prepareAr( $data, $relations, $attributes )
            : $data;

        return $result;

    }

    private function prepareAr($model, $relations = [ ], $attributes = [ ])
    {
        if ( $model->errors ) {
            $result = [ 'result' => false, 'errors' => $model->errors ];
        } else {
            $result = [ 'result' => true ];

            if ( $attributes ) {
                foreach ( $attributes as $attribute )
                    $result[ $attribute ] = $model->$attribute;
            } else if ( $attributes !== null )
                $result[ 'attributes' ] = $model->attributes;

            if ( $relations ) {
                foreach ( $relations as $relationName ) {

                    $relationData = $model->$relationName;
                    if ( is_array( $relationData ) ) {
                        $data = [ ];
                        /** @var $relationElement ActiveRecord */
                        foreach ( $relationData as $relationElement )
                            $data[ ] = $relationElement->attributes;
                    } else
                        $data = $relationData;

                    $result[ $relationName ] = $data;
                }
            }

        }
        return $result;
    }

    public function afterAction($action, $result)
    {
        if ( is_array( $result ) && $result[ 0 ] && $result[ 0 ] instanceof Model )
            $result = $this->activeRecord2Array( $result[ 0 ], $result[ 1 ], $result[ 2 ] );

        return parent::afterAction( $action, $result );
    }

}