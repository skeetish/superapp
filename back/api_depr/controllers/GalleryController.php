<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 18.10.14
 * Time: 21:49
 */

namespace app\api\controllers;

use app\models\Photo;
use yii\web\Controller;

class GalleryController extends Controller
{

    public function actionPhotos()
    {
        $data = Photo::find()->asArray()->all();
        echo json_encode( $data );
    }
} 