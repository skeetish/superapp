<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 18.10.14
 * Time: 21:49
 */

namespace app\api\controllers;

use app\api\components\ApiController;
use app\models\forms\TorrentForm;
use app\models\Torrent;
use app\models\TorrentQuery;
use Yii;

class TorrentsController extends ApiController
{

    public function actionCreate()
    {
        $form = new TorrentForm();
        $form->attributes = $_POST;

        if ( $form->validate() ) {
            $form->saveTorrent();
            $form->model->refresh();
        }

        $model = $form->model
            ? $form->model
            : $form;

        return [ $model , [ 'torrentFiles' , 'torrentLogs' ] ];
    }

    public function actionList()
    {
        $torrentData = Torrent::Find()
            ->active()
            ->byUser()
            ->compact()
            ->orderBy( [ 'torrentCreateDate' => SORT_DESC ] )
            ->with( [
                'torrentFiles' => function ( $query ) { return $query->compact()->orderBy( [ 'fileName' => SORT_ASC ] ); } ,
                'torrentLogs'  => function ( $query ) { return $query->compact(); }
            ] )
            ->asArray()
            ->all();

        return $torrentData;
    }

    public function actionParse()
    {
        /** @var $torrentModels Torrent[] */
        $torrentModels = Torrent::Find()
            ->active()
            ->byUser( 2 )
            ->compact()
            ->indexBy( 'torrentId' )
            ->all();

        $fileChanges = Torrent::refreshTorrents( $torrentModels );

        $attributes = [ ];
        foreach ( $fileChanges as $id => $change ) {
            $model = $torrentModels[$id];
            $attributes[$id] = $model->getAttributes( TorrentQuery::$compactAttributes );
        }

        return [
            'changes'    => $fileChanges ,
            'attributes' => $attributes ,
        ];
    }

}