<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 06.11.2014
 * Time: 1:46
 */

namespace app\api\controllers;

use app\api\components\ApiController;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\helpers\Json;
use yii\web\HttpException;

class SystemController extends ApiController
{

    public function actionShutdown()
    {
        shell_exec( "shutdown /s /t 300" );
    }

    public function actionError()
    {

        if ( ( $exception = Yii::$app->getErrorHandler()->exception ) === null ) {
            return '';
        }

        if ( $exception instanceof HttpException ) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ( $exception instanceof Exception ) {
            $name = $exception->getName();
        } else {
            $name = Yii::t( 'app', 'Error' );
        }
        if ( $code ) {
            $name .= " (#$code)";
        }

        if ( $exception instanceof UserException ) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t( 'app', 'An internal server error occurred.' );
        }

        return JSON::encode( [
            'message' => $message,
            'code'    => $code,
        ] );
    }

} 