<?php

/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 08.12.2014
 * Time: 3:33
 */
class ServiceLayerFactory
{

    private static $layers = array ();

    public static function getLayer($layerName)
    {
        if ( !isset( self::$layers[ $layerName ] ) ) {
            self::$layers[ $layerName ] = new $layerName;
        }
        return self::$layers[ $layerName ];
    }
} 