<?php

namespace app\backend\controllers;

use app\models\forms\LoginForm;
use app\models\Settings;
use Yii;
use yii\base\Exception;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className() ,
                'only' => [ 'logout' ] ,
                'rules' => [
                    [
                        'actions' => [ 'logout' ] ,
                        'allow' => TRUE ,
                        'roles' => [ '@' ] ,
                    ] ,
                ] ,
            ] ,
        ];
    }

    public function actionSettings()
    {
        $model = new Settings();
        return $this->render( 'settings' , [ 'model' => $model ] );
    }

    public function actionError()
    {

        if ( ( $exception = Yii::$app->getErrorHandler()->exception ) === NULL ) {
            return '';
        }

        if ( $exception instanceof HttpException ) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ( $exception instanceof Exception ) {
            $name = $exception->getName();
        } else {
            $name = Yii::t( 'app' , 'Error' );
        }
        if ( $code ) {
            $name .= " (#$code)";
        }

        if ( $exception instanceof UserException ) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t( 'app' , 'An internal server error occurred.' );
        }

        if ( Yii::$app->getRequest()->getIsAjax() ) {
            return "$name: $message";
        } else {
            return $this->render( 'error' , [
                'name' => $name ,
                'message' => $message ,
                'exception' => $exception ,
                'code' => $code ,
            ] );
        }
    }

    public function actionIndex()
    {
        return $this->render( 'index' );
    }

    public function actionLogin()
    {
        if ( !\Yii::$app->user->isGuest ) {
            return $this->goBack();
        }

        $this->layout = 'striped';
        $model = new LoginForm();

        if ( Yii::$app->request->isAjax ) {
            $model->load( yii::$app->request->post() );
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate( $model );
        }

        if ( $model->load( Yii::$app->request->post() ) && $model->login() ) {
            return $this->goBack();
        } else {
            return $this->render( 'login' , compact( 'model' ) );
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
