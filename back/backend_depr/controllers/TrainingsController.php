<?php

namespace app\backend\controllers;

use app\common\components\helpers\Text;
use app\models\Exercise;
use app\models\ExerciseName;
use app\models\forms\NewExerciseForm;
use app\models\Scheme;
use app\models\search\TrainingSearch;
use app\models\Training;
use kartik\helpers\Enum;
use Yii;
use yii\caching\DbDependency;
use yii\caching\ExpressionDependency;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TrainingsController implements the CRUD actions for Training model.
 */
class TrainingsController extends Controller
{

    /**
     * @return array
     */
    public function behaviors()
    {
        $trainDep = new DbDependency();
        $trainDep->sql = 'SELECT MAX(`updated`) FROM `training`';
        $trainDep->reusable = TRUE;
        $exerciseDep = new DbDependency();
        $exerciseDep->sql = 'SELECT MAX(`updated`) FROM `exercise`';
        $exerciseDep->reusable = TRUE;
        $timeDep = new ExpressionDependency();
        $timeDep->expression = "((time() - strtotime('midnight')) < 3600)";
        $timeDep->reusable = TRUE;

        return [
            'verbs' => [
                'class' => VerbFilter::className() ,
                'actions' => [
                    'delete' => [ 'post' ] ,
                    'save' => [ 'post' ] ,
                ] ,
            ] ,
            //            'pageCache' => [
            //                'class' => 'yii\filters\PageCache' ,
            //                'duration' => 3600 ,
            //                'dependency' => [
            //                    'class' => 'yii\caching\ChainedDependency' ,
            //                    'dependencies' => [ $exerciseDep , $trainDep ]
            //                ] ,
            //                'variations' => [
            //                    yii::$app->user->identity->userLogin ,
            //                    Enum::getBrowser()[ 'platform' ] ,
            //                ]
            //            ] ,
        ];
    }

    public function actionPrint( $schemeAlias )
    {
        if ( !$schemeModel = Scheme::FindOne( [ 'schemeAlias' => $schemeAlias ] ) )
            throw new NotFoundHttpException( Text::NotFound( 'Схема' ) );

        $trainingModels = Training::Find()
            ->where( [ 'schemeNumber' => $schemeModel->schemeNumber ] )
            ->orderBy( [ 'date' => SORT_DESC ] )
            ->with( [ 'exercises.params' ] )
            ->all();

        if ( $trainingModels ) {
            $this->layout = 'print';
            return $this->render( 'print' , compact( 'trainingModels' , 'schemeModel' ) );
        } else echo 'lol';
    }


    /**
     * Lists all Training models.
     * @return mixed
     */

    //todo поиск с кучей фильтров)
    public function actionIndex( $schemeAlias = NULL , $version = FALSE )
    {

        if ( $schemeAlias )
            $condition = [ 'schemeAlias' => $schemeAlias ];
        else
            $condition = [ 'schemeNumber' => Yii::$app->user->identity->userScheme ];

        if ( !$schemeModel = Scheme::FindOne( $condition ) )
            throw new NotFoundHttpException( Text::NotFound( 'Схема' ) );

        $searchModel = new TrainingSearch();
        $dataProvider = $searchModel->search( Yii::$app->request->getQueryParams() );
        $dataProvider->query
            ->where( [ 'schemeNumber' => $schemeModel->schemeNumber ] )
            ->with( [ 'exercises.params' ] );

        $dataProvider->sort->defaultOrder = [ 'date' => SORT_DESC ];
        $dataProvider->pagination->pageSize = 12;

        $newExerciseForm = new NewExerciseForm();
        $newExerciseForm->increaseType = $schemeModel->schemeWeightsIncrease;
        $columnCount = Exercise::longestExercise( $schemeModel->schemeNumber );

        $newTrain = $schemeModel->generateNewTrain();

        $browserData = Enum::getBrowser();

        if ( $browserData[ 'platform' ] == 'ios' || $version == 'mob' ) {
            return $this->render( 'light' , compact( 'newTrain' , 'dataProvider' ) );
        } else {

            return $this->render( 'index' , compact(
                'newTrain' , 'schemeModel' , 'newExerciseForm' ,
                'dataProvider' , 'searchModel' , 'columnCount' ) );

        }
    }


    /**
     * Displays a single Training model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView( $id )
    {
        return $this->render( 'view' , [
            'model' => $this->findModel( $id ) ,
        ] );
    }

    public function actionExercise( $alias , $schemeAlias )
    {
        if ( !$schemeModel = Scheme::FindOne( [ 'schemeAlias' => $schemeAlias ] ) )
            throw new NotFoundHttpException( Text::NotFound( 'Схема' ) );

        if ( !$params = ExerciseName::FindOne( [ 'exerciseAlias' => $alias ] ) )
            throw new NotFoundHttpException( Text::NotFound( 'Упражнение' ) );

        if ( $params->load( yii::$app->request->post() ) )
            $params->save();

        // todo Нормальная обработка параметров для выбора схемы и построения графика.
        $exerciseModels = Exercise::Find()
            ->where( [
                'exerciseId' => $params->exerciseId ,
                'training.schemeNumber' => $schemeModel->schemeNumber
            ] )
            ->joinWith( [ 'training' ] )
            ->asArray()
            ->all();

        if ( $exerciseModels && $params ) {
            return $this->render( 'exercise' , [
                'models' => $exerciseModels ,
                'params' => $params ,
            ] );
        } else {
            throw new NotFoundHttpException( Yii::t( 'app' , 'Page not found . ' ) );
        }
    }

    /**
     * Creates a new Training model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Training;

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view' , 'id' => $model->id ] );
        } else {
            return $this->render( 'create' , [
                'model' => $model ,
            ] );
        }
    }

    /**
     * Updates an existing Training model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate( $id )
    {
        $model = $this->findModel( $id );

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view' , 'id' => $model->id ] );
        } else {
            return $this->render( 'update' , [
                'model' => $model ,
            ] );
        }
    }

    /**
     * Deletes an existing Training model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete( $id )
    {
        $model = $this->findModel( $id );
        $model->delete();
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id )
    {
        if ( ( $model = Training::findOne( $id ) ) !== NULL ) {
            return $model;
        } else {
            throw new NotFoundHttpException( Yii::t( 'app' , 'Page not found . ' ) );
        }
    }
}
