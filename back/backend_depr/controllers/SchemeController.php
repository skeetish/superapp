<?php

namespace app\backend\controllers;

use app\common\components\helpers\Text;
use app\models\forms\SchemeDayForm;
use app\models\scheme;
use app\models\SchemeDay;
use app\models\SchemeDayExercise;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SchemeController implements the CRUD actions for scheme model.
 */
class SchemeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className() ,
                'actions' => [
                    'delete' => [ 'post' ] ,
                ] ,
            ] ,
        ];
    }

    /**
     * Lists all scheme models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider( [
            'query' => scheme::find() ,
        ] );

        return $this->render( 'index' , [
            'dataProvider' => $dataProvider ,
        ] );
    }

    /**
     * Displays a single scheme model.
     *
     * @param integer $_id
     *
     * @return mixed
     */
    public function actionView( $id )
    {
        return $this->render( 'view' , [
            'model' => $this->findModel( $id ) ,
        ] );
    }

    /**
     * Creates a new scheme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new scheme();

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [ 'view' , 'id' => (string)$model->_id ] );
        } else {
            return $this->render( 'create' , [
                'model' => $model ,
            ] );
        }
    }

    /**
     * Updates an existing scheme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $_id
     *
     * @return mixed
     */
    public function actionUpdate( $id )
    {
        $model = $this->findModel( $id );

        if ( $model->load( Yii::$app->request->post() ) ) {

            $validatedDays = [ ];

            $schemeDayExercises = yii::$app->request->post( 'SchemeDayExercise' );

            if ( $schemeDays = yii::$app->request->post( 'SchemeDay' ) )
                foreach ( $schemeDays as $k => $schemeDay ) {

                    $dayModel = new SchemeDay();
                    $dayModel->attributes = $schemeDay;
                    if ( $dayModel->validate() )
                        $validatedDays[ $k ] = $schemeDay;
                    else continue;

                    if ( $dayExercises = ArrayHelper::getValue( $schemeDayExercises , "$k" ) )
                        foreach ( $dayExercises as $exercise ) {

                            $exerciseModel = new SchemeDayExercise();
                            $exerciseModel->attributes = $exercise;
                            if ( $exerciseModel->validate() )
                                $validatedDays[ $k ][ 'dayExercises' ][ ] = $exercise;

                        }

                }

            if ( $validatedDays )
                $model->schemeDays = $validatedDays;

            if ( $model->save() )
                return $this->redirect( [ 'view' , 'id' => (int)$model->schemeNumber ] );
        }
        return $this->render( 'update' , [ 'model' => $model , ] );

    }

    /**
     * Deletes an existing scheme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $_id
     *
     * @return mixed
     */
    public function actionDelete( $id )
    {
        $this->findModel( $id )->delete();

        return $this->redirect( [ 'index' ] );
    }

    /**
     * Finds the scheme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $_id
     *
     * @return scheme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id )
    {
        if ( $model = scheme::find()->where( [ 'schemeNumber' => (int)$id ] )->one() ) {
            return $model;
        } else {
            throw new NotFoundHttpException( Text::NotFound( 'Схема' ) );
        }
    }
}
