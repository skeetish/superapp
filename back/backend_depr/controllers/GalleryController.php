<?php

namespace app\backend\controllers;

use app\models\Photo;
use app\models\search\Gallery as gallerySearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * GalleryController implements the CRUD actions for gallery model.
 */
class GalleryController extends Controller
{
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className() ,
//                'rules' => [
//                    [
//                        'allow' => TRUE ,
//                        'roles' => [ '@' ] ,
//                    ] ,
//                ] ,
//            ] ,
//            'verbs' => [
            //                'class' => VerbFilter::className(),
            //                'actions' => [
            //                    'delete' => ['post','pjax'],
            //                ],
            //            ],
        ];
    }

    /**
     * Lists all gallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new gallerySearch;

        $dataProvider = $searchModel->search( Yii::$app->request->getQueryParams() );

        $dataProvider->pagination->pageSize = 24;
        $dataProvider->sort->defaultOrder = [ 'photoDate' => 'asc' ];

        return $this->render( 'index' , [
            'dataProvider' => $dataProvider ,
            'searchModel' => $searchModel ,
        ] );
    }

    /**
     * Displays a single gallery model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView( $id )
    {
        return $this->render( 'view' , [
            'model' => $this->findModel( $id ) ,
        ] );
    }

    /**
     * Creates a new gallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Photo;

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [
                'view' ,
                'id' => $model->photoId
            ] );
        } else {
            return $this->render( 'create' , [
                'model' => $model ,
            ] );
        }
    }

    /**
     * Updates an existing gallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate( $id )
    {
        $model = $this->findModel( $id );

        if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
            return $this->redirect( [
                'view' ,
                'id' => $model->photoId
            ] );
        } else {
            return $this->render( 'update' , [
                'model' => $model ,
            ] );
        }
    }

    /**
     * Deletes an existing gallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete( $id )
    {
        $this->findModel( $id )->delete();
        return $this->redirect( [ 'index' ] );
    }

    public function beforeAction( $action )
    {
        return parent::beforeAction( $action );
    }

    /**
     * Finds the gallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.

     *
*@param integer $id

     *
*@return Photo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel( $id )
    {
        if ( ( $model = Photo::findOne( $id ) ) !== NULL ) {
            return $model;
        } else {
            throw new NotFoundHttpException( 'The requested page does not exist.' );
        }
    }
}
