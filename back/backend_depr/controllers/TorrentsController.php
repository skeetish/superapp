<?php

namespace app\backend\controllers;

use app\common\components\helpers\Text;
use app\models\Torrent;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TorrentsController extends Controller
{
    public $dropdownItems = [
        [
            'icon'  => 'fa fa-cog fa-2x text-primary' ,
            'label' => 'Настройки' ,
            'url'   => [ '/backend/torrents/index' ]
        ] ,
        [
            'icon'  => 'fa fa-plus fa-2x text-success' ,
            'label' => 'Добавить' ,
            'url'   => [ '/backend/torrents/create' ]
        ]
    ];

    /**
     * Lists all news models.
     * @return mixed
     */
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider();
        $dataProvider->query = Torrent::find();
        $dataProvider->pagination->pageSize = 5;
        return $this->render( 'index' , [
            'dataProvider' => $dataProvider ,
        ] );
    }


    public function actionParse()
    {
        $fileChanges = NULL;

        $torrentModels = Torrent::Find()
            ->byUser()
            ->active()
            ->with( [ 'torrentFiles' => function ( $query ) { return $query->orderBy( [ 'fileName' => SORT_ASC ] ); } , 'torrentLogs' ] )
            ->all();

        $count = count( $torrentModels );
        $fileChanges = Torrent::refreshTorrents( $torrentModels );
        $newTorrent = new Torrent();

        return $this->render( 'parse' , compact( 'torrentModels' , 'fileChanges' , 'count' , 'newTorrent' ) );

    }

    protected function findModel( $id )
    {
        if ( $model = Torrent::find()->where( [ 'torrentId' => $id ] )->one() ) {
            return $model;
        } else {
            throw new NotFoundHttpException( Text::NotFound( 'Торрент' ) );
        }
    }

}



