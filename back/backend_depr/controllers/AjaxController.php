<?php

namespace app\backend\controllers;

use app\common\components\helpers\Text;
use app\models\Exercise;
use app\models\forms\FileUploadForm;
use app\models\forms\NewCommentForm;
use app\models\forms\NewExerciseForm;
use app\models\Photo;
use app\models\Scheme;
use app\models\Torrent;
use app\models\Training;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

class AjaxController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'upload' => ['POST'],
                    'remove' => ['POST'],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
        //        $get = yii::$app->getRequest()->get();
        //        if ( !$id = ArrayHelper::getValue( $get , 'Training.id' ) )
        //            $this->sendResponse( 500 , 'Нет необходимого параметра: ID' );

        if (!$model = Training::find()->where(['id' => $id])->with('exercises.params')->one()) {
            $this->sendResponse(404, 'Not Found');
        } else {
            $data['Training'] = $model->attributes;
            $data['Exercises'] = $model->exercises;
            $this->sendResponse(200, $data);
        }

    }

    public function actionAddTorrent()
    {
        if (!$params = yii::$app->getRequest()->post('Torrent'))
            $this->sendResponse(400, 'Bad Params');

        $torrent = new Torrent();
        $torrent->attributes = $params;

        if ($torrent->validate()) {

            $torrentData = $torrent->parseTorrentUrl();
            $torrent->attributes = $torrentData['attributes'];
            if ($torrent->validate()) {

                $torrentData['attributes']['torrentSize'] = Text::bytes2words($torrentData['attributes']['torrentSize']);
                $this->sendResponse(200, $torrentData);
            }
        } else
            $this->sendResponse(500, $torrent->errors);

    }

    public function actionAddExercise()
    {
        if (!$params = yii::$app->getRequest()->post('NewExerciseForm'))
            $this->sendResponse(400, 'Bad Params');

        $newExerciseForm = new NewExerciseForm();
        $newExerciseForm->attributes = $params;

        if ($newExerciseForm->validate() && ($exercise = $newExerciseForm->CreateExercise())) {

            $exercise['params'] = ArrayHelper::merge(
                ArrayHelper::getValue($exercise, 'params'),
                $params
            );

            $this->sendResponse(200, $exercise);
        } else
            $this->sendResponse(500, $newExerciseForm->errors);
    }

    public function actioncomment()
    {
        if (!$post = yii::$app->getRequest()->post('NewCommentForm'))
            $this->sendResponse(400, 'Bad Params');

        $commentForm = new NewCommentForm();
        $commentForm->attributes = $post;

        if ($commentForm->validate()) {

            if (!$exerciseModel = Exercise::find()->where(['id' => $commentForm->id])->one())
                $this->sendResponse(404, 'Not Found');

            $exerciseModel->comment = $commentForm->comment;

            if (!$exerciseModel->save())
                $this->sendResponse(500, $exerciseModel->errors);

            $this->sendResponse(200, true);

        } else
            $this->sendResponse(500, $commentForm->errors);

    }

    public function actionUpload()
    {

        $data = [];
        if ($file = UploadedFile::getInstanceByName('upload')) {

            $fileUploadForm = new FileUploadForm();
            $fileUploadForm->loadFiles($file);
            $result = $fileUploadForm->saveFiles();
            if (!$result['errors'] && $result['files']) {

                $id = yii::$app->getRequest()->post('replace-id');
                if (!$model = Photo::findOne(['photoId' => $id]))
                    $model = new Photo;

                $fileName = pathinfo($result['files'], PATHINFO_FILENAME).'.'.pathinfo($result['files'], PATHINFO_EXTENSION);

                $model->photoOriginal = $file->name;
                $model->photoPath = $fileName;

                if ($model->save())
                    $data = $model->attributes;
                else
                    $data['errors'] = $model->errors;

            } else
                $data['errors'] = $result['errors'];

        }
        if ($data['errors'])
            $this->sendResponse(500, $data);
        else
            $this->sendResponse(200, $data);
    }

    public function actionRemove()
    {

        if (!$id = yii::$app->request->post('id'))
            $this->sendResponse(404, 'Нет необходимого параметра: ID');

        if (Training::FindOne($id)->delete())
            $this->sendResponse(200, true);

    }

    public function actionSave()
    {
        $post = Yii::$app->request->post();
        $id = ArrayHelper::getValue($post, 'Training.id');
        $schemeAlias = ArrayHelper::getValue($post, 'Training.schemeAlias');
        $exercises = ArrayHelper::getValue($post, 'Training.exercises');
        $data = [];

        if (!$model = Training::Find()->with('exercises', 'scheme')->where(['id' => $id])->one()) {
            $model = new Training;
            $model->populateRelation('scheme', Scheme::Find()->where(['schemeAlias' => $schemeAlias])->one());
            $model->schemeNumber = $model->scheme->schemeNumber;
        }
        if ($exercises && $model->load($post) && $model->save()) {

            /** @var $exercise Exercise */
            foreach ($model->exercises as $exercise)
                $exercise->delete();

            /** @var $exercise Array */
            foreach ($exercises as &$exercise) {

                $newExercise = new Exercise;
                $newExercise->attributes = $exercise;
                $newExercise->id = $model->id;
                if ($newExercise->save()) {

                    $increaseWeight = $newExercise
                        ->training->scheme
                        ->checkTrainWeightIncrease($model->trainModifier);

                    $url = Url::toRoute([
                        '/backend/trainings/exercise',
                        'alias'       => $exercise['exerciseAlias'],
                        'schemeAlias' => $schemeAlias,
                    ]);

                    $data ['exercises'][] = [
                        'params'  => ArrayHelper::merge(
                            $newExercise->attributes,
                            $newExercise->params->attributes,
                            ['increaseWeight' => $increaseWeight]
                        ),
                        'weights' => $newExercise->calculateWeights(),
                        'site'    => ['url' => $url],

                    ];
                }
            }

            $this->sendResponse(200, ArrayHelper::merge($model->attributes, $data));
        }
    }

    private
    function sendResponse(
        $code, $message, $result = true
    ){
        Yii::$app->response->data = $message;
        Yii::$app->response->setStatusCode($code);
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->send();
        exit;
    }

}
