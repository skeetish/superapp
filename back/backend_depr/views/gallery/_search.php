<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View              $this
 * @var app\models\search\Gallery $model
 * @var yii\widgets\ActiveForm    $form
 */
?>

<div class="gallery-search">

    <?php $form = ActiveForm::begin( [
        'action' => [ 'index' ] ,
        'method' => 'get' ,
    ] ); ?>

    <?= $form->field( $model , 'photoId' ) ?>

    <?= $form->field( $model , 'photoDate' ) ?>

    <?= $form->field( $model , 'photoPath' ) ?>

    <?= $form->field( $model , 'photoOriginal' ) ?>

    <div class="form-group">
        <?= Html::submitButton( 'Search' , [ 'class' => 'btn btn-primary' ] ) ?>
        <?= Html::resetButton( 'Reset' , [ 'class' => 'btn btn-default' ] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
