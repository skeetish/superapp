<?php

/* @var app\models\Photo $model
 *
 */

use app\models\Photo;
use yii\helpers\Html;

?>
<?=
Html::a( Html::img( Photo::getImageLink( $model->photoPath , TRUE ) ) , Photo::getImageLink( $model->photoPath ) , [
    'class' => 'thumbnail' , 'rel' => 'fancybox' , 'data-pjax' => '0'
] ) ?>
<div class="gallery-action-bar">
    <?= Html::a( '' , '#' , [ 'class' => 'glyphicon glyphicon-file white' , 'data-role' => 'replace-button' ] ) ?>
    <?=
    Html::a( '' , [ 'gallery/delete' , 'id' => $model->photoId ] , [
        'data-pjax' => '0' , 'class' => 'glyphicon glyphicon-remove white' , 'data-role' => 'delete-button'
    ] ) ?>
</div>

