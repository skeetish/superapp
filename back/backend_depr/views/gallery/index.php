<?php

use app\backend\components\widgets\Panel;
use newerton\fancybox\FancyBox;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\News      $searchModel
 */
$ajaxUploadLink = yii::$app->urlManager->createUrl( [ '/backend/ajax/upload' ] );

//        'js/jquery.ui.widget.js' ,
//        'js/jquery.iframe-transport.js' ,
//        'js/jquery.fileupload.js' ,
$this->registerJs( "var ajaxUploadLink = '$ajaxUploadLink'" , view::POS_HEAD );
$this->registerJs( "var pageSize = '{$dataProvider->pagination->pageSize}'" , view::POS_HEAD );
$this->registerAssetBundle( 'app\common\assets\packages\HandlebarsAsset' , View::POS_END );

$this->title = 'News';
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>


<?php Panel::begin( [
    'headerContent' => $this->title ,
    'containerOptions' => [ 'class' => 'container' ]
] ) ?>

<div class="col-lg-12">

    <?php $form = ActiveForm::Begin( [ 'options' => [ 'enctype' => "multipart/form-data" ] ] ) ?>
    <div id="errors" class="alert"></div>

    <?=
    Html::fileInput( 'upload' , '' , [
        'style' => 'display:none' , 'id' => 'upload-form' , 'multiple' => TRUE
    ] ) ?>
    <?= Html::hiddenInput( 'replace-id' , '' , [ 'style' => 'display:none' , 'id' => 'replace-id' ] ) ?>
    <?php ActiveForm::End() ?>
    <?= Html::a( 'Загрузить фото!' , '#' , [ 'class' => 'btn btn-success' , 'id' => 'upload-button' ] ) ?>
    <hr>

</div>
<?=
ListView::widget( [
    'id' => 'list-view' ,
    'dataProvider' => $dataProvider ,

    'itemView' => '_item' ,
    'itemOptions' => [ 'data-role' => 'item' , 'class' => 'col-xs-2 m-b-xs' ] ,

    'layout' => '{items}</div><div class="col-lg-12"><hr>{pager}' ,
    'pager' => [ 'hideOnSinglePage' => TRUE ]
] ) ?>
<?php Panel::end() ?>


<?=
FancyBox::widget( [
    'target' => 'a[rel=fancybox]' ,
    'config' => [
        'maxWidth' => '100%' ,
        'maxHeight' => '100%' ,
        'padding' => FALSE ,
        'closeBtn' => TRUE ,
        'openOpacity' => TRUE ,
    ]
] );
?>
<script id="item" type="text/x-handlebars-template">

    <div class="col-xs-2 m-b-xs" data-role="item" data-key="{{photoId}}">
        <a class="thumbnail" href="/images/pics/{{photoPath}}" rel="fancybox" data-pjax="0"><img src='/images/thumbs/{{photoPath}}' alt=""></a>

        <div class="gallery-action-bar">
            <a class="glyphicon glyphicon-file white" href="#" data-role="replace-button"></a>
            <a class="glyphicon glyphicon-remove white" href='/backend/gallery/delete?id={{photoId}}' data-pjax="0" data-role="delete-button"></a>
        </div>

    </div>
</script>



