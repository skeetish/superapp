<?php

use app\backend\components\widgets\Panel;
use app\common\components\helpers\Date;
use app\common\components\helpers\Text;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\News      $searchModel
 */

$this->title = Yii::t( 'app' , 'News' );
$this->params[ 'breadcrumbs' ][ ] = $this->title;

?>
<?php Pjax::begin( [ 'id' => 'pjax-content' ] ) ?>
<?= Html::a( Yii::t( 'app' , 'Create {modelClass}' , [ 'modelClass' => 'News' ] ) , [ 'create' ] , [ 'class' => 'btn btn-success' ] ) ?>    </p>

<?php Panel::begin( [
    'bodyOptions' => FALSE ,
    'headerOptions' => FALSE ,
] ) ?>
<?php
$options = [
    'dataProvider' => $dataProvider ,
    'filterModel' => $searchModel ,
    'columns' => [
        [ 'class' => 'yii\grid\SerialColumn' ] ,
        [
            'attribute' => 'entryTitle' ,
            'value' => function ( $model ) { return Text::shorten( $model->entryTitle , 30 ); }
        ] ,
        [
            'attribute' => 'entryText' ,
            'format' => 'html' ,
            'value' => function ( $model ) {
                    return html::a( Text::shorten( $model->entryText , 100 ) , [
                        'view' , 'id' => $model->id
                    ] );
                }
        ] ,
        [
            'attribute' => 'entryDate' ,
            'value' => function ( $model ) { return Date::display( $model->entryDate ); }
        ] ,
        [
            'attribute' => 'entryUpdated' ,
            'value' => function ( $model ) { return Date::display( $model->entryUpdated ); }
        ] ,
        [
            'class' => 'yii\grid\ActionColumn' ,
            'buttons' => [
                'view' => function ( $url , $model ) { return Html::a( '' , [ '/backend/news/view' , 'id' => $model->id ] , [ 'class' => 'fa fa-eye' ] ); } ,
                'update' => function ( $url , $model ) { return Html::a( '' , [ '/backend/news/update' , 'id' => $model->id ] , [ 'class' => 'fa fa-pencil' ] ); } ,
                'delete' => function ( $url , $model ) { return Html::a( '' , [ '/backend/news/delete' , 'id' => $model->id ] , [ 'class' => 'i i-trashcan' ] ); }
            ]
        ] ,
    ] ,
];
?>
<?= GridView::widget( ArrayHelper::merge( $options , $this->context->module->widgetOptions( 'GridView' ) ) ); ?>

<?php Panel::end() ?>
<?php Pjax::end() ?>
