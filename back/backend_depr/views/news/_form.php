<?php

use app\backend\components\widgets\Panel;
use app\common\components\helpers\Date;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var app\models\News        $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<?php Panel::begin( [
    'headerContent' => Html::tag( 'h4' , ' #' . Html::encode( $model->primaryKey ) , [ 'class' => 'font-thin ' . ( $model->isNewRecord ? 'fa fa-plus' : ' fa fa-edit' ) ] )
] ) ?>

<?php $form = ActiveForm::begin( $this->context->module->widgetOptions( 'BSActiveForm' ) ); ?>
<?= $form->field( $model , 'entryTitle' )->textInput( [ 'maxlength' => 255 ] ) ?>
<?= $form->field( $model , 'entryText' )->textarea( [ 'rows' => 16 ] ) ?>
<?=
$form->field( $model , 'entryDate' )->textInput( [
    'value' => Date::display( $model->entryDate ) , 'readonly' => 'true'
] ) ?>
<?=
$form->field( $model , 'entryUpdated' )->textInput( [
    'value' => Date::display( $model->entryUpdated ) , 'readonly' => 'true'
] ) ?>

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11 btn-group">
        <?= Html::submitButton( $model->isNewRecord ? Yii::t( 'yii' , 'Create' ) : Yii::t( 'yii' , 'Save' ) , [ 'class' => ( $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ) ] ) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php Panel::end() ?>
