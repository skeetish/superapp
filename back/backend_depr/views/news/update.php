<?php

/**
 * @var yii\web\View    $this
 * @var app\models\News $model
 */

use yii\widgets\Pjax;

$this->title = Yii::t( 'app' , 'Update {modelClass}: ' , [
        'modelClass' => 'News' ,
    ] ) . ' ' . $model->id;
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => Yii::t( 'app' , 'News' ) , 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => $model->id , 'url' => [ 'view' , 'id' => $model->id ] ];
$this->params[ 'breadcrumbs' ][ ] = Yii::t( 'app' , 'Update' );
?>
<?php Pjax::begin( [ 'id' => 'pjax-content' ] ) ?>

<?=
$this->render( '_form' , [
    'model' => $model ,
] ) ?>

<?php Pjax::end() ?>
