<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View           $this
 * @var app\models\search\News $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="news-search">

    <?php $form = ActiveForm::begin( [
        'action' => [ 'index' ] ,
        'method' => 'get' ,
    ] ); ?>

    <?= $form->field( $model , 'id' ) ?>

    <?= $form->field( $model , 'entryTitle' ) ?>

    <?= $form->field( $model , 'entryText' ) ?>

    <?= $form->field( $model , 'entryDate' ) ?>

    <?= $form->field( $model , 'entryUpdated' ) ?>

    <div class="form-group">
        <?= Html::submitButton( Yii::t( 'yii' , 'Search' ) , [ 'class' => 'btn btn-primary' ] ) ?>
        <?= Html::resetButton( Yii::t( 'yii' , 'Reset' ) , [ 'class' => 'btn btn-default' ] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
