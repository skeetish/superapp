<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use app\common\components\helpers\Date;
use app\common\components\helpers\Text;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View       $this
 * @var app\models\Torrent $model
 */

$this->title = $model->torrentName;
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => Yii::t( 'app' , 'News' ) , 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>

<?php Pjax::begin( [ 'id' => 'pjax-content' ] ) ?>

<?php Panel::begin( [
    'bodyOptions' => [ 'class' => 'panel-body form-horizontal' ] ,
    'headerContent' => "<i class='fa fa-eye fa-2x'></i> <h4 class='inline font-thin '> #" . Html::encode( $model->primaryKey ) . "</h4>"

] ) ?>

<?php
$options = [
    'model' => $model ,
    'template' => "<div class='form-group'><div class='control-label col-lg-2'>{label}</div><div class='col-lg-10'><span class='form-control'>{value}</span></div></div>" ,
    'attributes' => [
        [
            'attribute' => 'torrentCreateDate' ,
            'value' => Date::display( $model->torrentCreateDate )
        ] ,
        [
            'attribute' => 'torrentParseDate' ,
            'value' => Date::display( $model->torrentParseDate )
        ] ,
        [
            'attribute' => 'torrentChangeDate' ,
            'value' => Date::display( $model->torrentChangeDate )
        ] ,
    ]
];?>
<?= DetailView::widget( ArrayHelper::merge( Backend::widgetOptions( 'DetailView' ) , $options ) ); ?>

<div class='form-group'>
    <div class="col-lg-offset-2 col-lg-10 btn-group">

        <?= Html::a( Yii::t( 'yii' , 'Update' ) , [ 'update' , 'id' => $model->torrentId ] , [ 'class' => 'btn btn-info' ] ) ?>
        <?=
        Html::a( Yii::t( 'yii' , 'Delete' ) , [ 'delete' , 'id' => $model->torrentId ] , [
            'class' => 'btn btn-danger' ,
            'data' => [
                'confirm' => Yii::t( 'yii' , 'Are you sure you want to delete this item?' ) ,
                'method' => 'post' ,
            ] ,
        ] ) ?>
    </div>
</div>
<?php Panel::begin( [
    'headerContent' => "<i class='fa fa-file'></i> <h5 class='inline font-thin'>Файлы</h5>" ,
    'options' => [ 'class' => 'panel panel-default col-xs-offset-2' ]
] ) ?>
<?php /** @var $file \app\models\TorrentFile */
foreach ( $torrentFiles as $file ): ?>
    <div class="col-xs-12 b-b">
        <div class="col-xs-6 no-padder"><?= $file->fileName ?></div>
        <div class="col-xs-6"><?= Text::bytes2words( $file->fileSize ) ?></div>
    </div>
<?php endforeach ?>
<?php Panel::end() ?>
</div>
<?php Panel::end() ?>
<?php Pjax::end() ?>
