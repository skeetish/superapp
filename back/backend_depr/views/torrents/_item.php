<?php
/** @var $torrentModel \app\models\Torrent */

use app\backend\components\widgets\ModalExt;
use app\common\components\helpers\Date;
use app\common\components\helpers\Text;
use app\models\TorrentLog;
use yii\helpers\Html;

?>

<?php if ( $torrentModel->torrentLogs ): ?>

    <?php
    $modal = ModalExt::begin( [
        'header' => Html::tag( 'i' , '' , [ 'class' => 'fa fa-calendar' ] ) . ' <b>' . $torrentModel->torrentName . '</b>' ,
        'bodyOptions' => [ 'class' => 'modal-body panel-body' ]
    ] ) ?>
    <?php /** @var $logModel TorrentLog */

    foreach ( $torrentModel->torrentLogs as $logModel ) : ?>
        <?php if ( $logModel->logType == TorrentLog::TYPE_FILE_ADD ) : ?>
            <div class="text-success-dker bg-success-ltest b-b row">
                <div class="m-l-xs tiny-column pull-left">+</div>
                <div>
                    <?= $logModel->logValue ?>
                    <div class="pull-right m-r-xl"><?= Date::display( $logModel->logDate ) ?></div>
                </div>

            </div>
        <?php else: ?>
            <div class="text-danger-dker bg-danger-ltest b-b row">
                <div class="m-l-xs tiny-column pull-left">-</div>
                <div>
                    <?= $logModel->logValue ?>
                    <div class="pull-right m-r-xl"><?= Date::display( $logModel->logDate ) ?></div>
                </div>
            </div>
        <?php endif ?>

    <?php endforeach ?>
    <?php ModalExt::end() ?>
<?php endif ?>

<?php

if ( $torrentChanges ):
    foreach ( $torrentChanges as $k => $changedFile ) :

        if ( $changedFile[ 'type' ] == 'added' ):?>
            <div class="b-b col-xs-12 no-padder bg-success-ltest text-success-dker">
                <div class="col-xs-5"><?= $changedFile[ 'fileName' ] ?></div>
                <div class="pull-left short-column"><?= Text::bytes2words( $changedFile[ 'fileSize' ] ) ?></div>
                <div class="col-xs-2">Добавлено</div>
            </div>
        <?php else: ?>
            <div class="b-b col-xs-12 no-padder bg-danger-ltest text-danger-dker">
                <div class="col-xs-5"><?= $changedFile[ 'fileName' ] ?></div>
                <div class="pull-left short-column"><?= Text::bytes2words( $changedFile[ 'fileSize' ] ) ?></div>
                <div class="col-xs-2">Удалено</div>
            </div>
        <?php endif; ?>

    <?php endforeach; ?>

    <div class="col-xs-12 b-b m-t-sm">

        <div class="col-xs-offset-5 pull-left text-left m-l-n-xs">
            <a href="<?= $torrentModel->torrentLink ?>">
                <span class="text-primary">
                    Скачать <i class="i i-download"></i>
                </span>
            </a>
        </div>

    </div>

<?php else: ?>
    <div class="col-xs-12 b-b m-b-md">

        <div class="col-xs-5">
            <?php if ( $torrentModel->torrentLogs ): ?>
                <a data-toggle="modal" data-target="#<?= $modal->id ?>" href="#" class="">

                    <span class="text-primary">
                        <i class="fa fa-calendar-o text-primary"></i> Последние изменения:
                        <b><?= Date::display( $torrentModel->torrentChangeDate ) ?></b>
                    </span>
                </a>
            <?php endif ?>
        </div>

        <div class="pull-left text-left m-l-n-xs">
            <a href="<?= $torrentModel->torrentLink ?>">
                <span class="text-primary">
                    Скачать <i class="i i-download"></i>
                </span>
            </a>
        </div>

    </div>
<?php endif ?>
<?php foreach ( $torrentModel->torrentFiles as $file ) : ?>
    <div class="col-xs-12 no-padder b-b">
        <div class="col-xs-5"><?= $file->fileName ?></div>
        <div class="pull-left short-column"><?= Text::bytes2words( $file->fileSize ) ?></div>
    </div>
<?php endforeach; ?>
