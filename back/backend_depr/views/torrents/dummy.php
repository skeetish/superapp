<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\Pjax;

$this->registerJs( "$.pjax.reload({container:'#pjax'});" );
$this->title = 'Torrents';
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>
<?php Panel::begin( [ 'headerOptions' => FALSE ] ) ?>

<?php Panel::begin( [ 'headerOptions' => FALSE ] ) ?>

<?php $form = ActiveForm::begin( Backend::widgetOptions( 'BSActiveForm' ) ) ?>
<?php $form->beforeSubmit = new JsExpression( 'function (form, attribute, messages) { return false } ' ) ?>
<?=
$form
    ->field( $newTorrent , 'torrentUrl' , [ 'options' => [ 'class' => 'col-xs-8' ] ] )
    ->textInput()
?>
<?= Html::a( '' , '#' , [ 'class' => 'fa fa-plus text-success fa-2x' ] ) ?>
<?php ActiveForm::end() ?>
<?php Panel::end() ?>

<?php Pjax::begin( [ 'id' => 'pjax' , 'timeout' => 10000 ] ) ?>
<?= Html::img( '/images/ajax-loader.gif' ) ?>
<?php Pjax::end() ?>
<?php Panel::end() ?>
