<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use kartik\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Collapse;
use yii\helpers\Html;

/** @var $fileChanges Array */
/** @var $torrentModel \app\models\Torrent */
/** @var $newTorrent \app\models\Torrent */
/** @var $torrentModels Array */
/**
 * @var yii\web\View $this
 * @var              $dataProvider Array
 */

$this->title = 'Torrents';
$this->params['breadcrumbs'][] = $this->title;

$this->registerAssetBundle( 'app\common\assets\packages\HandlebarsAsset' );
?>
<?php Panel::begin( [ 'headerOptions' => FALSE ] ) ?>





<?php Panel::begin( [ 'headerOptions' => FALSE ] ) ?>

<?php $form = ActiveForm::begin( Backend::widgetOptions( 'BSActiveForm' ) ) ?>
<?php //$form->beforeSubmit = new JsExpression( 'function (form, attribute, messages) { return false } ' ) ?>
<?php //$form->afterValidate = new JsExpression('
//    function (form, attribute, messages) {
//    if ( Object.keys(messages).length == 0) {
//    clearTimeout(window.timeout);
//    window.timeout = setTimeout( function () { alert("alert") }, 1000 )};
//    }')
?>

<div class="row">
    <?=
    $form
        ->field( $newTorrent , 'torrentUrl' , [ 'options' => [ 'class' => 'col-xs-8' ] ] )
        ->textInput()
    ?>

    <?= Html::a( '' , '#' , [ 'data-role' => 'add-torrent-button' , 'class' => 'fa fa-plus text-success fa-2x' ] ) ?>
</div>
<div class="m-t-md" id="new-torrent-block">

</div>
<?php ActiveForm::end() ?>

<?php Panel::end() ?>

<?php //Pjax::begin( [ 'id' => 'pjax' ,'timeout' => 10000] ) ?>

<?php if ( !$fileChanges ): ?>

    <?php
    Alert::begin( [ 'options' => [ 'class' => 'alert-info' ] ] ) ?>
    Нет изменений.
    <br>
    <?= implode( '<br>' , \app\models\Torrent::$logging ) ?>
    <?php Alert::end() ?>

<?php elseif ( isset( $fileChanges['errors'] ) ): ?>

    <?php
    Alert::begin( [ 'options' => [ 'class' => 'alert-danger' ] ] ) ?>
    <?php foreach ( $fileChanges['errors'] as $name => $message ) : ?>
        <?= $name ?> - <?= $message ?> <br>
    <?php endforeach ?>
    <?php Alert::end() ?>

<?php endif ?>

<?php foreach ( $torrentModels as $torrentModel ): ?>
    <?php /** @var $file \app\models\TorrentFile */
    $content = NULL;

    $torrentChanges = $fileChanges[$torrentModel->torrentName];
    $content .= $this->render( '_item' , compact( 'torrentChanges' , 'torrentModel' ) );

    //todo сохранение размера торрента, хешей файлов
    if ( count( $torrentModels ) ) {
        $content .= Html::tag( 'div' , 'Всего: ' , [ 'class' => 'col-xs-5 text-right' ] );
        $content .= Html::tag( 'div' , count( $torrentModel->torrentFiles ) , [ 'class' => 'pull-left short-column' ] );
    }
    ?>


    <?php

    if ( !$torrentChanges )
        $caret = Html::tag( 'i' , '' , [ 'class' => 'fa fa-plus-square-o' , 'data-role' => 'caret' ] );
    else
        $caret = Html::tag( 'i' , '' , [ 'class' => 'fa fa-minus-square-o' , 'data-role' => 'caret' ] );

    $collapseItems[$caret . ' ' . $torrentModel->torrentName] = [
        'content'        => $content ,
        'contentOptions' => [ 'class' => ( ( $torrentChanges ) ? 'in' : 'collapsed' ) ]
    ] ?>

<?php endforeach ?>
<?=
Collapse::widget( [
    'items'         => $collapseItems ,
    'clientOptions' => FALSE
] ); ?>

<?php //Pjax::end()?>
<?php Panel::end() ?>

<script id="torrent" type="text/x-handlebars-template">
    <div>
        <div data-id='{{attributes.torrentId}}' class="row b-b b-light bg-success-ltest text-success-dker">
            <div class="m-l-xs tiny-column pull-left">+</div>
            <div class="col-xs-offset-1 col-xs-4">{{attributes.torrentName}}</div>
            <div class="short-column pull-left">{{attributes.torrentSize}}</div>
        </div>
    </div>

</script>

<script id="notification" type="text/x-handlebars-template">
    <div>
        <div id='notification' class="m-t-lg col-xs-offset-1 m-b-n-sm">
            Торрент добавлен. Необходимо обновить страницу.
        </div>
    </div>

</script>

