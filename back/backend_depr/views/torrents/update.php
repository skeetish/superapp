<?php

/**
 * @var yii\web\View    $this
 * @var app\models\News $model
 */

use yii\widgets\Pjax;

$this->title = Yii::t( 'app' , 'Update {modelClass}: ' , [
        'modelClass' => 'Torrent' ,
    ] ) . ' ' . $model->torrentId;
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => Yii::t( 'app' , 'News' ) , 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][ ] = Yii::t( 'app' , 'Update' );
?>
<?php Pjax::begin( [ 'id' => 'pjax-content' ] ) ?>

<?=
$this->render( '_form' , [
    'model' => $model ,
] ) ?>

<?php Pjax::end() ?>
