<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\Torrent         $model
 */

$this->title = Yii::t( 'app' , 'News' );

?>
<?php //Pjax::begin( [ 'id' => 'pjax-content' ] ) ?>

<?php Panel::begin( [ 'headerOptions' => FALSE ] ) ?>
<p>
    <?= Html::a( '<i class="fa fa-plus"></i> Добавить' , [ '/backend/torrents/create' ] , [ 'class' => 'font-thin text-success text-md' ] ) ?>
</p><p>
    <?= Html::a( '<i class="fa fa-refresh"></i> Обновить' , [ '/backend/torrents/refresh' ] , [ 'class' => 'font-thin text-primary text-md' ] ) ?>
</p>
<?php
$options = [
    'dataProvider' => $dataProvider ,
    'columns' => [
        [ 'class' => 'yii\grid\SerialColumn' ] ,
        'torrentName' ,
        'torrentHash' ,
        'torrentUrl' ,
        [
            'class' => 'yii\grid\ActionColumn' ,
            'template' => '{download} {view} {update} {delete}' ,
            'buttons' => [
                'view' => function ( $url , $model ) { return Html::a( '' , [ '/backend/torrents/view' , 'id' => $model->torrentId ] , [ 'class' => 'i i-eye' , 'data-pjax' => '0' ] ); } ,
                'download' => function ( $url , $model ) { return Html::a( '' , $model->torrentLink , [ 'class' => 'fa fa-download' , 'data-pjax' => '0' ] ); } ,
                'update' => function ( $url , $model ) { return Html::a( '' , [ '/backend/torrents/update' , 'id' => $model->torrentId ] , [ 'class' => 'fa fa-pencil' ] ); } ,
                'delete' => function ( $url , $model ) { return Html::a( '' , [ '/backend/torrents/delete' , 'id' => $model->torrentId ] , [ 'class' => 'i i-trashcan' ] ); }
            ]
        ] ,
    ] ,
];
?>
<?= GridView::widget( ArrayHelper::merge( $options , Backend::widgetOptions( 'GridView' ) ) ); ?>

<?php Panel::end() ?>
<?php //Pjax::end() ?>
