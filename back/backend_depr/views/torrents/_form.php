<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use app\common\components\helpers\Date;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var app\models\News        $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<?php Panel::begin( [
    'headerContent' => Html::tag( 'h4' , ' #' . Html::encode( $model->primaryKey ) , [ 'class' => 'font-thin ' . ( $model->isNewRecord ? 'fa fa-plus' : ' fa fa-edit' ) ] )
] ) ?>

<?php $form = ActiveForm::begin( Backend::widgetOptions( 'BSActiveForm' ) ); ?>
<?= $form->field( $model , 'torrentName' )->textInput() ?>
<?= $form->field( $model , 'torrentUrl' )->textInput() ?>
<?php if ( !$model->isNewRecord ): ?>
    <?= $form->field( $model , 'torrentHash' )->textInput( [ 'readonly' => 'true' ] ) ?>
    <?=
    $form->field( $model , 'torrentLastParse' )->textInput( [
        'value' => Date::display( $model->torrentLastParse ) , 'readonly' => 'true'
    ] ) ?>
<?php endif ?>
<!--todo разобраться со сценариями-->

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11 btn-group">
        <?= Html::submitButton( $model->isNewRecord ? Yii::t( 'yii' , 'Create' ) : Yii::t( 'yii' , 'Save' ) , [ 'class' => ( $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ) ] ) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php Panel::end() ?>
