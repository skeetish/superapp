<?php

use app\backend\components\widgets\Panel;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var app\models\News        $model
 * @var yii\widgets\ActiveForm $form
 * @var yii\web\View           $this
 * @var app\models\News        $model
 */
?>
<?php ob_start(); ?>





<?php
$form = ActiveForm::begin( \app\backend\Backend::widgetOptions( 'BSActiveForm' ) );

echo $form->field( $model , 'lal' );

ActiveForm::end();
?>



<?php $firstTab = ob_get_contents();
ob_end_clean();?>

<?php Panel::begin( [
    'headerContent' => Html::tag( 'h4' , ' #' . Html::encode( $model->primaryKey ) , [ 'class' => 'font-thin ' . ( $model->isNewRecord ? 'fa fa-plus' : ' fa fa-edit' ) ] )
] );
?>

<?php

echo Tabs::widget( [
    'itemOptions' => [ 'class' => 'padder-v' ] ,
    'items' => [
        [
            'label' => 'One' ,
            'content' => $firstTab ,
            'active' => TRUE
        ] ,
        [
            'label' => 'Two' ,
            'content' => 'Anim pariatur cliche...' ,
            'options' => [ 'id' => 'myveryownID' ] ,
        ] ,
    ] ,
] );
?>

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11 btn-group">
        <?= Html::submitButton( $model->isNewRecord ? Yii::t( 'yii' , 'Create' ) : Yii::t( 'yii' , 'Save' ) , [ 'class' => ( $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ) ] ) ?>
    </div>
</div>
<?php Panel::end() ?>
