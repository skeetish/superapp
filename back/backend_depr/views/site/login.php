<?php

/**
 * @var yii\web\View                $this
 * @var yii\widgets\ActiveForm      $form
 * @var \app\models\forms\LoginForm $model
 */
use app\backend\components\widgets\Panel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Login';
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>
<?php Panel::begin(
    [
        'containerOptions' => [ 'class' => 'container aside-xl' ] ,
        'headerOptions' => FALSE ,
    ] ) ?>

<a class="navbar-brand block">Scale</a>
<header class="wrapper text-center"><strong>Sign in to get in touch</strong></header>


<?php $form = ActiveForm::begin(
    [
        'enableClientValidation' => TRUE ,
        'enableAjaxValidation' => TRUE ,
        'validateOnSubmit' => TRUE ,
        'validateOnChange' => FALSE ,
        'validateOnType' => FALSE ,
        'errorSummaryCssClass' => 'error-summary text-danger-dker text-center m-b-md' ,
        'fieldConfig' =>
            [
                'template' => '{input}' ,
                'inputOptions' => [
                    'class' => 'form-control no-border padder-v b-light b-a' ,
                    'style' => 'height:48px'
                ]
            ] ,
    ] ) ?>

<?= $form->field( $model , 'username' )->textInput( [ 'placeholder' => $model->getAttributeLabel( 'username' ) ] ) ?>
<?= $form->field( $model , 'password' )->passwordInput( [ 'placeholder' => $model->getAttributeLabel( 'password' ) ] ) ?>
<?= $form->errorSummary( $model , [ 'header' => FALSE ] ) ?>
<?= Html::submitButton( '<i class="i i-login"></i> ' . yii::t( 'app' , 'Login' ) , [ 'class' => 'btn btn-md text-primary bg-primary-ltest b-primary btn-block' ] ) ?>
<?php ActiveForm::end() ?>

<?php Panel::end() ?>

