<?php
use yii\helpers\Html;


/**
 * @var \yii\web\View $this
 * @var string        $content
 */

$this->registerCssFile( 'css/app.v1.css' );
$this->registerCssFile( 'css/back.css' );

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="app">
<head>
    <?= Html::csrfMetaTags() ?>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<section id="content"
">
<?= $content ?>
</section>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
