<?php
use app\backend\assets\AppAsset;
use app\backend\components\widgets\VNavBar;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;


/**
 * @var \yii\web\View $this
 * @var string        $content
 */
AppAsset::register( $this );
//$this->registerJsFile( '/js/app.plugin.js' );
//$this->registerJsFile( '/js/app.v1.gzip.js' );
BootstrapPluginAsset::register( $this );
BootstrapAsset::register( $this );
$this->registerJsFile( '/js/' . $this->context->id . '.js' , [ '\yii\web\JqueryAsset' ] );
$this->registerJs( "var csrf = '" . yii::$app->request->csrfToken . "';" , view::POS_BEGIN );
$this->registerJs( "var ajaxUrl = '" . Url::toRoute( ( [ '/backend/ajax/' ] ) ) . "';" , view::POS_BEGIN );


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="app">
<head>
    <?= Html::csrfMetaTags() ?>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode( $this->title ) ?></title>
    <?php $this->head() ?>
    <link rel="apple-touch-icon" href="apple-touch-icon.png"/>
</head>
<body>

<?php $this->beginBody() ?>

<?php //Pjax::begin( [ 'options' => [ 'class' => 'vbox' ] , 'id' => 'pjax', 'formSelector' => '#pjax-form' ] ) ?>
<section class="vbox">
    <header class="header header-md navbar bg-light navbar-fixed-top-xs box-shadow">
        <div class="bg-primary navbar-header dk">
            <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
                <i class="fa fa-bars"></i> </a>
            <?= Html::a( Html::img( '/images/logo.png' , [ 'class' => 'm-r-sm' ] ) , '/' , [ 'class' => 'navbar-brand navbar-xs' ] ) ?>
        </div>
        <div class="hidden-xs">
            <div class="m-l-md m-t-md inline"><?= $this->title ?></div>
            <form class="navbar-form navbar-left input-s-lg m-t pull-right" role="search">

                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-sm bg-white b-white btn-icon">
                                <i class="fa fa-search"></i></button>
                        </span>
                        <input type="text" class="form-control input-sm no-border" placeholder="Search apps, projects...">
                    </div>
                </div>
            </form>
            <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user user">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="thumb-sm avatar pull-left">
                            <img src="/images/logo.png" alt="...">
                        </span>
                        <?= yii::$app->user->identity->userLogin ?>
                        <b class="caret"></b> </a>
                    <ul class="dropdown-menu animated fadeInRight">
                        <li>
                            <span class="arrow top"></span>
                            <a href="#">Settings</a></li>
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="#">
                                <span class="badge bg-danger pull-right">3</span>
                                Notifications </a></li>
                        <li><a href="docs.html">Help</a></li>
                        <li class="divider"></li>
                        <li><a href="modal.lockme.html" data-toggle="ajaxModal">Logout</a></li>
                        <li><a href="/backend/site/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>

            <!--<ul class="nav navbar-nav hidden-xs">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="i i-grid"></i>
                    </a>
                    <section class="dropdown-menu aside-lg bg-white on animated fadeInLeft">
                        <div class="row m-l-none m-r-none m-t m-b text-center">

                            <?php
            /*if ( isset($this->context->dropdownItems) && $dropdownItems = $this->context->dropdownItems )
                                            foreach ( $dropdownItems as $item ): */
            ?>
                                    <div class="col-xs-4">
                                        <div class="padder-v">
                                            <a href="<? /*= Yii::$app->urlManager->createUrl($item['url'])*/ ?>">
                                                <span class="m-b-xs block"> <i class="<? /*= $item['icon']*/ ?>"></i> </span>
                                                <small class="text-muted"><? /*= $item[ 'label' ] */ ?></small>
                                            </a></div>
                                    </div>
                                <?php /*endforeach */ ?>
                        </div>
                    </section>
                </li>
            </ul>-->
        </div>
    </header>
    <section>

        <section class="hbox stretch">

            <?=
            VNavBar::widget( [
                'items' => [
                    /*[
                         'label' => 'Новости' ,
                         'url' => [ '/backend/news/index/' ] ,
                         'icon' => 'i i-docs' ,
                         'items' => [
                             [
                                 'icon' => 'fa fa-table' , 'label' => yii::t( 'yii' , 'Index' ) ,
                                 'url' => [ '/backend/news/index/' ] ,
                             ] ,
                             [
                                 'icon' => 'fa fa-paste' , 'label' => yii::t( 'yii' , 'Create' ) ,
                                 'url' => [ '/backend/news/create/' ]
                             ] ,
                         ]
                     ] ,*/
                    [
                        'icon'  => 'i i-pictures' ,
                        'label' => 'Галерея' ,
                        'url'   => [ '/backend/gallery/index' ]
                    ] ,
                    [
                        'icon'  => 'fa fa-tasks' ,
                        'label' => 'Дневник' ,
                        'url'   => [ '/backend/trainings/index' ]
                    ] ,
                    [
                        'icon'  => 'fa fa-cog' ,
                        'label' => 'Схемы' ,
                        'url'   => [ '/backend/scheme/index' ]
                    ] ,
                    [
                        'icon'  => 'fa fa-download' ,
                        'label' => 'Торренты' ,
                        'url'   => [ '/backend/torrents/parse' ]
                    ] ,
                ] ,
            ] );
            ?>
            <section id="content">
                <section class="vbox">
                    <!--                    <header class="header bg-white b-b b-light">--><!----><!--                    </header>-->
                    <section class="scrollable wrapper">

                        <?= $content ?>

                    </section>
                    <!--<footer class="footer bg-white b-t b-light">
                        <p>This is a footer</p>
                    </footer>-->
                </section>
                <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
            </section>
        </section>
    </section>
    <?php //Pjax::end() ?>
</section>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
