<?php
use app\backend\assets\AppAsset;
use yii\helpers\Html;


/**
 * @var \yii\web\View $this
 * @var string        $content
 */
AppAsset::register( $this );




?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="app">
<head>
    <?= Html::csrfMetaTags() ?>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode( $this->title ) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">
    <?= $content ?>
</section>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
