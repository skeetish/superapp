<?php
/* @var \yii\bootstrap\ActiveForm $form */

use app\backend\components\widgets\Panel;
use app\models\SchemeDay;
use app\models\SchemeDayExercise;
use yii\helpers\Html;


?>

<div class="form-group field-scheme-schemedays">
    <div class="col-xs-12">
        <?php Panel::begin( [ 'headerOptions' => FALSE ] ) ?>
        <?php
        if ( $model->schemeDays )
            foreach ( $model->schemeDays as $k => $day ) :?>
                <div class="col-xs-4 b-r b-light" data-role="scheme-day" data-index="<?= $k ?>">
                    <?php $schemeDay = new SchemeDay() ?>
                    <?php $schemeDay->attributes = $day; ?>
                    <?=
                    $form
                        ->field( $schemeDay , "[$k]dayName" , [ 'options' => [ 'class' => 'form-group m-b-none' ] , 'horizontalCssClasses' => [ 'offset' => FALSE , 'wrapper' => 'col-xs-12' ] ] )
                        ->textInput( [ 'class' => 'm-t-xs form-control' ] )
                        ->label( FALSE )
                        ->error( FALSE );
                    ?>
                    <?=
                    $form
                        ->field( $schemeDay , "[$k]dayExpression" , [ 'options' => [ 'class' => 'form-group m-b-none' ] , 'horizontalCssClasses' => [ 'offset' => FALSE , 'wrapper' => 'col-xs-12' ] ] )
                        ->textInput( [ 'class' => 'm-t-xs form-control' ] )
                        ->label( FALSE )
                        ->error( FALSE );
                    ?>
                    <div data-role="day-exercises" class="col-xs-12 no-padder m-t-md">
                        <?php if ( $day[ 'dayExercises' ] ): ?>
                            <?php foreach ( $day[ 'dayExercises' ] as $i => $exercise ): ?>
                                <?php
                                $schemeDayExerciseModel = new SchemeDayExercise();
                                $schemeDayExerciseModel->attributes = $exercise;
                                ?>
                                <div data-role="day-exercise" data-index="<?= $i ?>">
                                    <div class="col-xs-5 padder-small">
                                        <?= Html::activeDropDownList( $schemeDayExerciseModel , "[$k][$i]exerciseId" , $exerciseList , [ 'class' => 'form-control input-sm col-xs-4' ] ) ?>
                                    </div>
                                    <div class="col-xs-3 padder-small">
                                        <?= Html::activeDropDownList( $schemeDayExerciseModel , "[$k][$i]exerciseLinkedTo" , $dayList , [ 'prompt' => 'Нет связи' , 'class' => 'form-control input-sm col-xs-4' ] ) ?>
                                    </div>
                                    <div class="col-xs-3 padder-small">
                                        <?= Html::activeTextInput( $schemeDayExerciseModel , "[$k][$i]exerciseLinkModifier" , [ 'class' => 'form-control input-sm col-xs-4' ] ) ?>
                                    </div>
                                    <div class="col-xs-1 padder-small">
                                        <?= Html::a( '' , '' , [ 'class' => 'fa fa-2x fa-minus text-danger' ] ) ?>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>

                        <div class="col-xs-12">
                            <?= Html::a( '' , '#' , [ 'data-role' => 'add-day-exercise-button' , 'class' => 'fa fa-2x fa-plus text-success' ] ) ?>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        <?= Html::a( 'Day' , '#' , [ 'class' => 'btn btn-success btn-xs' , 'data-role' => 'add-day-button' ] ) ?>
        <?php Panel::end() ?>

    </div>