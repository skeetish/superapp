<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View                $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\News      $searchModel
 */

$this->title = Yii::t( 'app' , 'Scheme' );
$this->params[ 'breadcrumbs' ][ ] = $this->title;

?>
<?php Pjax::begin( [ 'id' => 'pjax-content' ] ) ?>
<?= Html::a( Yii::t( 'app' , 'Create {modelClass}' , [ 'modelClass' => 'News' ] ) , [ 'create' ] , [ 'class' => 'btn btn-success' ] ) ?>    </p>

<?php Panel::begin( [
    'headerOptions' => FALSE
] ) ?>
<?php
$options = [
    'dataProvider' => $dataProvider ,
    'columns' => [
        [ 'class' => 'yii\grid\SerialColumn' ] ,
        'schemeName' ,
        'schemeInterval' ,
        'schemeCustomInterval' ,
        [
            'buttons' =>
                [
                    'update' => function ( $url , $model ) { return Html::a( '' , [ 'update' , 'id' => $model->schemeNumber ] , [ 'class' => 'fa fa-edit' ] ); }

                ] ,
            'class' => 'yii\grid\ActionColumn' ,

        ] ,
    ] ,
];
?>
<?= GridView::widget( ArrayHelper::merge( $options , Backend::widgetOptions( 'GridView' ) ) ); ?>

<?php Panel::end() ?>
<?php Pjax::end() ?>
