<?php
use app\backend\components\widgets\Panel;
use app\models\SchemeModifier;
use yii\helpers\Html;

?>
<?php Panel::begin( [ 'headerOptions' => FALSE ] ) ?>
<div class="form-group col-xs-12">
    <?php
    if ( $model->schemeModifiers )
        foreach ( $model->schemeModifiers as $k => $modifier ) :?>
            <div class="col-xs-2 b-r b-light" data-role="scheme-modifier" data-index="<?= $k ?>">

                <?php $schemeModifier = new SchemeModifier() ?>
                <?php $schemeModifier->attributes = $modifier; ?>
                <?=
                $form
                    ->field( $schemeModifier , "[$k]modifierValue" , [ 'options' => [ 'class' => 'form-group m-b-none' ] , 'horizontalCssClasses' => [ 'offset' => FALSE , 'wrapper' => 'col-xs-12' ] ] )
                    ->textInput( [ 'class' => 'm-t-xs form-control' ] )
                    ->label( FALSE )
                    ->error( FALSE );
                ?>
                <?=
                $form
                    ->field( $schemeModifier , "[$k]modifierLabel" , [ 'options' => [ 'class' => 'form-group m-b-none' ] , 'horizontalCssClasses' => [ 'offset' => FALSE , 'wrapper' => 'col-xs-12' ] ] )
                    ->textInput( [ 'class' => 'm-t-xs form-control' ] )
                    ->label( FALSE )
                    ->error( FALSE );
                ?>
                <?=
                $form
                    ->field( $schemeModifier , "[$k]modifierWeightIncrease" , [ 'horizontalCssClasses' => [ 'offset' => FALSE , 'wrapper' => 'col-xs-12' ] ] )
                    ->checkbox();
                ?>
                <?= Html::errorSummary( $schemeModifier ) ?>

            </div>
        <?php endforeach ?>
    <?= Html::a( 'Md' , '#' , [ 'class' => 'btn btn-success btn-xs' , 'data-role' => 'add-modifier-button' ] ) ?>
</div>
<?php Panel::end() ?>
