<?php

/**
 * @var yii\web\View $this
 */

use app\models\forms\SchemeDayForm;

$this->title = Yii::t( 'app' , 'Update {modelClass}: ' , [ 'modelClass' => 'News' , ] ) . ' ' . $model->schemeAlias;
$this->params[ 'breadcrumbs' ][ ] = Yii::t( 'app' , 'Update' );
?>
<?= $this->render( '_form' , [ 'model' => $model ] ) ?>
