<?php
use app\backend\components\widgets\Panel;
use app\models\Exercise;

?>
<?php Panel::begin( [ 'headerOptions' => FALSE , 'options' => [ 'class' => 'panel panel-default b-r-none' ] ] ) ?>
<?= $form->field( $model , 'schemeName' )->textInput( [ 'maxlength' => 255 ] ) ?>
<?= $form->field( $model , 'schemeDescription' )->textarea() ?>
<?= $form->field( $model , 'schemeWeightsIncrease' )->dropDownList( Exercise::increaseTypes() ); ?>
<?= $form->field( $model , 'schemeSeparateModifiers' )->checkbox() ?>
<?php Panel::end() ?>

