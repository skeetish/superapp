<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use app\common\assets\packages\HandlebarsAsset;
use app\models\ExerciseName;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var app\models\News        $model
 * @var yii\widgets\ActiveForm $form
 */
$exerciseList = ExerciseName::Find()->asArray()->all();
$exerciseList = ArrayHelper::index( $exerciseList , 'exerciseId' );
$exerciseList = ArrayHelper::getColumn( $exerciseList , 'exerciseName' , TRUE );

$dayList = ArrayHelper::getColumn( $model->schemeDays , 'dayName' );

HandlebarsAsset::register( $this );
?>

<?php Panel::begin( [
    'headerContent' => Html::tag( 'h4' , ' #' . Html::encode( $model->schemeAlias ) , [ 'class' => 'font-thin ' . ( $model->isNewRecord ? 'fa fa-plus' : ' fa fa-edit' ) ] )
] ) ?>

<?php $form = ActiveForm::begin( Backend::widgetOptions( 'BSActiveForm' ) ); ?>
<?php $form->fieldConfig[ 'horizontalCssClasses' ][ 'label' ] = 'col-sm-2' ?>
<?php $form->fieldConfig[ 'horizontalCssClasses' ][ 'wrapper' ] = 'col-sm-10' ?>

<?=
Tabs::widget(
    [
        'itemOptions' => [ 'class' => 'fade in' ] ,
        'navType' => 'nav-justified nav-tabs' ,
        'items' => [
            [
                'label' => 'Настройки' ,
                'content' => $this->render( '_form_settings' , compact( 'model' , 'form' , 'exerciseList' ) ) ,
            ] ,
            [
                'label' => 'Модификаторы' ,
                'content' => $this->render( '_form_modifiers' , compact( 'model' , 'form' , 'exerciseList' ) ) ,
            ] ,
            [
                'label' => 'Дни' ,
                'active' => TRUE ,

                'content' => $this->render( '_form_days' , compact( 'dayList' , 'model' , 'form' , 'exerciseList' ) ) ,
            ]
        ]
    ] );

?>




<div class="col-lg-offset-1 col-lg-11 btn-group">
    <?= Html::submitButton( $model->isNewRecord ? Yii::t( 'yii' , 'Create' ) : Yii::t( 'yii' , 'Save' ) , [ 'class' => ( $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ) ] ) ?>
    <?= Html::a( 'dummy' , [ '/backend/trainings/' , 'schemeAlias' => $model->schemeAlias , 'alias' => '35' ] , [ 'class' => 'btn btn-info' ] ) ?>
</div>
<?php ActiveForm::end(); ?>
<?php Panel::end() ?>




<script id="scheme-modifier" type="text/x-handlebars-template">
    <div>
        <div class="col-xs-2 b-r b-light" data-role="scheme-modifier" data-index="{{index}}">

            <input class="form-control m-t-xs" name="Scheme[schemeModifiers][{{index}}][modifierLabel]" type="text">
            <input class="form-control m-t-xs" name="Scheme[schemeModifiers][{{index}}][modifierValue]" type="text">

            <div class="checkbox">
                <label>Увеличить вес</label>
                <input name="Scheme[schemeModifiers][{{index}}][modifierWeightIncrease]" type="checkbox">
            </div>

        </div>
    </div>
</script>

<script id="scheme-day" type="text/x-handlebars-template">
    <div>
        <div class="col-xs-2 b-r b-light" data-role="scheme-day" data-index="{{index}}">
            <input class="form-control m-t-xs" name="Scheme[schemeDays][{{index}}][dayName]" type="text">
            <input class="form-control m-t-xs" name="Scheme[schemeDays][{{index}}][dayExpression]" type="text">
        </div>
    </div>
</script>

<script id="scheme-day-exercise" type="text/x-handlebars-template">
    <div>
        <div data-index="{{exerciseIndex}}" data-role="day-exercise">
            <div class="col-xs-5 padder-small">
                <?= Html::dropDownList('SchemeDayExercise[{{dayIndex}}][{{exerciseIndex}}][exerciseId]','',$exerciseList,['class' => 'form-control input-sm col-xs-4']) ?></div>
            <div class="col-xs-3 padder-small">
                <?= Html::dropDownList('SchemeDayExercise[{{dayIndex}}][{{exerciseIndex}}][exerciseLinkedTo]' , '' , $dayList , [ 'prompt' => 'Нет связи' , 'class' => 'form-control input-sm col-xs-4' ] ) ?>
            </div>
            <div class="col-xs-3 padder-small">
                <?= Html::textInput( 'SchemeDayExercise[{{dayIndex}}][{{exerciseIndex}}][exerciseLinkModifier]' ,'' , [ 'class' => 'form-control input-sm col-xs-4' ] ) ?>
            </div>
            <div class="col-xs-1 padder-small">
            <?= Html::a( '' , '' , [ 'data-role'=>'remove-button','class' => 'fa fa-2x fa-minus text-danger' ] ) ?>
            </div>
        </div>
    </div>
</script>