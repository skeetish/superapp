<?php

/**
 * @var yii\web\View    $this
 * @var app\models\News $model
 */

$this->title = Yii::t( 'app' , 'New {modelClass}' , [
    'modelClass' => 'схема' ,
] );
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => Yii::t( 'app' , 'News' ) , 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>
<?=
$this->render( '_form' , [
    'model' => $model ,
] ) ?>

