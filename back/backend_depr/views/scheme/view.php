<?php

use app\backend\components\widgets\Panel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View    $this
 * @var app\models\News $model
 */

$this->title = $model->schemeName;
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => Yii::t( 'app' , 'News' ) , 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>

<?php Pjax::begin( [ 'id' => 'pjax-content' ] ) ?>

<?php Panel::begin( [
    'bodyOptions' => [ 'class' => 'panel-body form-horizontal' ] ,
    'headerContent' => "<h4 class='inline font-thin fa fa-eye'> #" . Html::encode( $model->schemeAlias ) . "</h4>"

] ) ?>
<?php
$options = [
    'model' => $model ,
    'attributes' => [
        'schemeAlias' ,
        'schemeName'

    ]
];?>
<?= DetailView::widget( ArrayHelper::merge( $options , $this->context->module->widgetOptions( 'DetailView' ) ) ); ?>

<div class='form-group'>
    <div class="col-lg-offset-1 col-lg-11 btn-group">

        <?= Html::a( Yii::t( 'yii' , 'Update' ) , [ 'update' , 'id' => $model->schemeNumber ] , [ 'class' => 'btn btn-info' ] ) ?>
        <?=
        Html::a( Yii::t( 'yii' , 'Delete' ) , [ 'delete' , 'id' => $model->schemeNumber ] , [
            'class' => 'btn btn-danger' ,
            'data' => [
                'confirm' => Yii::t( 'yii' , 'Are you sure you want to delete this item?' ) ,
                'method' => 'post' ,
            ] ,
        ] ) ?>
    </div>
</div>
<?php Panel::end() ?>
<?php Pjax::end() ?>
