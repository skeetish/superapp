<?php

/**
 * @var yii\web\View        $this
 * @var app\models\Training $model
 */

$this->title = Yii::t( 'app' , 'Create {modelClass}' , [
    'modelClass' => 'Training' ,
] );
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => Yii::t( 'app' , 'Trainings' ) , 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>
<div class="training-create">

    <?=
    $this->render( '_form' , [
        'model' => $model ,
    ] ) ?>

</div>
