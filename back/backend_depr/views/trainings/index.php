<?php

use app\backend\Backend;
use app\backend\components\widgets\ModalExt;
use app\backend\components\widgets\Panel;
use app\models\Exercise;
use app\models\ExerciseName;
use app\models\forms\NewCommentForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View                      $this
 * @var yii\data\ActiveDataProvider       $dataProvider
 * @var app\models\search\trainings       $searchModel
 * @var                                   $columnCount
 * @var array                             $modifierValues
 * @var \app\models\Scheme                $schemeModel
 * @var \app\models\forms\NewExerciseForm $newExerciseForm
 * @var                                   $newTrain
 */


if ( $columnCount )
    $nameColumnSize = floor( ( 12 - $columnCount ) / 2 );
else $nameColumnSize = 3;

$this->registerJs( "var nameColumnSize = '$nameColumnSize'" , View::POS_BEGIN );
$this->registerJs( "$(document).on('mouseenter','[data-role=set-comment-button]',function (){ $(this).popover('show')})" );
$this->registerJs( "$(document).on('mouseleave','[data-role=set-comment-button]',function (){ $(this).popover('hide')})" );

$exercisesList = ExerciseName::Find()->asArray()->all();
$exercisesList = ArrayHelper::index( $exercisesList , 'exerciseId' );
$exercisesList = ArrayHelper::getColumn( $exercisesList , 'exerciseName' , TRUE );
$exerciseIncTypes = Exercise::increaseTypes();

$this->registerAssetBundle( 'app\common\assets\packages\HandlebarsAsset' );
$this->title = 'Схема: ' . Html::encode( $schemeModel->schemeName );
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>

<?php
$modal = ModalExt::begin( [
    'toggleButton' => FALSE ,
    'header' => Html::tag( 'i' , '' , [ 'class' => 'fa fa-comment' ] ) . ' <b>Комментарий</b>' ,
    'bodyOptions' => [ 'class' => 'modal-body panel-body' ]
] );
$this->params[ 'modalId' ] = $modal->id;
$this->registerJs( "var modalId = '" . $this->params[ 'modalId' ] . "'" , View::POS_BEGIN );
?>

<?php
$newCommentForm = new NewCommentForm();
$form = ActiveForm::begin(
    [
        'enableClientValidation' => TRUE ,
        'options' => [ 'data-role' => 'comment-form' ] ,
    ]
) ?>
<?=
$form
    ->field( $newCommentForm , 'id' )
    ->hiddenInput( [ 'data-role' => 'entry-id' ] )
    ->error( FALSE )
    ->label( FALSE ) ?>

<?=
$form
    ->field( $newCommentForm , 'comment' )
    ->textarea( [ 'data-role' => 'exercise-comment' ] ) ?>

<?= Html::submitButton( 'Submit' , [ 'class' => 'btn btn-success' ] ) ?>
<?php ActiveForm::end() ?>
<?php ModalExt::end() ?>

<?php Panel::begin( [
    'headerOptions' => FALSE
] ) ?>

<?php Pjax::begin( [ 'id' => 'generated-train' , 'linkSelector' => '[data-role=refresh-button]' ] ) ?>

<!--<div class="btn-group">-->
<?php Html::a( Yii::t( 'app' , 'Mobile Version' ) , [ '/backend/trainings' , 'schemeAlias' => $schemeModel->schemeAlias , 'version' => 'mob' ] , [ 'class' => 'btn btn-default btn-xs m-b-xs' ] ) ?>
<?php Html::a( Yii::t( 'app' , 'Print Version' ) , [ '/backend/trainings/print' , 'schemeAlias' => $schemeModel->schemeAlias ] , [ 'class' => 'btn btn-default btn-xs m-b-xs' ] ) ?>
<!--</div>-->
<?php if ( $newTrain && $newTrain->isNewRecord ) : ?>

    <?=
    $this->render( '_item' , [
        'model' => $newTrain ,
        'modalId' => $modal->id ,
        'schemeModel' => $schemeModel ,
        'nameColumnSize' => $nameColumnSize ,
        'exercisesList' => $exercisesList ,
        'exerciseIncTypes' => $exerciseIncTypes ,
        'newExerciseForm' => $newExerciseForm ,
    ] ) ?>
<?php endif ?>
<?php Pjax::end() ?>

<?php
$options = [
    'summary' => FALSE ,
    'itemView' => '_item' ,
    'itemOptions' => [ 'tag' => FALSE ] ,
    'viewParams' => compact(
        'nameColumnSize' ,
        'exercisesList' ,
        'schemeModel' ,
        'exerciseIncTypes' ,
        'newExerciseForm'
    ) ,
    'dataProvider' => $dataProvider ,
    'pager' => Backend::widgetOptions( 'LinkPager' )
];
$trainingListWidget = ListView::widget( $options );
echo $trainingListWidget;
?>
<!--todo цвет крестиков удаления упражнения-->

<?php Panel::end() ?>




<script id="exercise" type="text/x-handlebars-template">
    <div>
        <div class="b-b row{{#unless params.completed}} bg-danger-ltest{{/unless}}" data-role="exercise" data-index="{{index}}">
            <div class="col-xs-{{nameColumnSize}}">
                <a class="btn-link" href="{{site.url}}">{{params.exerciseName}}</a>
                <a class="fa fa-minus {{#if isGenerated}}text-primary{{else}}text-danger{{/if}}" href="#" data-role="remove-button" data-type="exercise" data-pjax="0"></a>
            </div>

            <div class="col-xs-{{weightColumnSize}} b-r m-r-xs">
                {{#each weights}}
                <div class="short-column col-xs-1">{{this}}</div>
                {{/each}}
            </div>

            <input name="Training[exercises][{{index}}][exerciseId]" value="{{params.exerciseId}}" autocomplete="off" type="hidden">
            <input name="Training[exercises][{{index}}][increaseType]" value="{{params.increaseType}}" autocomplete="off" type="hidden">

            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                <span style="display:none" data-role="param-text" class="{{#if params.increaseWeight}}text-success-dker font-bold{{/if}}">{{params.weight}}</span>
                <input class="form-control m-t-xxs  m-l-n-xxs" name="Training[exercises][{{index}}][weight]" value="{{params.weight}}" autocomplete="off" data-role="param-input" type="text">
            </div>
            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                <input class="form-control m-t-xxs m-l-n-xxs" name="Training[exercises][{{index}}][count]" value="{{params.count}}" autocomplete="off" data-role="param-input" type="text">
            </div>

            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                <input class="form-control m-t-xxs m-l-n-xxs" name="Training[exercises][{{index}}][reps]" value="{{params.reps}}" autocomplete="off" data-role="param-input" type="text">
            </div>
            <div class="exercise-param tiny-column pull-left text-left">
                {{params.weightStep}}
            </div>
            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                {{#if params.completed}}
                <input name="Training[exercises][{{index}}][completed]" autocomplete="off" style="margin-top: 0px;" checked value="1" data-role="param-input" type="checkbox"> {{else}}
                <input name="Training[exercises][{{index}}][completed]" autocomplete="off" style="margin-top: 0px;" value="1" data-role="param-input" type="checkbox"> {{/if}}
            </div>
            <div class="exercise-param tiny-column pull-left text-left">
                <input name="Training[exercises][{{index}}][comment]" value="{{params.comment}}" autocomplete="off" type="hidden">
                <a class="fa fa-comment text-muted" href="#" data-toggle="modal" data-content="{{params.comment}}" data-target="#w0" data-role="set-comment-button"></a>
            </div>

        </div>
    </div>

</script>

