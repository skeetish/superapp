<?php

use app\backend\Backend;
use app\backend\components\widgets\Panel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View           $this
 * @var app\models\News        $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<?php Panel::begin( [
    'headerContent' => "<h5 class='inline font-thin'><i class='fa fa-edit'></i> " . yii::t( 'yii' , 'Update' ) . "</h5>"
] ) ?>

<?php
$options = Backend::widgetOptions( 'BSActiveForm' );
$options[ 'fieldConfig' ][ 'horizontalCssClasses' ] = [
    'label' => 'col-sm-2' ,
    'wrapper' => 'col-sm-10' ,
    'error' => 'm-b-none m-t-none'
];

$form = ActiveForm::begin( $options ); ?>
<?= $form->field( $model , 'exerciseName' )->textInput( [ 'maxlength' => 255 ] ) ?>
<?= $form->field( $model , 'weightStep' )->textInput( [ 'maxlength' => 255 ] ) ?>
<?= $form->field( $model , 'exerciseAlias' )->textInput( [ 'maxlength' => 255 ] ) ?>

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10 btn-group">
            <?= Html::submitButton( $model->isNewRecord ? Yii::t( 'yii' , 'Create' ) : Yii::t( 'yii' , 'Save' ) , [ 'class' => ( $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary' ) ] ) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php Panel::end() ?>