<?php

use app\backend\components\widgets\Panel;
use app\common\components\helpers\Date;

/** @var $dataProvider \yii\data\ActiveDataProvider */
$trainingModels = $dataProvider->getModels();
?>
    <!--todo добавить виджету проверку на наличие класса в настройках блока-->
<?php Panel::begin( [ 'headerOptions' => FALSE , 'options' => [ 'class' => 'panel panel-default box-shadow' ] , 'bodyOptions' => [ 'style' => 'padding-top:0!important' , 'class' => 'panel-body' ] ] ) ?>
    <!--        --><? //= Html::a( Yii::t( 'app' , 'Full Version' ) , [ '/backend/trainings' , 'schemeAlias' => $schemeModel->schemeAlias,'version' => 'full' ] , [ 'class' => 'btn btn-success box-shadow m-b-xs' ] ) ?>
<?php if ( $newTrain ): ?>
    <div class="row generated b b-primary bg-primary-ltest ">
        <div class="col-xs-12 text-center padder-v b-b b-primary">
            <?= Date::display( $newTrain->date , TRUE ) ?><?php if ( count( $schemeModel->trainModifiers ) > 1 ) : ?>--<?= $model->trainModifierValue * 100 ?>%<?php endif ?>
        </div>
        <div class="col-xs-12">

            <?php /** @var $exercise \app\models\Exercise */
            foreach ( $newTrain->exercises as $i => $exercise ): ?>
                <div class="<?= ( $i === 0 ) ? 'b-b' : 'b-b' ?> row">

                    <div class="col-xs-6 text-right" style="direction: rtl">
                        <span class="label label-primary"><?= $exercise->weight ?></span> <?= $exercise->params->exerciseName ?>
                    </div>
                    <div class="col-xs-6 text-sm">
                        <?php foreach ( $exercise->weights as $weight ): ?>
                            <?= $weight ?>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php endforeach ?>

        </div>
    </div>
<?php endif ?>

<?php /** @var $model \app\models\Training */
foreach ( $trainingModels as $k => $model ) : ?>
    <div class="row <?= ( date::display( $model->date ) == date::display( time() ) ) ? 'bg-success-ltest' : NULL ?> <?= ( $k === 0 ) ? 'b-t b-b b-2x' : 'b-b b-2x' ?>">
        <div class="col-xs-12 text-center padder-v">
            <span><?= Date::display( $model->date , TRUE ) ?><?php if ( count( $schemeModel->trainModifiers ) > 1 ) : ?>--<?= $model->trainModifierValue * 100 ?>%<?php endif ?></span>
        </div>
        <div class="col-xs-12">

            <?php /** @var $exercise \app\models\Exercise */
            foreach ( $model->exercises as $i => $exercise ): ?>
                <div class="row <?= ( $i === 0 ) ? 'b-t' : NULL ?> <?= ( $i != count( $model->exercises ) - 1 ) ? 'b-b' : NULL ?> <?= !$exercise->completed ? 'bg-danger-ltest' : '' ?>">

                    <div class="col-xs-6 text-right" style="direction: rtl">
                        <span class="label label-default"><?= $exercise->weight ?></span> <?= $exercise->params->exerciseName ?>
                    </div>
                    <div class="col-xs-6 text-sm">
                        <?php foreach ( $exercise->weights as $weight ): ?>
                            <?= $weight ?>
                        <?php endforeach ?>
                    </div>
                </div>

            <?php endforeach ?>
        </div>
    </div>
<?php endforeach ?>
<?php Panel::end() ?>