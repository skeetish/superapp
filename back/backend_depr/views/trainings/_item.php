<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 09.06.14
 * Time: 22:09
 * /**
 * @var yii\web\View           $this
 * @var app\models\News        $model
 * @var yii\widgets\ActiveForm $form
 * @var \app\models\Training   $model
 * @var \app\models\Exercise   $exercise
 * @var                        $columnCount
 * @var                        $exercisesIncreaseTypes
 */
/** @var $newExerciseForm \app\models\forms\NewExerciseForm */
/** @var $schemeModel \app\models\Scheme */
use app\backend\components\widgets\Panel;
use app\common\components\helpers\Date;
use kartik\widgets\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$isGenerated = $model->isNewRecord;
$model->populateRelation( 'scheme' , $schemeModel );
$schemeModifiers = ArrayHelper::getColumn( $model->scheme->schemeModifiers , 'modifierLabel' );
$schemeDays = ArrayHelper::getColumn( $model->scheme->schemeDays , 'dayName' );
$weightIncreased = $model->scheme->checkTrainWeightIncrease( $model->trainModifier );

$color = [
    'success' => [
        'panel' => 'success bg-success-ltest' ,
        'action-bar' => 'text-success-dker'
    ] ,
    'primary' => [
        'panel' => 'primary bg-primary-ltest' ,
        'action-bar' => 'text-primary'
    ] ,
    'default' => [
        'panel' => 'default' ,
        'action-bar' => 'text-muted'
    ]
];

if ( $isGenerated )
    $class = 'primary';
else if ( date::display( $model->date ) == date::display( time() ) )
    $class = 'success';
else $class = 'default';

$function = "$(this).parents('[data-role=param-field]').find('[data-role=param-text]').html($(this).val());";
?>

<?php Panel::begin(
    [
        'options' => [
            'class' => 'box-shadow panel panel-' . ArrayHelper::getValue( $color , $class . '.panel' ) ,
            'data-role' => 'training'
        ] ,
        'headerOptions' => FALSE ,
        'bodyOptions' => [ 'class' => 'panel-body' ] ,
    ] ) ?>
<?php ActiveForm::begin(
    [
        'options' => [
            'data-key' => ( !$isGenerated ) ? $model->id : 'generated' ,
            'id' => ( !$isGenerated ) ? 'train-' . $model->id : 'train-generated' ,
            'data-role' => 'training-form'
        ]
    ] ) ?>
<!--todo Title нормальные, возможно с поповером-->
<div data-role="training-params" class="text-center col-lg-2 padder-v">
    <p class="action-bar">
        <?php if ( $isGenerated ) : ?>
            <?= html::a( '' , '#' , [ 'data-role' => 'save-button' , 'data-pjax' => '0' , 'class' => 'fa fa-check fa-2x text-primary' ] ) ?>
            <!--            --><? //= html::a( '' , [ '/backend/trainings/' , 'schemeAlias' => $model->scheme->schemeAlias ] , [ 'data-role' => 'refresh-button' , 'data-pjax' => '1' , 'class' => 'fa fa-refresh fa-2x text-primary' ] ) ?>
            <?= html::a( '' , '#' , [ 'style' => 'position:relative;top:2px' , 'data-role' => 'edit-button' , 'class' => "fa fa-edit fa-2x " . ArrayHelper::getValue( $color , $class . '.action-bar' ) ] ) ?>
        <?php else: ?>
            <?= html::a( '' , '#' , [ 'style' => 'position:relative;top:2px' , 'data-role' => 'edit-button' , 'class' => "fa fa-edit fa-2x " . ArrayHelper::getValue( $color , $class . '.action-bar' ) . " m-b-n-xs" ] ) ?>
            <?= html::a( '' , [ '/backend/trainings/delete/' , 'id' => $model->id ] , [ 'data-role' => 'remove-button' , 'data-type' => 'training' , 'data-pjax' => '0' , 'class' => 'fa fa-trash-o fa-2x ' . ArrayHelper::getValue( $color , $class . '.action-bar' ) ] ) ?>
        <?php endif ?>
    </p>

    <?php if ( !$isGenerated )
        echo Html::hiddenInput( 'Training[id]' , $model->id , [ 'autocomplete' => 'off' ] ) ?>

    <?= Html::hiddenInput( 'Training[schemeAlias]' , $model->scheme->schemeAlias , [ 'autocomplete' => 'off' ] ) ?>
    <div class="train-date" data-role="param-field">
        <span data-role="param-text"><?= Date::display( html::encode( $model->date ) , TRUE ) ?></span>
        <?= DatePicker::widget( [ 'pluginEvents' => [ 'changeDate' => 'function () {' . $function . '}' ] , 'type' => DATEPICKER::TYPE_INPUT , 'pluginOptions' => [ 'format' => 'DD, d MM ' , 'language' => FALSE ] , 'value' => Date::display( $model->date , TRUE ) , 'name' => 'Training[date]' , 'options' => [ 'autocomplete' => 'off' , 'class' => 'date form-control' , 'style' => 'display:none' , 'data-role' => 'param-input' ] ] ) ?>

    </div>
    <div class="train-modifier" data-role="param-field">
        <?php if ( count( $schemeModifiers ) > 1 ): ?>
            <span data-role="param-text"><?= $model->trainModifierValue * 100 ?>%</span>
            <?= Html::dropDownList( "Training[trainModifier]" , $model->trainModifier , $schemeModifiers , [ 'autocomplete' => 'off' , 'class' => 'form-control' . ( ( count( $schemeDays ) > 1 && count( $schemeModifiers ) > 1 ) ? ' short' : '' ) , 'style' => 'display:none' , 'data-role' => 'param-input' ] ) ?>
        <?php else: ?>
            <?= Html::hiddenInput( 'Training[trainModifier]' , $model->trainModifier , [ 'autocomplete' => 'off' ] ) ?>
        <?php endif ?>
    </div>
    <div class="train-day" data-role="param-field">
        <?php if ( count( $schemeDays ) > 1 ): ?>
            <?= Html::dropDownList( "Training[trainDay]" , $model->trainDay , $schemeDays , [ 'autocomplete' => 'off' , 'class' => 'form-control' . ( ( count( $schemeDays ) > 1 && count( $schemeModifiers ) > 1 ) ? ' short' : '' ) , 'style' => 'display:none' , 'data-role' => 'param-input' ] ) ?>
        <?php else: ?>
            <?= Html::hiddenInput( 'Training[trainDay]' , $model->trainDay , [ 'autocomplete' => 'off' ] ) ?>
        <?php endif ?>
    </div>
</div>
<div data-role="training-exercises" class="b-l col-lg-10 text-right">

    <?php foreach ( $model->exercises as $k => $exercise ): ?>
        <div class="b-b row<?= ( !$exercise->completed && !$isGenerated ) ? ' bg-danger-ltest' : '' ?>" data-role="exercise" data-id="<?= $exercise->id ?>" data-index="<?= $k ?>">

            <div class="col-xs-<?= $nameColumnSize ?>">
                <?= html::a( $exercise->params->exerciseName , [ '/backend/trainings/exercise' , 'alias' => $exercise->params->exerciseAlias , 'schemeAlias' => $schemeModel->schemeAlias ] , [ 'class' => 'btn-link' ] ) ?>
                <?= html::a( '' , '#' , [ 'data-role' => 'remove-button' , 'data-type' => 'exercise' , 'data-pjax' => '0' , 'class' => 'fa fa-minus text-' . ( ( $isGenerated ) ? 'primary' : 'danger' ) ] ) ?>
            </div>
            <div class="col-xs-<?= 9 - $nameColumnSize ?> b-r m-r-xs">
                <?php foreach ( $exercise->weights as $i => $weight ) : ?>
                    <div class='short-column col-xs-1'><?= $weight ?></div>
                <?php endforeach; ?>

            </div>

            <?= Html::hiddenInput( "Training[exercises][$k][exerciseId]" , $exercise->exerciseId , [ 'autocomplete' => 'off' ] ) ?>
            <?= Html::hiddenInput( "Training[exercises][$k][exerciseAlias]" , $exercise->params->exerciseAlias , [ 'autocomplete' => 'off' ] ) ?>
            <?= Html::hiddenInput( "Training[exercises][$k][increaseType]" , $exercise->increaseType , [ 'autocomplete' => 'off' ] ) ?>
            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                <span class="<?= ( ( !$isGenerated && $weightIncreased ) || ( $isGenerated && $weightIncreased && $exercise->completed ) ) ? 'text-success-dker font-bold' : '' ?>" data-role='param-text'><?= $exercise->weight ?></span>
                <?= Html::textInput( "Training[exercises][$k][weight]" , $exercise->weight , [ 'autocomplete' => 'off' , 'style' => 'display:none' , 'class' => 'form-control m-t-xxs m-l-n-xxs' , 'data-role' => 'param-input' ] ) ?>
            </div>
            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                <?= Html::textInput( "Training[exercises][$k][count]" , $exercise->count , [ 'autocomplete' => 'off' , 'style' => 'display:none' , 'class' => 'form-control m-t-xxs m-l-n-xxs' , 'data-role' => 'param-input' ] ) ?>
            </div>

            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                <?= Html::textInput( "Training[exercises][$k][reps]" , $exercise->reps , [ 'autocomplete' => 'off' , 'style' => 'display:none' , 'class' => 'form-control m-t-xxs m-l-n-xxs' , 'data-role' => 'param-input' ] ) ?>
            </div>
            <div class="exercise-param tiny-column pull-left text-left">
                <?= $exercise->params->weightStep ?>
            </div>

            <div class="exercise-param tiny-column pull-left text-left" data-role="param-field">
                <?= Html::checkbox( "Training[exercises][$k][completed]" , ( $isGenerated ) ? 1 : $exercise->completed , [ 'autocomplete' => 'off' , 'style' => 'display:none; margin-top:0' , 'data-role' => 'param-input' ] ) ?>
            </div>

            <div class="exercise-param tiny-column pull-left text-left">
                <?= Html::hiddenInput( "Training[exercises][$k][comment]" , $exercise->comment , [ 'autocomplete' => 'off' ] ) ?>

                <?php
                $commentClass = 'text-muted';

                if ( $exercise->comment ) {
                    $commentClass = 'text-success-dker';
                }
                ?>

                <?=
                Html::a( '' , '#' , [
                    'data-toggle' => "modal" ,
                    'data-content' => $exercise->comment ,
                    'data-target' => "#" . $this->params[ 'modalId' ] ,
                    'data-role' => 'set-comment-button' ,
                    'class' => 'fa fa-comment ' . $commentClass ,
                ] ) ?>
            </div>

        </div>
    <?php endforeach; ?>

</div>
<?php ActiveForm::end() ?>

<?php $form = ActiveForm::begin(
    [
        'fieldConfig' => [ 'template' => '{input}' ] ,
        'enableClientValidation' => TRUE ,
        'validateOnBlur' => TRUE ,
        'validateOnChange' => FALSE ,
        'options' =>
            [
                'data-key' => ( !$isGenerated ) ? $model->id : 'generated' ,
                'data-role' => 'creation-form' ,
                'style' => 'display:none'
            ]
    ] )?>
<div class="col-lg-10 col-lg-offset-2">

    <!--    //todo Placeholderы  прицепить к лейблам-->
    <?= $form->field( $newExerciseForm , 'exerciseId' , [ 'options' => [ 'class' => 'col-xs-2 col-xs-offset-' . ( $nameColumnSize - 2 ) . ' m-t-xs text-right' ] ] )->dropDownList( $exercisesList , [ 'autocomplete' => 'off' , 'class' => 'form-control text-right' ] ) ?>
    <?= $form->field( $newExerciseForm , 'weight' , [ 'options' => [ 'class' => 'padder pull-left m-t-xs short-column' ] ] )->textInput( [ 'autocomplete' => 'off' , 'placeholder' => $newExerciseForm->getAttributeLabel( 'weight' ) , 'class' => 'form-control text-right' ] ); ?>
    <?= $form->field( $newExerciseForm , 'count' , [ 'options' => [ 'class' => 'padder pull-left m-t-xs short-column' ] ] )->textInput( [ 'autocomplete' => 'off' , 'placeholder' => $newExerciseForm->getAttributeLabel( 'count' ) , 'class' => 'form-control text-right' ] ); ?>
    <?= $form->field( $newExerciseForm , 'reps' , [ 'options' => [ 'class' => 'padder pull-left m-t-xs short-column' ] ] )->textInput( [ 'autocomplete' => 'off' , 'placeholder' => $newExerciseForm->getAttributeLabel( 'reps' ) , 'class' => 'form-control text-right' ] ); ?>
    <?= $form->field( $newExerciseForm , 'increaseType' , [ 'options' => [ 'class' => 'col-xs-2 m-t-xs text-right' ] ] )->dropDownList( $exerciseIncTypes , [ 'autocomplete' => 'off' , 'class' => 'form-control text-left' ] ) ?>

    <div class="pull-left m-t-xs tiny-column text-left">
        <?= html::a( '' , '#' , [ 'data-role' => 'add-button' , 'class' => 'fa fa-plus fa-2x ' . ( ( $isGenerated ) ? 'text-primary' : 'text-success' ) ] ) ?>

    </div>

</div>
<div class="col-lg-10 col-lg-offset-2 text-danger-dker">
    <?= $form->errorSummary( $newExerciseForm , [ 'header' => FALSE , 'class' => 'error-summary text-left col-lg-10 col-xs-offset-3' ] ) ?>
</div>
<?php ActiveForm::end() ?>

<?php Panel::end() ?>





