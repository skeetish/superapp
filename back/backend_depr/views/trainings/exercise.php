<?php

use app\backend\components\widgets\Chart;
use app\backend\components\widgets\Panel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var yii\web\View    $this
 * @var app\models\News $model
 * @var                 $models Array
 */
$this->title = Html::encode( $params->exerciseName );
$this->params[ 'breadcrumbs' ][ ] = [ 'label' => Yii::t( 'app' , 'News' ) , 'url' => [ 'index' ] ];
$this->params[ 'breadcrumbs' ][ ] = $this->title;
?>
<div class="exercise-view">

    <?php Panel::begin( [
        'containerOptions' => [ 'class' => 'col-xs-6 no-padder' ] ,
        'bodyOptions' => [ 'class' => 'panel-body form-horizontal' ] ,
        'headerContent' => "<h5 class='inline font-thin'><i class='fa fa-eye'></i> " . yii::t( 'app' , 'Stats' ) . "</h5>"

    ] ) ?>

    <?php
    ArrayHelper::multisort( $models , 'training.date' );

    $data = ArrayHelper::getColumn( $models , function ( $element ) {
        $date = ArrayHelper::getValue( $element , 'training.date' );
        $year = date( "Y" , $date );
        $month = date( "m" , $date ) - 1;
        $day = date( "d" , $date );
        return [ "Date.UTC($year, $month, $day)" , ArrayHelper::getValue( $element , 'weight' ) ];
    } );
    //    todo проставить No-padder
    echo Chart::widget( [
        'values' => [
            'name' => $params->exerciseName ,
            'data' => $data
        ] ,

        'chartOptions' => [
            'plotOptions' => [
                'line' => [
                    'dataLabels' => [ 'enabled' => 'true' ] ,
                    'enableMouseTracking' => 'false' ,
                ] ,
            ] ,
            'legend' => FALSE ,
            'subtitle' => [ 'text' => $params->exerciseName ] ,
            'title' => [ 'text' => FALSE ] ,
            'yAxis' => [
                'labels' => FALSE ,
                'title' => [ 'text' => FALSE ] ,

            ] ,
            'xAxis' => [
                'type' => 'datetime' ,
                'dateTimeLabelFormats' => [
                    'month' => '%e. %b' ,
                    'year' => '%b'
                ]

            ] ,
        ]
    ] ) ?>
    <?php Panel::end() ?>

    <div class="col-xs-6 no-padder">
        <?= $this->render( '_form' , [ 'model' => $params ] ) ?>
    </div>

</div>

