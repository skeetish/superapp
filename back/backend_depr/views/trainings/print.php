<?php

use app\common\components\helpers\Date;

?>
<style type="text/css">
    body
    {
        border-collapse: 0;
        letter-spacing: 1px;
    }

    .col-short
    {
        width: 45px;
        margin-right: 5px;
    }

    .col-medium
    {
        width: 75px;
        margin-right: 5px;
    }

    .border-left-xs
    {
        border-left: 1px solid black;
    }

    .border-right-xs
    {
        border-right: 1px solid black;
    }

    .border-top-xs
    {
        border-top: 1px solid black;
    }

    .border-bottom-xs
    {
        border-bottom: 1px solid black;
    }

    .border-bottom-sm
    {
        padding: 2px;
        border-bottom: 2px solid black;
    }

    .border-light
    {
        border-color: #D3D3D3;
    }

    .table
    {
        display: table;
    }

    .padder
    {
        padding: 5px;
    }

</style>
<div class="table">
    <?php /** @var $model \app\models\Training */
    foreach ( $trainingModels as $model ) : ?>
        <div class="row col-xs-12 border-bottom-sm border-light">
            <div class="col-xs-2 text-center">
                <p><?= Date::display( $model->date ) ?></p>

                <p><?= $model->trainModifierValue * 100 ?>%</p>
            </div>
            <div class="col-xs-10 border-left-xs border-light">
                <div class="row">
                    <?php /** @var $exercise \app\models\Exercise */
                    foreach ( $model->exercises as $exercise ): ?>
                        <div class="col-xs-10 border-light border-bottom-xs <?php if ( !$exercise->completed )
                            echo 'bg-danger' ?>">
                            <div class="col-xs-3 text-right padder border-light border-right-xs"><?= $exercise->params->exerciseName ?></div>
                            <?php foreach ( $exercise->weights as $weight ): ?>
                                <div class="pull-left border-right-xs border-light text-right col-medium padder"><?= $weight ?></div>
                            <?php endforeach ?>
                        </div>
                        <div class="hidden-xs col-xs-2 border-left-xs border-light padder">
                            <div class="pull-left col-short"><?= $exercise->weight ?></div>
                            <div class="pull-left col-short"><?= $exercise->params->weightStep ?></div>
                            <?php if ( !$exercise->completed ): ?>
                                <div class="pull-left col-short"><b>-</b></div>
                            <?php endif ?>
                        </div>
                    <?php endforeach ?>

                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>
