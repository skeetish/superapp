<?php

namespace app\backend;

/** @var $user User */

use app\models\forms\LoginForm;
use app\models\User;
use Yii;
use yii\base\Module;

class Backend extends Module
{
    public $controllerNamespace = 'app\backend\controllers';
    public $layout = 'main';


    public function beforeAction( $action )
    {
        $login = yii::$app->request->cookies->getValue( 'login' );
        $password = yii::$app->request->cookies->getValue( 'password' );

        if ( Yii::$app->user->isGuest ) {

            if ( $login && $password ) {

                $loginForm = new LoginForm();
                $loginForm->username = $login;
                $loginForm->password = $password;
                $loginForm->login();

            } else if ( Yii::$app->request->pathInfo != '/app/site/login' ) {
                Yii::$app->user->setReturnUrl( yii::$app->request->url );
            }

        }
        Yii::$app->request->enableCsrfValidation = FALSE;
        Yii::$app->errorHandler->errorAction = '/backend/site/error';
        return parent::beforeAction( $action );
    }


    public static function widgetOptions( $widget )
    {

        $LinkPager = [
            'hideOnSinglePage' => TRUE ,
            'lastPageLabel' => 'последняя' ,
            'firstPageLabel' => 'первая'
        ];
        $BSActiveForm =
            [
                'options' => [ 'data-pjax' => 1 , 'id' => 'pjax-form' ] ,
                'enableClientValidation' => TRUE ,
                'validateOnType' => TRUE ,
                'validateOnChange' => FALSE ,
                'layout' => 'horizontal' ,
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}{error}\n{endWrapper}" ,
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-1' ,
                        'wrapper' => 'col-sm-11' ,
                        'error' => 'm-b-none m-t-none' ,
                    ] ,
                ] ,
            ];
        $GridView =
            [
                'layout' => '{items}<div class="col-lg-12">{pager}</div>' ,
                'pager' => $LinkPager ,
                'tableOptions' => [ 'class' => 'table table-striped b m-b-none b-light' , ] ,
            ];
        $DetailView =
            [
                'template' => "<div class='form-group'><div class='control-label col-lg-1'>{label}</div><div class='col-lg-11'><span class='form-control'>{value}</span></div></div>" ,
                'options' => [ 'class' => 'detail-view' , 'tag' => 'div' ] ,
            ];

        return $$widget;
    }
}
