<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 08.06.14
 * Time: 22:26
 */

namespace app\backend\components\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class VNavBar extends Nav
{


    public function init()
    {
        echo Html::beginTag( 'aside' , [ 'class' => "bg-dark aside-sm nav-xs" , 'id' => 'nav' ] );
        echo Html::beginTag( 'section' , [ 'class' => 'vbox' ] );
        echo Html::beginTag( 'nav' , [ 'class' => 'nav-primary hidden-xs' ] );
        parent::init();

    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        parent::run();
        //        $this->renderInfo();
        echo Html::endTag( 'nav' );
        echo Html::endTag( 'section' );
        echo Html::endTag( 'aside' );

    }

    // Заглушка
    public function renderInfo()
    {
        if ( $this->info ) {
            echo Html::beginTag( 'div' , [ 'class' => 'line dk hidden-nav-xs' ] );
            if ( $this->info[ 'title' ] ) {
                echo Html::beginTag( 'div' , [ 'class' => 'text-muted text-xs hidden-nav-xs padder m-t-sm m-b-sm' ] );
                echo $this->info[ 'title' ];
                echo Html::endTag( 'div' );
            }
        }
        //<div class="line dk hidden-nav-xs"></div>
        //<div class="text-muted text-xs hidden-nav-xs padder m-t-sm m-b-sm">Lables</div>
        /*<ul class="nav">
            <li>
                <a href="mail.html#projects">
                    <i class="i i-circle-sm text-danger-dk"></i>
                    <span>Projects</span>
                </a>
            </li>
        </ul>*/

    }

    public function renderItems()
    {
        $items = [ ];
        foreach ( $this->items as $i => $item ) {
            if ( isset( $item[ 'visible' ] ) && !$item[ 'visible' ] ) {
                unset( $items[ $i ] );
                continue;
            }
            $items[ ] = $this->renderItem( $item );
        }

        return Html::tag( 'ul' , implode( "\n" , $items ) , $this->options );
    }

    /**
     * Renders a widget's item.
     *
     * @param string|array $item the item to render.
     *
     * @return string the rendering result.
     * @throws InvalidConfigException
     */
    public function renderItem( $item )
    {
        if ( is_string( $item ) ) {
            return $item;
        }
        if ( !isset( $item[ 'label' ] ) ) {
            throw new InvalidConfigException( "The 'label' option is required." );
        }

        $label = $this->encodeLabels ? Html::encode( $item[ 'label' ] ) : $item[ 'label' ];
        $options = ArrayHelper::getValue( $item , 'options' , [ ] );
        $items = ArrayHelper::getValue( $item , 'items' );
        $url = ArrayHelper::getValue( $item , 'url' , '#' );
        $linkOptions = ArrayHelper::getValue( $item , 'linkOptions' , [ ] );
        $badge = ArrayHelper::getValue( $item , 'badge' );
        $data = ArrayHelper::getValue( $item , 'data' );

        if ( isset( $item[ 'active' ] ) ) {
            $active = ArrayHelper::remove( $item , 'active' , FALSE );
        } else {
            $active = $this->isItemActive( $item );
        }

        if ( $badge && $data ) {
            Html::addCssClass( $badge , 'badge pull-right' );
            $badge = Html::tag( 'b' , $data , $badge );
        } else {
            $badge = '';
        }

        if ( isset( $item[ 'icon' ] ) ) {
            $icon = Html::tag( 'i' , '' , [ 'class' => ArrayHelper::getValue( $item , 'icon' ) ] );
        } else {
            $icon = '';
        }

        if ( isset( $item[ 'url' ] ) ) {
            $url = Yii::$app->urlManager->createUrl( $url );
        } else {
            $url = '#';
        }
        if ( $items !== NULL ) {

            if ( is_array( $items ) ) {
                if ( $this->activateItems ) {
                    $items = $this->isChildActive( $items , $active );
                }

                foreach ( $items as $item ) {
                    if ( is_array( $items ) )
                        $items = Html::beginTag( 'ul' , [ 'class' => 'nav dk' ] );

                    $items .= $this->renderItem( $item );
                }
                $items .= Html::endTag( 'ul' );

            }
        }

        if ( $this->activateItems && $active ) {
            Html::addCssClass( $options , 'active' );
        }

        return Html::tag( 'li' , Html::a( $badge . $icon . html::tag( 'span' , $label ) , $url , $linkOptions ) . $items , $options );
    }

}
