<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\backend\components\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Modal renders a modal window that can be toggled by clicking on a button.
 * The following example will show the content enclosed between the [[begin()]]
 * and [[end()]] calls within the modal window:
 * ~~~php
 * Modal::begin([
 *     'header' => '<h2>Hello world</h2>',
 *     'toggleButton' => ['label' => 'click me'],
 * ]);
 * echo 'Say hello...';
 * Modal::end();
 * ~~~
 * @see    http://getbootstrap.com/javascript/#modals
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class ModalExt extends \yii\bootstrap\Modal
{

    /**
     * @var array
     */
    public $bodyOptions = [ 'class' => 'modal-body' ];

    protected function renderBodyBegin()
    {

        return Html::beginTag( 'div' , $this->bodyOptions );
    }

    public function renderToggleButton()
    {
        if ( $this->toggleButton !== FALSE ) {
            $tag = ArrayHelper::remove( $this->toggleButton , 'tag' , 'button' );
            $label = ArrayHelper::remove( $this->toggleButton , 'label' , 'Show' );
            if ( $tag === 'button' && !isset( $this->toggleButton[ 'type' ] ) ) {
                $this->toggleButton[ 'type' ] = 'button';
            }

            return Html::tag( $tag , $label , $this->toggleButton );
        } else {
            return NULL;
        }
    }
}
