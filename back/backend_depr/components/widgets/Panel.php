<?php


namespace app\backend\components\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * ActiveForm ...
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Panel extends Widget
{
    public $options = [ 'class' => 'panel panel-default' ];
    public $bodyOptions = [ 'class' => 'panel-body' ];
    public $headerOptions = [ 'class' => 'panel-heading' ];

    public $headerContent;
    public $containerOptions = FALSE;


    public function init()
    {
        if ( !isset( $this->options[ 'id' ] ) ) {
            $this->options[ 'id' ] = $this->getId();
        }

        if ( $this->containerOptions )
            echo Html::beginTag( 'div' , $this->containerOptions );

        if ( $this->options )
            echo Html::beginTag( 'div' , $this->options );

        if ( $this->headerOptions )
            echo Html::tag( 'div' , $this->headerContent , $this->headerOptions );

        if ( $this->bodyOptions )
            echo Html::beginTag( 'div' , $this->bodyOptions );

    }

    public function run()
    {
        if ( $this->options )
            echo Html::endTag( 'div' );
        if ( $this->bodyOptions )
            echo Html::endTag( 'div' );
        if ( $this->containerOptions )
            echo Html::endTag( 'div' );
    }

}
