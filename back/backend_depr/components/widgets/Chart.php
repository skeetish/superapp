<?php
/**
 * Created by PhpStorm.
 * User: skeet
 * Date: 08.06.14
 * Time: 22:26
 */

namespace app\backend\components\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

class Chart extends Widget
{
    public $options = [ 'id' => 'chart', 'tag' => 'div' ];
    public $chartOptions = [ ];

    public $values;
    public $tag;
    public $id;

    public function init()
    {
        yii::$app->controller->view->registerAssetBundle( 'app\common\assets\packages\HighchartsAsset', View::POS_END );

        if ( !$this->id = arrayHelper::getValue( $this->options, 'id' ) )
            $this->id = 'chart';

        if ( $this->tag = arrayHelper::getValue( $this->options, 'tag' ) )
            ArrayHelper::remove( $this->options, 'tag' );
        else $this->tag = 'div';

        if ( $this->values ) {
            $this->chartOptions[ 'series' ] = $this->values;
        }

        echo Html::beginTag( $this->tag, $this->options );
        parent::init();

    }

    public function run()
    {
        parent::run();
        echo Html::endTag( $this->tag );
        $chartOptions = $this->renderPluginOptions();
        $this->getView()->registerJs( "$('#" . $this->id . "') . highcharts({" . $chartOptions . "});" );
    }

    public function renderPluginOptions()
    {
        $chartOptions = '';
        foreach ( $this->chartOptions as $option => $value ) {

            if ( $chartOptions )
                $chartOptions .= ',';

            $chartOptions = $option == 'series' && !( is_array( $value ) && isset( $value[ 0 ] ) )
                ? $chartOptions . $option . ': [' . $this->renderOption( $value ) . ']'
                : $chartOptions . $option . ':' . $this->renderOption( $value );
        }

        return $chartOptions;
    }

    public function renderOption($value)
    {
        $html = '';
        if ( !is_array( $value ) ) {
            preg_match( '/Date.UTC\((.*)\)/', $value, $match );
            $html = ( ( $value == 'true' || $value == 'false' ) || is_numeric( $value ) || $match ) ? $value : "'$value'";

        } else {
            $array = $value;
            $isAssoc = true;

            if ( isset ( $array[ 0 ] ) )
                $isAssoc = false;

            $html .= ( ( $isAssoc ) ? '{' : '[' );
            foreach ( $array as $option => $value ) {

                $html = $isAssoc
                    ? $html . $option . ":" . $this->renderOption( $value )
                    : $html . $this->renderOption( $value );

                if ( next( $array ) )
                    $html .= ',';
            }
            $html .= ( ( $isAssoc ) ? '}' : ']' );

        }
        return $html;

    }

}
