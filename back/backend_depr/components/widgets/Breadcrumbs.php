<?php


namespace app\backend\components\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * ActiveForm ...
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Breadcrumbs extends Widget
{

    public function run()
    {
        $paths = explode( '/' , yii::$app->urlManager->createUrl( [ yii::$app->requestedRoute ] ) );
        $breadcrumbs = [ ];
        $prevPath = NULL;

        foreach ( $paths as $path ) {
            if ( $path && $path != 'index' ) {
                $prevPath .= '/' . $path;
                $breadcrumbs[ ] = [
                    'url' => yii::$app->urlManager->createUrl( [ $prevPath ] ) ,
                    'label' => yii::t( 'app' , $path )
                ];
            }
        }

        foreach ( $breadcrumbs as $k => $breadcrumb ) {
            if ( count( $breadcrumbs ) > $k + 1 )
                echo Html::a( $breadcrumb[ 'label' ] , $breadcrumb[ 'url' ] ) . '->';
            else echo $breadcrumb[ 'label' ];

        }
    }

}
