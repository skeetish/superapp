<?php

$params = require(__DIR__.'/params.php');

$config = [
    'id'                  => 'basic',
    'modules'             => [
        'rest' => [
            'basePath'            => '@app/rest',
            'class' => 'app\rest\Rest',
            'controllerNamespace' => 'app\rest\controllers',
        ],
    ],
    'controllerNamespace' => '\app\common\controllers',
    'timeZone'            => 'Europe/Simferopol',
    'language'            => 'ru',
    'sourceLanguage'      => 'en',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'extensions'          => require(__DIR__.'/../vendor/yiisoft/extensions.php'),
    'components'          => [
        'i18n'         => [
            'translations' => [
                'app' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath'       => '@app/common/messages',
                ]
            ],
        ],
        'request'      => [
            'enableCsrfValidation'   => false,
            'enableCookieValidation' => false,
        ],
        'urlManager'   => [
            'showScriptName'      => false,
            'enableStrictParsing' => true,
            'enablePrettyUrl'     => true,
            'rules'               => [

                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => 'rest/scheme',
                    'patterns'   => [
                        'POST {id}/trainings'            => 'create-training',
                        'GET {id}/trainings'             => 'trainings',
                        'GET {id}/exercises/<alias:\w+>' => 'exercises',
                        'GET {id}/generate'              => 'generate',
                        'GET initials'                   => 'initials',

                    ]
                ],
                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => 'rest/torrent',
                    'patterns'   => [
                        'GET parse' => 'parse',
                        'GET'       => 'index',
                        'POST'      => 'create',

                    ]
                ],
                [
                    'class'      => 'yii\rest\UrlRule',
                    'controller' => 'rest/training',
                ],

                '/<controller>/<action>' => '/rest/<controller>/<action>',
                '/<controller>'          => '/rest/<controller>/index',
                '/'                      => '/rest/default/index',

            ]
        ],
        'services'     => [
            'class' => 'app\common\components\LayerFactory'
        ],
        'factory'      => [
            'class' => 'app\common\components\AppFactory'
        ],

        'cache'        => [
            'class' => 'yii\caching\DummyCache'
        ],
        'user'         => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'rest/system/error',
        ],
        'mail'         => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'           => require(__DIR__.'/'.YII_ENV.'/db.php'),
        'mongodb'      => require_once(__DIR__.'/mongodb.php'),

    ],
    'params'              => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
