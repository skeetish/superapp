<?php

Yii::setAlias('@tests', dirname(__DIR__).'/tests');

$params = require(__DIR__.'/params.php');
$db = require(__DIR__.'/dev/db.php');

return [
    'id'                  => 'basic-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerMap'       => [
        'fixture' => [
            'class'           => 'yii\faker\FixtureController',
            'templatePath'    => '@tests/codeception/unit/templates/fixtures',
            'fixtureDataPath' => '@tests/codeception/unit/fixtures/data'

        ],
    ],
    'controllerNamespace' => 'app\commands',
    'extensions'          => require(__DIR__.'/../vendor/yiisoft/extensions.php'),
    'components'          => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log'   => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db'    => $db,
    ],
    'params'              => $params,
];
