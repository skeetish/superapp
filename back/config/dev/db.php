<?php

return [
    'class'    => 'yii\db\Connection',
    'dsn'      => 'mysql:host=127.0.0.1;dbname=superapp',
    'username' => 'root',
    'password' => '',
    'charset'  => 'utf8',
    'enableSchemaCache' => false,
];
