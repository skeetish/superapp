<?php
/**
 * Yii bootstrap file.
 *
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

require(__DIR__.'/BaseYii.php');

class Yii extends \yii\BaseYii
{
    /**
     * @var IdeApplication
     */
    public static $app;
}

/**
 * @property \app\common\components\AppFactory   factory
 * @property \app\common\components\LayerFactory services
 * @property \app\rest\components\RestUser       user
 */
class IdeApplication extends \yii\web\Application
{

}

