<?php
/**
 * Application configuration shared by all test types
 */
return [
    'components' => [
        'mongodb' => 'xd',
        'factory' => function () {
            return 35;
        },
        'db'         => [
            'dsn' => 'mysql:host=localhost;dbname=yii2_basic_tests',
        ],
        'mailer'     => [
            'useFileTransport' => true,
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
    ],
];


