<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 15.12.2014
 * Time: 2:45
 */
$filename = $faker->word.$faker->fileExtension;
return /*[
    'file' => $faker->uuid,
    'phone' => $faker->phoneNumber,
    'city' => $faker->city,
    'password' => Yii::$app->getSecurity()->generatePasswordHash('password_' . $index),
    'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
    'intro' => $faker->sentence(7, true),  // generate a sentence with 7 words
];*/
[
    'fileId'      => $faker->uuid,
    'fileName'    => $filename,
    'filePath'    => "The Legend of Korra. Book 4. Rus\\$filename",
    'fileSize'    => $faker->numberBetween(10000,50000),
    'torrentId'   => '1',
    'fileCreated' => '14',
    'fileUpdated' => '12'
];