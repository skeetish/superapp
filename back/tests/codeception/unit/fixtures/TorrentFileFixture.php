<?php
namespace tests\codeception\unit\fixtures;


use yii\test\ActiveFixture;

class TorrentFileFixture extends ActiveFixture
{
    public $modelClass = '\app\models\TorrentFile';
}