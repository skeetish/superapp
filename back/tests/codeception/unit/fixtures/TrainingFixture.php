<?php
namespace tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class TrainingFixture extends ActiveFixture
{
    public $modelClass = '\app\models\Training';
}