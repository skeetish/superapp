<?php

return [
    '1'     => [
        'torrentId'         => '1',
        'torrentName'       => 'Легенда о Корре. Книга 3. Flux-Team',
        'torrentUrl'        => 'http://dl.rutracker.org/forum/dl.php?t=4772247',
        'torrentCreateDate' => '1414236775',
        'torrentParseDate'  => '1417998464',
        'torrentChangeDate' => '1417998464',
        'torrentFilePath'   => 'bac95bbcfdec4ecff76e2a6e190781c4',
        'torrentStatus'     => '0',
        'torrentUserId'     => '2'
    ],
    'empty' => [
        'torrentId'         => '2',
        'torrentName'       => 'Empty',
        'torrentUrl'        => 'empty',
        'torrentCreateDate' => '1414236775',
        'torrentParseDate'  => '1417998464',
        'torrentChangeDate' => '1417998464',
        'torrentFilePath'   => 'null',
        'torrentStatus'     => '0',
        'torrentUserId'     => '2'
    ],
];