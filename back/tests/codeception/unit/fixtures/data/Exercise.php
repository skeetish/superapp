<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 01.02.2015
 * Time: 13:29
 */
return [
    '1.1.1' =>
        [
            'id'            => '164',
            'trainId'       => '588',
            'exerciseId'    => '1',
            'count'         => '5',
            'weight'        => '75.0',
            'exerciseOrder' => '0',
            'increaseType'  => '1',
            'reps'          => '5',
            'completed'     => '1',
            'updated'       => '0',
            'comment'       => '',
        ],
    '1.1.2' =>
        [
            'id'            => '165',
            'trainId'       => '588',
            'exerciseId'    => '3',
            'count'         => '5',
            'weight'        => '55.0',
            'exerciseOrder' => '0',
            'increaseType'  => '1',
            'reps'          => '5',
            'completed'     => '1',
            'updated'       => '0',
            'comment'       => '',
        ],
    '1.1.3' =>
        array (
            'id'            => '166',
            'trainId'       => '588',
            'exerciseId'    => '4',
            'count'         => '5',
            'weight'        => '85.0',
            'exerciseOrder' => '0',
            'increaseType'  => '1',
            'reps'          => '5',
            'completed'     => '0',
            'updated'       => '0',
            'comment'       => '',
        ),
    '1.1.4' =>
        array (
            'id'            => '167',
            'trainId'       => '588',
            'exerciseId'    => '5',
            'count'         => '5',
            'weight'        => '125.0',
            'exerciseOrder' => '0',
            'increaseType'  => '1',
            'reps'          => '5',
            'completed'     => '0',
            'updated'       => '0',
            'comment'       => '',
        ),
];