<?php
/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 18.01.2015
 * Time: 1:50
 */

return [

    '1.saturday' => [
        'id'           => '1',
        'schemeId'     => '1',
        'label'        => 'суббота',
        'expression'   => 'saturday',
        'dayExercises' => null,
    ],
    '1.tuesday'  => [
        'id'           => '2',
        'schemeId'     => '1',
        'label'        => 'вторник',
        'expression'   => 'tuesday',
        'dayExercises' => null,
    ],
    '1.thursday' => [
        'id'           => '3',
        'schemeId'     => '1',
        'label'        => 'четверг',
        'expression'   => 'thursday',
        'dayExercises' => null,
    ],

];