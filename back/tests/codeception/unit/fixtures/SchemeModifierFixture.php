<?php
namespace tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class SchemeModifierFixture extends ActiveFixture
{
    public $modelClass = '\app\models\SchemeModifier';
}