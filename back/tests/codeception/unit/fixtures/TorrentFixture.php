<?php
namespace tests\codeception\unit\fixtures;

/**
 * Created by PhpStorm.
 * User: Skeet
 * Date: 09.12.2014
 * Time: 17:11
 */

use yii\test\ActiveFixture;

class TorrentFixture extends ActiveFixture
{
    public $modelClass = '\app\models\Torrent';
}