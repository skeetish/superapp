<?php
namespace tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class SchemeDayFixture extends ActiveFixture
{
    public $modelClass = '\app\models\SchemeDay';
}