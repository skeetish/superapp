<?php
namespace tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class ExerciseFixture extends ActiveFixture
{
    public $modelClass = '\app\models\Exercise';
}