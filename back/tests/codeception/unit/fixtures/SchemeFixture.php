<?php
namespace tests\codeception\unit\fixtures;

use yii\test\ActiveFixture;

class SchemeFixture extends ActiveFixture
{
    public $modelClass = '\app\models\Scheme';
}