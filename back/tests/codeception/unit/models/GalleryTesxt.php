<?php

namespace tests\codeception\unit\models;

use app\models\Photo;
use Codeception\Specify;
use tests\codeception\unit\fixtures\GalleryFixture;
use yii\codeception\DbTestCase;

class GalleryTest extends DbTestCase
{
    use Specify;

    public function fixtures()
    {
        return [
            'photos' => GalleryFixture::className()
        ];
    }

    public function testGetImageLink()
    {
        $this->assertEquals( '/images/pics/123.jpg', Photo::getImageLink( '123.jpg' ) );
        $this->assertEquals( '/images/thumbs/123.jpg', Photo::getImageLink( '123.jpg', true ) );
    }

    public function testGetImageDir()
    {
        $this->assertEquals( '/images/pics/', Photo::getImageDir() );
        $this->assertEquals( '/images/thumbs/', Photo::getImageDir( true ) );
    }

    protected function setUp()
    {
        parent::setUp();
    }

}


