<?php

namespace tests\codeception\unit\models;

use app\models\Torrent;
use app\models\TorrentFile;
use Codeception\Specify;
use Codeception\TestCase;
use tests\codeception\unit\fixtures\TorrentFileFixture;
use tests\codeception\unit\fixtures\TorrentFixture;
use Yii;
use yii\codeception\DbTestCase;

/**
 * @method Torrent torrents
 * @method TorrentFile torrentFiles
 * */
class TorrentTest extends DbTestCase
{
    use Specify;

    public $srcFile;
    public $destFile;

    public function fixtures()
    {
        return [
            'torrentFiles' => TorrentFileFixture::className(),
            'torrents'     => TorrentFixture::className(),
        ];
    }


    public function testSaveTempFile()
    {
        $torrent = $this->torrents(1);
        $fileContent = md5(time());

        file_put_contents($this->destFile, $fileContent);
        $torrent->saveTempFile($torrent->torrentFilePath);

        $this->assertEquals($fileContent, file_get_contents($torrent->getTorrentPath()));

        unlink($torrent->getTorrentPath());
    }

    public function testExtractCookie()
    {
        $torrent = $this->torrents(1);

        $cookie = $torrent->extractCookie(Yii::getAlias('@uploads').'/cookie.txt', $torrent->torrentUrl);
        $expected = 'bb_data=1-15526342-fMq4WHP1Flw40nBPpmYe-90714763-1418157247-1418158397-3109931736-1;bb_dl=4772247';
        $this->assertEquals($expected, $cookie);

        $cookie = $torrent->extractCookie(Yii::getAlias('@uploads').'/empty.txt', $torrent->torrentUrl);
        $expected = '';
        $this->assertEquals($expected, $cookie);
    }

    public function testParseTorrentFile()
    {
        $expected = [
            'torrentName'     => 'PvZ-v3060.ipa',
            'torrentSize'     => 70910510,
            'torrentFilePath' => '0c2796a9abba7649815bf9559e17de9c',
            'torrentHash'     => 'dcacc1528421e409456b0c87f831431f',
            'torrentFiles'    => [['fileName' => 'PvZ-v3060.ipa', 'filePath' => 'PvZ-v3060.ipa', 'fileSize' => 70910510]]
        ];
        $torrent = $this->torrents(1);
        $this->assertEquals($expected, $torrent->parseTorrentFile($this->srcFile));

        $expected = [
            'torrentName'     => 'superapp/web/../tests/codeception/unit/fixtures/files/empty.torrent',
            'torrentSize'     => 0.0,
            'torrentFilePath' => '05bd7b33c83cf4975787de619265b6a4',
            'torrentHash'     => 'd41d8cd98f00b204e9800998ecf8427e',
            'torrentFiles'    => [['fileName' => 'empty.torrent', 'filePath' => 'superapp/web/../tests/codeception/unit/fixtures/files/empty.torrent', 'fileSize' => 0.0]]
        ];
        $torrent = $this->torrents('empty');
        $this->assertEquals($expected, $torrent->parseTorrentFile(Torrent::buildPath('empty')));

    }

    public function testPerformFetch()
    {
        $torrent = $this->torrents(1);
        $curlRequest = $this->getMock('\app\common\components\CurlRequest', ['makeRequest']);
        Yii::$app->factory->expects($this->any())->method('getCurlInstance')->will($this->returnValue($curlRequest));

        $curlRequest->expects($this->at(0))->method('makeRequest')->will($this->returnValue(file_get_contents($this->srcFile)));
        $curlRequest->expects($this->at(1))->method('makeRequest')->will($this->returnValue('wskeseas'));
        $curlRequest->expects($this->at(2))->method('makeRequest')->will($this->returnValue(''));

        $this->assertTrue($torrent->performFetch($torrent->torrentUrl, $this->destFile));
        $this->assertFalse($torrent->performFetch($torrent->torrentUrl, $this->destFile));
        $this->assertFalse($torrent->performFetch($torrent->torrentUrl, $this->destFile));
    }

    public function testBuildChangeList()
    {
        $expected = [
            'torrentFiles' => [['fileName' => 'PvZ-v3060.ipa', 'filePath' => 'PvZ-v3060.ipa', 'fileSize' => 70910510]]
        ];

        $currentFiles = [
            $this->torrentFiles(0),
            $this->torrentFiles(1),
            $this->torrentFiles(2)
        ];

        $incoming = [
            'torrentName'     => 'PvZ-v3060.ipa',
            'torrentSize'     => 70910510,
            'torrentUrl'      => 'http://dl.rutracker.org/forum/dl.php?t=4772247',
            'torrentFilePath' => '0c2796a9abba7649815bf9559e17de9c',
            'torrentHash'     => 'dcacc1528421e409456b0c87f831431f',
            'torrentFiles'    => [['fileName' => 'PvZ-v3060.ipa', 'filePath' => 'PvZ-v3060.ipa', 'fileSize' => 70910510]]
        ];
        $torrent = $this->torrents(1);
        $this->assertEquals([], $torrent->buildChangeList($incoming, $currentFiles));
    }

    protected function setUp()
    {
        parent::setUp();
        Yii::$app->setComponents(['factory' => $this->getMock('\app\common\components\AppFactory')]);
        Yii::setAlias('@uploads', Yii::getAlias('@webroot').'/../tests/codeception/unit/fixtures/files');

        $this->srcFile = Yii::getAlias('@uploads').'/src.torrent';
        $this->destFile = Yii::getAlias('@uploads').'/temp.torrent';
        file_put_contents($this->destFile, '');
    }
}


