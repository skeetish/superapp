<?php

namespace tests\codeception\unit\models;

use app\models\Exercise;
use app\models\Scheme;
use app\models\SchemeDay;
use app\models\SchemeModifier;
use app\models\Training;
use Codeception\Specify;
use tests\codeception\unit\fixtures\ExerciseFixture;
use tests\codeception\unit\fixtures\SchemeDayFixture;
use tests\codeception\unit\fixtures\SchemeFixture;
use tests\codeception\unit\fixtures\SchemeModifierFixture;
use tests\codeception\unit\fixtures\TrainingFixture;
use yii\codeception\DbTestCase;

/**
 * @method Scheme Scheme()
 * @method SchemeModifier Modifier()
 * @method Training Training()
 * @method SchemeDay Day()
 * @method Exercise Exercise()
 */
class SchemeTest extends DbTestCase
{
    use Specify;

    public function fixtures()
    {
        return [
            'scheme'   => SchemeFixture::className(),
            'modifier' => SchemeModifierFixture::className(),
            'training' => TrainingFixture::className(),
            'day'      => SchemeDayFixture::className(),
            'exercise' => ExerciseFixture::className()
        ];
    }

    public function testFindClosestDate()
    {
        /** @var Scheme $scheme */

        $scheme = $this->scheme(1);
        $this->assertEquals(1200, $scheme->findClosestDate([
                'nextFromNow'  => [1200, 1300, 1400],
                'nextFromDate' => [200, 300, 400]
            ]),
            'incorrect closest date');

        $this->assertEquals(1200, $scheme->findClosestDate([
                'nextFromNow'  => [200, 300, 400],
                'nextFromDate' => [1200, 1300, 1400]
            ]),
            'incorrect closest date');
    }

    public function testCalcNextModifier()
    {
        /** @var Scheme $scheme */
        $scheme = $this->scheme(1);
        $modifiers = [
            $this->modifier('1.100%'),
            $this->modifier('1.50%'),
            $this->modifier('1.75%')
        ];
        $training = $this->training('1.1');

        $this->assertEquals(
            $modifiers[1],
            $scheme->calcNextModifier($training, $modifiers),
            'incorrect next modifier'
        );

    }

    public function testCustomIntervalDate()
    {
        $scheme = $this->scheme(1);
        $mock = $this->getMock($scheme::className(), ['findClosestDate']);

        $days = [
            $this->day('1.saturday'),
            $this->day('1.tuesday'),
            $this->day('1.thursday'),
        ];

        $mock->expects($this->any())
            ->method('findClosestDate')
            ->will($this->returnValue(1423260000));

        /** @var Scheme $mock */
        $this->assertEquals(
            ['day' => $days[0], 'date' => 1423260000],
            $mock->customIntervalDate(123, $days)
        );

    }

    public function testCalcNextDay()
    {
        $training = $this->training('1.1');
        $days = [
            $this->day('1.saturday'),
            $this->day('1.tuesday'),
            $this->day('1.thursday'),
        ];

        $mock = $this->getMock(
            Scheme::className(),
            ['customIntervalDate', 'fixedIntervalDate']
        );

        $mock->expects($this->any())
            ->method('fixedIntervalDate')
            ->will($this->returnValue('fixed interval'));

        $mock->expects($this->any())
            ->method('customIntervalDate')
            ->will($this->returnValue('custom interval'));

        /** @var Scheme $mock */
        $mock->customInterval = 1;
        $mock->populateRelation('days', $days);
        $this->assertEquals('custom interval', $mock->calcNextDay($training));

        $mock->customInterval = 0;
        $mock->intervalTime = 24000;
        $this->assertEquals('fixed interval', $mock->calcNextDay($training));

    }

    public function testGenerateNewTrain()
    {
        $mock = $this->getMock(
            Scheme::className(),
            ['calcNextTrainParams']
        );

        $nextTrainParams = [
            'day'       => $this->day('1.tuesday'),
            'modifier'  => $this->modifier('1.100%'),
            'exercises' => ['exercises'],
            'date'      => 12345
        ];
        $mock->expects($this->any())
            ->method('calcNextTrainParams')
            ->will($this->returnValue($nextTrainParams));

        /** @var Scheme $mock */
        $actual = $mock->generateNewTrain();

        $this->assertTrue($actual instanceof Training);
        $this->assertEquals($actual->day, $nextTrainParams['day']);
        $this->assertEquals($actual->modifier, $nextTrainParams['modifier']);
        $this->assertEquals($actual->exercises, $nextTrainParams['exercises']);
        $this->assertEquals($actual->date, 12345);

    }

    public function testCalcNextTrainExercises()
    {
        $scheme = $this->scheme('1');
        /** @var Exercise[] $exercises */
        $exercises = [
            $this->exercise('1.1.1'),
            $this->exercise('1.1.2'),
            $this->exercise('1.1.3'),
            $this->exercise('1.1.4'),
        ];

        $modifier = $this->modifier('1.100%');
        $actual = $scheme->calcNextTrainExercises($exercises, $modifier);
        foreach ($actual as $exercise) {
            $this->assertEquals(1, $exercise->completed);
            $this->assertEquals(0, $exercise->comment);
        }
        $this->assertLessThan($actual[0]->weight, $exercises[0]->weight);
        $this->assertLessThan($actual[1]->weight, $exercises[1]->weight);
        $this->assertEquals($actual[2]->weight, $exercises[2]->weight);
        $this->assertEquals($actual[3]->weight, $exercises[3]->weight);

        $modifier = $this->modifier('1.50%');
        $actual = $scheme->calcNextTrainExercises($exercises, $modifier);
        $this->assertEquals($actual[0]->weight, $exercises[0]->weight);
        $this->assertEquals($actual[1]->weight, $exercises[1]->weight);
        $this->assertEquals($actual[2]->weight, $exercises[2]->weight);
        $this->assertEquals($actual[3]->weight, $exercises[3]->weight);

        $this->assertEmpty($scheme->calcNextTrainExercises([], $modifier));
    }

    public function testCalcSeparate()
    {
        $trainingMock = $this->getMock(
            Training::className(),
            ['getExercises']
        );
        $schemeMock = $this->getMock(
            Scheme::className(),
            ['getLastTraining', 'calcNextModifier', 'calcNextTrainExercises']
        );

        $schemeMock->expects($this->any())
            ->method('calcNextTrainExercises')
            ->will($this->returnValue(['modified']));

        $schemeMock->expects($this->any())
            ->method('getLastTraining')
            ->will($this->returnValue($trainingMock));

        $modifier1 = $this->modifier('1.50%');
        $modifier2 = $this->modifier('1.50%');
        $modifier3 = $this->modifier('1.100%');

        $schemeMock->expects($this->any())
            ->method('calcNextModifier')
            ->will(
                $this->onConsecutiveCalls(
                    $modifier1,
                    $modifier2,
                    $modifier3
                ));

        /** @var Scheme $schemeMock */
        $this->assertEquals(
            ['modifier' => $modifier1, 'exercises' => []],
            $schemeMock->calcSeparate(['day' => $this->day('1.saturday')])
        );

        $schemeMock->separateModifiers = true;
        $this->assertEquals(
            ['modifier' => $modifier2, 'exercises' => ['modified']],
            $schemeMock->calcSeparate(['day' => $this->day('1.saturday')])
        );

        $schemeMock->separateModifiers = false;
        $this->assertEquals(
            ['modifier' => $modifier3, 'exercises' => ['modified']],
            $schemeMock->calcSeparate(['day' => $this->day('1.saturday')])
        );

    }

    public function testCalcNonSeparate()
    {

    }

    protected function setUp()
    {
        parent::setUp();
    }

}


