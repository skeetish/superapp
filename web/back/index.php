<?php

if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    defined('YII_ENV') or define('YII_ENV', 'dev');
} else {
    defined('YII_ENV') or define('YII_ENV', 'production');
}

    defined('YII_DEBUG') or define('YII_DEBUG', true);

require(__DIR__.'/../../back/vendor/autoload.php');
require(__DIR__.'/../../back/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__.'/../../back/config/web.php');
Yii::setAlias('@webroot', __DIR__);
Yii::setAlias('@uploads', __DIR__.'/uploads');

(new yii\web\Application($config))->run();

